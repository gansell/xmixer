.\"
.\" Written by Rasca, Berlin 1997-99
.\" Published under the GNU GPL V2
.\"
.TH XMIXER 1 "Aug 1999" "GNU" "Version 0.9"
.SH NAME
xmixer - Xaw-based soundcard mixer program for Linux/X11
.br
xgmixer - Gtk+-based soundcard mixer program for Linux/X11
.SH SYNOPSIS
.BI "xmixer [-h?qnVD] [-m "<device> "] [-d "<display> "]
.br
.BI "xgmixer [-h?qnVD] [-m "<device> "] [--display "<display> "]

XMixer is a X11 soundcard mixer program for the Linux OS.
It uses the OSS/Free sound driver so it may work also on other
OS which support OSS/Free. It is published under the GNU General Public
License, Version 2.

Use the up and down cursor keys or the mouse for changing the values
of the device sliders.

To bring back the menu use the middle mouse button.

.SH OPTIONS

.B -m mixer_device
.br
Use an alternate mixer device, '/dev/mixer' is the default.

.B -q
.br
Quiet mode - parse and eval the configuration file, do not
start the X11 interface but terminate. This mode is designed
for batches to initialize the mixer interface with your previous
saved values, e.g. '~/.xsession'.

.B -n
.br
Do (n)ot load the SCFF configuration file on start up.

.B -V
.br
Show version information of xmixer on standard output.

.B -D
.br
Just dump the settings of the mixer device to stdout and
exit. Perhaps
this is interessting for some usage in shell scripts ..

.SH X APPLICATION RESOURCES (Xaw Version)

.B deviceName
.br
Default: '/dev/mixer', this is ignored in batch only or in dump mode,
use the '-m <dev_name>' switch instead ..

.B updateTime
.br
Default: '30', time in seconds for updating the slider positions
if an other
program has changed mixer setting.

.B helpCommand
.br
Default: 'rxvt -e man xmixer &', command which should be executed if the
Help->Manual menu entry is choosen.

.B focusColor
.br
Default: 'black', color which should be used for the border if you activate
a device ..

.SH FILES
Xaw version:
.br
$HOME/.xmixer.scf
.br
$XAPPLRESDIR/XMixer

Gtk+ version:
.br
$HOME/.xgmixer.scf
.br
$HOME/.gtk/xgmixer.rc

.SH AUTHOR
Rasca
.br
http://home.pages.de/~rasca/
.br
XMixer is published under the GNU General Public License

