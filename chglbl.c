/*
 * chglbl.c
 *
 * Copyright (C) 1997 Rasca, Berlin
 * EMail: thron@gmx.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/Xaw/Form.h>
#include <Xw/Field.h>
#include <Xw/Button.h>
#include <Xw/Label.h>

void WmDelEH (Widget, XtPointer, XEvent*, Boolean*);
static Widget shell = None;
static Widget frame, field, ok;

/*
 * find toplevel widget
 */
static Widget
Toplevel (Widget w)
{
    while ((!XtIsTopLevelShell(w)) && XtParent(w))
        w = XtParent(w);
    return (w);
}

/*
 */
static void
CbDone (Widget w, XtPointer client_data, XtPointer call_data)
{
#ifdef DEBUG
	printf ("CbDone()\n");
#endif
	if ((strcmp ("ok", XtName(w)) == 0) || (strcmp("field", XtName(w)) ==0)) {
		char *val;

		val = XwFieldGetString (field);
		if (val)
			XtVaSetValues ((Widget)client_data, XtNlabel, val, NULL);
	}
	XtDestroyWidget (shell);
}

/*
 */
static void
CbTab (Widget w, XtPointer client_data, XtPointer call_data)
{
	Widget *pn = (Widget*)client_data;
	XEvent *ev = (XEvent*) call_data;

#ifdef DEBUG
	printf ("CbTab()\n");
#endif
	if (ev->type != KeyPress)
		return;
	if (ev->xkey.state == ShiftMask) {
		XtSetKeyboardFocus (XtParent(pn[0]), pn[0]);
	} else {
		XtSetKeyboardFocus (XtParent(pn[1]), pn[1]);
	}
}

/*
 * change the label of a device
 */
void
CbChangeLabel (Widget w, XtPointer client_data, XtPointer call_data)
{
	Widget top, label, cancel;
	static Widget prev_next[3][2];
	Position x, y;
	char *name = NULL;
	static Atom wm_delete;

	wm_delete = (Atom) client_data;
	top = Toplevel(w);
	XtVaGetValues (w, XtNlabel, &name, NULL);
	shell = XtVaCreatePopupShell ("new_label", transientShellWidgetClass, top,
				NULL);

	frame = XtVaCreateManagedWidget ("label_box", formWidgetClass, shell,
				NULL);

	label = XtVaCreateManagedWidget ("label", xwLabelWidgetClass, frame,
				XtNlabel, "Label:",
				NULL);

	field = XtVaCreateManagedWidget ("field", xwFieldWidgetClass, frame,
				XtNstring, name,
				XtNfromHoriz, label,
				NULL);

	ok     = XtVaCreateManagedWidget ("ok", xwButtonWidgetClass, frame,
				XtNfromVert, label,
				NULL);

	cancel = XtVaCreateManagedWidget ("cancel", xwButtonWidgetClass, frame,
				XtNfromVert, label,
				XtNfromHoriz, ok,
				NULL);

	XtAddCallback (field, XtNactivateCallback, CbDone, w);
	XtAddCallback (ok, XtNcallback, CbDone, w);
	XtAddCallback (cancel, XtNcallback, CbDone, w);

	prev_next[0][0] = cancel;
	prev_next[0][1] = ok;
	XtAddCallback (field, XtNtabCallback, CbTab, prev_next[0]);
	prev_next[1][0] = field;
	prev_next[1][1] = cancel;
	XtAddCallback (ok, XtNtabCallback, CbTab, prev_next[1]);
	prev_next[2][0] = ok;
	prev_next[2][1] = field;
	XtAddCallback (cancel, XtNtabCallback, CbTab, prev_next[2]);

	XtTranslateCoords (w, 0, 0, &x, &y);
	XtVaSetValues (shell, XtNx, x, XtNy, y, NULL);
	XtPopup (shell, XtGrabNonexclusive);

	XSetWMProtocols (XtDisplay(shell), XtWindow(shell), &wm_delete, 1);
	XtAddEventHandler (shell, NoEventMask, True, WmDelEH , &wm_delete);
	XtInstallAccelerators (field, ok);
	XtInstallAccelerators (field, cancel);
	XtSetKeyboardFocus (frame, field);
}

