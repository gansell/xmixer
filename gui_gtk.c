/*
 * xmixer:gui_gtk.c
 *
 * Copyright (C) 1997-99 Rasca, Berlin
 * EMail: thron@gmx.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include <gdk/gdkkeysyms.h>
/**/
#include "gui.h"
#include "about.h"
#include "mixer.h"
#include "scf.h"

#define LEFT	0
#define RIGHT	1

#define GSIG	GTK_SIGNAL_FUNC
#define GOBJ	GTK_OBJECT

typedef struct {
	int dev;
	int locked;
	int mute;
	GtkWidget *box;
	GtkWidget *label;
	GtkWidget *lock;
	GtkObject *left;
	GtkObject *right;
} wMixer, mixer_t;

typedef struct {

	/* resources
	 */
	char *dev_name;
	int update_time;
	char *help_command;

	/* private
	 */
	char *program;
	mixer_t *devs;
	int max;
	GtkWidget *menu;
	GtkWidget *label;
	GtkTooltips *tips;
} AppData;

/* some globals
 */

static AppData App;

#define Toplevel()	(App.toplevel)

/* actions definitions
 */

/*
 */
static void
update_label (mixer_t *m)
{
	int vol_left, vol_right = 0;
	static char buff[32];

	vol_left = mixer_get_vol_left (1, m->dev);
	if (mixer_is_stereo (1, m->dev)) {
		vol_right = mixer_get_vol_right (1, m->dev);
		sprintf (buff, "%3d:%3d", vol_left, vol_right);
	} else {
		sprintf (buff, "%3d    ", vol_left);
	}
	gtk_label_set_text (GTK_LABEL(App.label), buff);
}

/*
 * called before gui_main()
 */
char *
gui_init (int *argc, char ***argv)
{
	char *home, *rc, *rc_path, *p;

	home = getenv ("HOME");
	if (home) {
		/* check for the program name .. */
		p = strrchr (*argv[0], '/');
		if (!p)
			p = *argv[0];
		else
			p = p+1;

		rc = malloc (strlen (p) + 4);
		if (!rc)
			exit;
		sprintf (rc, "%s.rc", p);
		rc_path = malloc (strlen (home) + strlen (rc) + 2);
		if (rc_path) {
			sprintf (rc_path, "%s/.gtk/%s", home, rc);
			gtk_rc_add_default_file (rc_path);
			free (rc_path);
		}
		free (rc);
	}
	gtk_init (argc, argv);
	return ((char *)NULL);
}

/*
 */
static void
cb_mute (GtkWidget *w, gpointer data)
{
	mixer_t *mix = (mixer_t *) data;
	int vol;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(w))) {
		mix->mute = 1;
		mixer_set_vol_left (1, mix->dev, 0);
		if (mixer_is_stereo (1, mix->dev))
			mixer_set_vol_right (1, mix->dev, 0);
	} else {
		mix->mute = 0;
		vol = 100 - GTK_ADJUSTMENT(mix->left)->value;
		mixer_set_vol_left (1, mix->dev, vol);
		if (mixer_is_stereo (1, mix->dev)) {
			vol = 100 - GTK_ADJUSTMENT(mix->right)->value;
			mixer_set_vol_right (1, mix->dev, vol);
		}
	}
}

/*
 */
static void
cb_rec (GtkWidget *w, gpointer data)
{
	mixer_t *mix = (mixer_t *) data;
	if (mix) {
		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(w))) {
			mixer_set_rec (1, mix->dev, 1);
		} else {
			mixer_set_rec (1, mix->dev, 0);
		}
	}
}

typedef struct {
	mixer_t *mix;
	AppData *ad;
	GtkWidget *ok;
	GtkWidget *cancel;
	GtkWidget *dlg;
	GtkWidget *en;
} dlg_data_t;


/*
 * change label
 */
static void
cb_change (GtkWidget *w, gpointer data)
{
	dlg_data_t *dlg_data = (dlg_data_t *) data;
	char *str;

	if (w == dlg_data->ok) {
		str = gtk_entry_get_text(GTK_ENTRY(dlg_data->en));
		if (str && *str) {
			gtk_label_set_text (
					GTK_LABEL(GTK_BIN(dlg_data->mix->label)->child), str);
		}
	}
	gtk_widget_destroy(GTK_WIDGET(dlg_data->dlg));
}

/*
 */
static gint
cb_key (GtkWidget *w, GdkEventKey *event, gpointer data)
{
	dlg_data_t *dlg_data = (dlg_data_t *) data;
	if (event->keyval == GDK_Escape) {
		cb_change (dlg_data->cancel, dlg_data);
		return TRUE;
	}
	if (event->keyval == GDK_Return) {
		cb_change (dlg_data->ok, dlg_data);
		return TRUE;
	}
	return FALSE;
}

/*
 * called if the user wants to change the label form a device slider
 */
static void
cb_label (GtkWidget *w, gpointer data)
{
	mixer_t *mix = (mixer_t *) data;
	GtkWidget *label, *btn, *entry, *box;
	static GtkWidget *dlg = NULL;
	static dlg_data_t dlg_data;
	char *str;

	if (dlg)
		return;
	dlg = gtk_dialog_new ();

	dlg_data.mix = mix;
	dlg_data.ad = &App;
	dlg_data.dlg = dlg;

	gtk_signal_connect (GOBJ(dlg), "destroy", GSIG(gtk_widget_destroyed), &dlg);
	gtk_window_set_title (GTK_WINDOW(dlg), "Change Label Dialog");
	gtk_window_position (GTK_WINDOW(dlg), GTK_WIN_POS_MOUSE);

	box = gtk_hbox_new (FALSE, 5);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dlg)->vbox),box,FALSE,TRUE,4);

	label = gtk_label_new ("New Label:");
	gtk_box_pack_start(GTK_BOX(box),label,TRUE,TRUE,0);

	entry = gtk_entry_new ();
	str = GTK_LABEL(GTK_BIN(mix->label)->child)->label;
	gtk_entry_set_text (GTK_ENTRY(entry), str);
	gtk_box_pack_start(GTK_BOX(box),entry,TRUE,TRUE,0);
	gtk_signal_connect(GOBJ(entry),"key_press_event",GSIG(cb_key),&dlg_data);
	dlg_data.en = entry;
	gtk_widget_grab_focus (entry);

	btn = gtk_button_new_with_label ("Ok");
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dlg)->action_area),btn,TRUE,TRUE,0);
	gtk_signal_connect (GOBJ(btn), "clicked", GSIG(cb_change), &dlg_data);
	dlg_data.ok = btn;

	btn = gtk_button_new_with_label ("Cancel");
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dlg)->action_area),btn,TRUE,TRUE,0);
	gtk_signal_connect (GOBJ(btn), "clicked", GSIG(cb_change), &dlg_data);
	dlg_data.cancel = btn;

	gtk_widget_show_all (dlg);
}

/*
 */
static void
cb_left (GtkWidget *w, gpointer data)
{
	mixer_t *mix = (mixer_t *) data;
	int vol;
	if (mixer_is_stereo (1, mix->dev)) {
		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(mix->lock))) {
			if (GTK_ADJUSTMENT(mix->left)->value
					!= GTK_ADJUSTMENT(mix->right)->value) {
			
				GTK_ADJUSTMENT(mix->right)->value =
					GTK_ADJUSTMENT(mix->left)->value;
				gtk_signal_emit_by_name(GTK_OBJECT(mix->right),"value_changed");
			}
		}
	}
	vol = 100 - GTK_ADJUSTMENT(mix->left)->value;
	if (!mix->mute)
		mixer_set_vol_left (1, mix->dev, vol);
	update_label (mix);
}

/*
 */
static void
cb_right (GtkWidget *w, gpointer data)
{
	mixer_t *mix = (mixer_t *) data;
	int vol;

	if (mixer_is_stereo (1, mix->dev)) {
		if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(mix->lock))) {
			if (GTK_ADJUSTMENT(mix->left)->value
					!= GTK_ADJUSTMENT(mix->right)->value) {
			
				GTK_ADJUSTMENT(mix->left)->value =
					GTK_ADJUSTMENT(mix->right)->value;
				gtk_signal_emit_by_name(GTK_OBJECT(mix->left),"value_changed");
			}
		}
	}
	vol = 100 - GTK_ADJUSTMENT(mix->right)->value;
	if (!mix->mute)
		mixer_set_vol_right (1, mix->dev, vol);
	update_label (mix);
}

/*
 */
static void
cb_save (GtkWidget *w, gpointer data)
{
	AppData *ad = (AppData *) data;
	const char *fname;
	FILE *fp;
	char *name, *label;
	int i;

	fname = scf_file_name (ad->program, SCF_PRIVAT);
	if (fname) {
		fp = fopen (fname, "w");
		if (!fp)
			return;
		fprintf (fp, "#SCFF 1.0\n");
		fprintf (fp, "# mixer device name\n");
		fprintf (fp, "[%s]\n\n", ad->dev_name);
		for (i = 0; i < ad->max; i++) {
			name = (char *) mixer_get_name (1, i);
			fprintf (fp, "# Settings for the %s-device = ", name);
			if (mixer_is_stereo(1, i))
				fprintf(fp,"{ Int[0-100]:leftValue Int[0-100]:rightValue }\n");
			else
				fprintf (fp, "{ Int[0-100]:value }\n");

			fprintf (fp, "%s = { ", mixer_get_name(1, i));
			if (mixer_is_stereo(1, i)) {
				fprintf (fp, "%d %d }\n", mixer_get_vol_left(1,i),
					mixer_get_vol_right(1,i));
			} else {
				fprintf (fp, "%d }\n", mixer_get_vol_left(1,i));
			}
			fprintf (fp,
				"# show slider for device \"%s\" = Bool[True, False]:Map\n",
				name);
			if (GTK_WIDGET_VISIBLE(ad->devs[i].box))
				fprintf (fp, "%s_map = True\n", name);
			else
				fprintf (fp, "%s_map = False\n", name);
			label = GTK_LABEL(GTK_BIN(ad->devs[i].label)->child)->label;
			if (label) {
				fprintf (fp,"# label of the device \"%s\" = String:Label\n",
						name);
				fprintf (fp,"%s_label = \"%s\"\n\n", name, label);
			}
		}
		if (!GTK_WIDGET_VISIBLE(ad->menu)) {
			fprintf (fp, "[%s]\n", GLOBAL);
			fprintf (fp, "# we don't want to see the menu bar\n");
			fprintf (fp, "menu_map = False\n");
		}
		fclose (fp);
	}
}

/*
 * load settings from config file
 */
static void
cb_load (GtkWidget *w, gpointer data)
{
	AppData *ad = (AppData *) data;
	scf_id id;
	int i, left, right, map;
	char *name;
	char key[32];

	id = scf_init (ad->program, NULL);
	if (scf_read(id)) {
		for (i = 0; i < ad->max; i++) {
			name = (char *)mixer_get_name (1, i);
			if (scf_get_array_int_val (id, ad->dev_name, name, 0, &left)
				== SCF_TRUE) {
				/* mixer_set_vol_left (1, i, left); */
				gtk_adjustment_set_value (GTK_ADJUSTMENT(ad->devs[i].left),
					(gfloat)(100-left));
			}
			if (scf_get_array_int_val (id, ad->dev_name, name, 1, &right)
				== SCF_TRUE) {
				/* mixer_set_vol_right (1, i, right); */
				if (mixer_is_stereo (1, i))
				gtk_adjustment_set_value (GTK_ADJUSTMENT(ad->devs[i].right),
					(gfloat)(100-right));
			}
			sprintf (key, "%s_map", name);
			if (scf_get_bool_val (id, ad->dev_name, key, &map)) {
				if (map == SCF_FALSE) {
					gtk_widget_hide_all (ad->devs[i].box);
				} else {
					gtk_widget_show_all (ad->devs[i].box);
				}
			}
		}
	}
	scf_fini(id);
}

/*
 */
static void
cb_quit (GtkWidget *w, gpointer data)
{
	gtk_exit (0);
}

/*
 */
static void
cb_menu (GtkWidget *w, gpointer data)
{
	AppData *ad = (AppData *) data;
	if (ad) {
		if (GTK_WIDGET_VISIBLE(ad->menu)) {
			gtk_widget_hide_all (ad->menu);
		} else {
			gtk_widget_show_all (ad->menu);
		}
	}
}

/*
 * hide or view a named device
 */
static void
cb_hide (GtkWidget *w, gpointer data)
{
	mixer_t *mix = (mixer_t *)data;
	if (GTK_WIDGET_VISIBLE(mix->box)) {
		gtk_widget_hide_all (mix->box);
	} else {
		gtk_widget_show_all (mix->box);
	}
}

/*
 */
static void
cb_about (GtkWidget *w, gpointer data)
{
	static GtkWidget *dlg = NULL;
	GtkWidget *label;

	if (dlg)
		return;
	dlg = gtk_dialog_new ();
	gtk_signal_connect (GOBJ(dlg), "destroy", GSIG(gtk_widget_destroyed), &dlg);
	gtk_window_set_title (GTK_WINDOW(dlg), "About");
	label = gtk_label_new (_ABOUT_);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dlg)->action_area),label,TRUE,TRUE,0);
	gtk_widget_show_all (dlg);
}

/*
 */
static void
cb_help (GtkWidget *w, gpointer data)
{
	system ("rxvt -e man xmixer &");
}

static GtkItemFactoryEntry menu_items [] = {
	{"/_File",						NULL,		NULL,		0,	"<Branch>"},
	{"/File/_Save Settings",		"<ctrl>S",	cb_save,	(guint)&App},
	{"/File/_Load Settings",		"<ctrl>O",	cb_load,	(guint)&App},
	{"/File/<Separator>",			NULL,		NULL,		0,	"<Separator>"},
	{"/File/_Quit",					"<alt>Q",	cb_quit,	0},
	{"/_View",						NULL,		NULL,		0,	"<Branch>"},
	{"/View/_Menu",				"<alt>M",cb_menu,(guint)&App,	"<ToggleItem>"},
	{"/View/<Separator>",			NULL,		NULL,		0,	"<Separator>"},
	{"/_Help",						NULL,		NULL,		0,	"<Branch>"},
	{"/Help/About ..",				"<ctrl>A",	cb_about,	0},
	{"/Help/Manual ..",				"<ctrl>H",	cb_help,	0},
	{"/       ",					NULL,		NULL,		0,	"<Item>"},
};

/*
 * gui_main()
 */
int
gui_main (char *mixer_dev, char *pname)
{
	GtkWidget *top, *frame, *menubar, *dbox, *btn, *slider,
				*bbox, *scale_box, *toggle;
	GtkObject *adj;
	GtkAccelGroup *accel;
	int nmenu_items = sizeof (menu_items) / sizeof (*menu_items);
	GtkItemFactory *factory;
	GtkItemFactoryEntry if_entry;
	int no_of_devs, i;
	wMixer *devs;
	double vol_left, vol_right;
	scf_id id;
	int have_config;
	char key[32], path[64], *name, *p;
	int map;

	no_of_devs = mixer_num_of_devs (1);
	devs = (wMixer *) malloc (sizeof (wMixer) * no_of_devs);
	if (!devs) {
		perror ("malloc()");
		return ERROR;
	}
	App.program = pname;
	App.max = no_of_devs;
	App.devs = devs;
	App.dev_name = mixer_dev;
	App.tips = gtk_tooltips_new ();

	top = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_signal_connect (GOBJ(top), "destroy", GSIG(cb_quit), NULL);


	frame = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER(top), frame);

	accel = gtk_accel_group_new();
	factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<menu>", accel);
	gtk_item_factory_create_items (factory, nmenu_items, menu_items, NULL);
	gtk_accel_group_attach (accel, GOBJ(top));
	menubar = gtk_item_factory_get_widget (factory, "<menu>");
	gtk_box_pack_start (GTK_BOX(frame), menubar, FALSE, TRUE, 0);
	App.menu = menubar;
	
	dbox = gtk_hbox_new (TRUE, 5);
	gtk_box_pack_start (GTK_BOX(frame), dbox, TRUE, TRUE, 1);

	gtk_widget_show_all (top);

	App.label = gtk_item_factory_get_widget (factory, "/       ");
	App.label = GTK_BIN(App.label)->child;

	id = scf_init (App.program, NULL);
	if (scf_read (id))
		have_config = 1;
	else
		have_config = 0;

	for (i = 0; i < no_of_devs; i++) {
		devs[i].box = gtk_vbox_new (FALSE, 0);
		devs[i].dev = i;
		devs[i].mute = 0;
		gtk_box_pack_start (GTK_BOX(dbox), devs[i].box, TRUE, TRUE, 0);

		btn = gtk_button_new_with_label (mixer_get_label(1, i));
		gtk_box_pack_start (GTK_BOX(devs[i].box), btn, FALSE, TRUE, 0);
		gtk_signal_connect (GOBJ(btn), "clicked", GSIG(cb_label), &devs[i]);
		devs[i].label = btn;

		/* */
		scale_box = gtk_hbox_new (TRUE, 0);
		gtk_box_pack_start (GTK_BOX(devs[i].box), scale_box, TRUE, TRUE, 0);

		vol_left = vol_right = 100 - mixer_get_vol_left (1, i);
		adj = gtk_adjustment_new (vol_left, 0.0, 101.0, 1.0, 10.0, 1.0);
		gtk_signal_connect(GOBJ(adj), "value_changed", GSIG(cb_left), &devs[i]);
		slider = gtk_vscale_new (GTK_ADJUSTMENT (adj));
		gtk_scale_set_draw_value (GTK_SCALE(slider), FALSE);
		gtk_box_pack_start (GTK_BOX(scale_box), slider, TRUE, TRUE, 0);
		devs[i].left = adj;
		if (mixer_is_stereo (1, i)) {
			vol_right = 100 - mixer_get_vol_right (1, i);
			adj = gtk_adjustment_new (vol_right, 0.0, 101.0, 1.0, 10.0, 1.0);
			gtk_signal_connect(GOBJ(adj),
				"value_changed",GSIG(cb_right),&devs[i]);
			slider = gtk_vscale_new (GTK_ADJUSTMENT (adj));
			gtk_scale_set_draw_value (GTK_SCALE(slider), FALSE);
			gtk_box_pack_start (GTK_BOX(scale_box), slider, TRUE, TRUE, 0);
			devs[i].right = adj;
		}

		/* add the buttons add the end */
		bbox = gtk_hbox_new (TRUE, 0);
		gtk_box_pack_start (GTK_BOX(devs[i].box), bbox, FALSE, TRUE, 0);

		/* lock button */
		btn = gtk_toggle_button_new_with_label ("L");
		gtk_box_pack_start (GTK_BOX(bbox), btn, FALSE, TRUE, 0);
		if (!mixer_is_stereo (1, i)) {
			gtk_widget_set_sensitive (btn, 0);
		} else {
			if (vol_left == vol_right) {
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (btn), 1);
			}
		}
		devs[i].lock = btn;
		gtk_tooltips_set_tip (App.tips, btn, "Lock on/off", "/Tooltips/");

		/* rec button */
		btn = gtk_toggle_button_new_with_label ("R");
		gtk_box_pack_start (GTK_BOX(bbox), btn, FALSE, TRUE, 0);
		if (mixer_is_rec_dev (1, i)) {
			if (mixer_is_rec_on (1, i)) {
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (btn), 1);
			}
			gtk_signal_connect (GOBJ(btn), "toggled", GSIG(cb_rec), &devs[i]);
			gtk_tooltips_set_tip (App.tips, btn, "Record on/off", "/Tooltips/");
		} else {
			gtk_widget_set_sensitive (btn, 0);
		}
		gtk_widget_set_name (btn, "recButton");

		/* mute button */
		btn = gtk_toggle_button_new_with_label ("M");
		gtk_box_pack_start (GTK_BOX(bbox), btn, FALSE, TRUE, 0);
		gtk_signal_connect (GOBJ(btn), "toggled", GSIG(cb_mute), &devs[i]);
		gtk_tooltips_set_tip (App.tips, btn, "Mute on/off", "/Tooltips/");
		gtk_widget_set_name (btn, "muteButton");

		name = (char *)mixer_get_name (1, i);
		/* check if it should be shown */
		map = TRUE;
		if (have_config) {
			char *str;

			sprintf (key, "%s_map", name);
			if (scf_get_bool_val (id, App.dev_name, key, &map)) {
				if (map != FALSE) {
					gtk_widget_show_all (devs[i].box);
				}
			} else {
				gtk_widget_show_all (devs[i].box);
			}
			sprintf (key, "%s_label", name);
			str = scf_get_val (id, App.dev_name, key);
			if (str) {
				gtk_label_set_text (
					GTK_LABEL(GTK_BIN(devs[i].label)->child), str);
				name = str;
			}
		} else {
			gtk_widget_show_all (devs[i].box);
		}
		/* add menu entries */
		sprintf (path, "/View/%s", name);
		if_entry.path = path;
		if_entry.accelerator = NULL;
		if_entry.callback = cb_hide;
		if_entry.callback_action = (guint)&devs[i];
		if_entry.item_type = "<ToggleItem>";
		gtk_item_factory_create_item (factory, &if_entry, NULL, 1);
		toggle = gtk_item_factory_get_widget (factory, path);
		if (toggle) {
			if (map == TRUE) {
				GTK_CHECK_MENU_ITEM(toggle)->active = 1;
			}
		}
	}

	toggle = gtk_item_factory_get_widget (factory, "/View/Menu");
	map = TRUE;
	if (have_config) {
		if (scf_get_bool_val (id, GLOBAL, "menu_map", &map)) {
			if (map == FALSE)
				gtk_widget_hide_all (App.menu);
		}
	}
	if (map) {
		GTK_CHECK_MENU_ITEM(toggle)->active = 1;
	}
	scf_fini(id);

	p = strrchr (pname, '/');
	if (p)
		p++;
	else
		p = pname;

	name = malloc (strlen (p) + strlen (mixer_dev) + 10);
	if (name) {
		sprintf (name, "%s: %s", p, mixer_dev);
		gtk_window_set_title (GTK_WINDOW(top), name);
		free (name);
	}

	/* main loop */
	gtk_main ();
	return (ERROR);
}

