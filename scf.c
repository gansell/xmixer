/*
 * scf.c, SCFF library, tab=4
 *
 * Copyright (C) Rasca, Berlin 1998
 * EMail: thron@gmx.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>			/* fprintf() */
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>			/* getuid(), getpid() */
#include <pwd.h>
#include <stdarg.h>
#include <sys/utsname.h>	/* uname() */
#include <scf.h>

extern int strncasecmp (const char *s1, const char *s2, size_t n);
/* extern char * strdup (const char *s); */

/* undef if you don't have strdup()
 */
#define HAS_STRDUP

#define MAX_LINE			1024
#define SCF_COMMENT_CHAR	'#'
#define SCF_KVP_SEPARATOR	'='
#define SCF_HEADER			"#SCFF 1.0"

/* a section name
 */
typedef struct SectionName {
	string *names;
	int depth;
} scf_secnam;

/* data entry */
typedef struct Entry {
	int type;			/* type of entry, eg. SCF_STRING */
	int val_int;		/* used for INT, Bool and as counter for arrays */
	string val_str;		/* for string values */
	string key;
	string comment;
	struct Entry **array;
	struct Entry *next;
} scf_entry;

/* a section entry/node
 */
typedef struct Section {
	unsigned int no_of_childs;		/* number of children */
	unsigned int no_of_entries;
	string comment;
	scf_secnam *sn;				/* section name */
	struct Section *previous;
	struct Section *child;
	struct Section *neighbour;
	struct Entry *entries;
} scf_section;

typedef struct Scf {
	unsigned int id;
	unsigned int mode;
	unsigned char key_value_separator;
	unsigned char comment_char;
	unsigned char array_chars[2];	/* left and right array character */
	unsigned char array_separator;
	scf_section *root;
	/* */
	char *fname;
	FILE *fp;
	/* for the linked list
	 */
	struct Scf *next;
	struct Scf *prev;
} SCF;


/* for the linked list of used SCFF files
 */
static SCF *first = NULL;
static SCF *last = NULL;
static int last_id = 0;

/*
 * multi free(), returns allways NULL
 */
void *
mfree (int num, void *ptr, ...)
{
	va_list ap;
	va_start (ap, ptr);
	while (num--) {
		free (ptr);
		if (num)
			ptr = va_arg(ap, void*);
	}
	va_end(ap);
	return (NULL);
}

/*
 * parse a section name and return a filled "sn" structure
 */
static scf_secnam *
parse_section_name (const string name)
{
	scf_secnam *sn;
	string buff, p;

	sn = (scf_secnam *) calloc (1, sizeof (scf_secnam));
	if (!sn)
		return (NULL);
	if (name == SCF_ROOT) {
		sn->depth = 0;
		sn->names = SCF_ROOT;
	} else {
		sn->depth = 1;
		buff = (string) malloc (strlen(name)+1);
		if (!buff) {
			free (sn);
			return (NULL);
		}
		strcpy (buff, name);
		p = strtok (buff, ".");
		if (p) {
#ifdef DEBUG_SCF
			fprintf (stderr, "parse_section_name() %s.", p);
#endif
			sn->names = (string *) malloc ((sn->depth) * sizeof (string));
			if (!sn->names)
				return ((scf_secnam *)mfree(2,buff,sn));
			sn->names[0] = (string) malloc ((strlen(p)+1) * sizeof (uchar));
			if (!sn->names[0])
				return ((scf_secnam *)mfree(3,buff,sn->names,sn));
			strcpy (sn->names[0], p);
			while (1) {
				p = strtok (NULL, ".");
				if (!p)
					break;
				sn->depth++;
				sn->names = (string *) realloc (
									sn->names, sn->depth * sizeof(string));
				if (!sn->names)
					return ((scf_secnam *)mfree(2, buff, sn));
				sn->names[sn->depth-1] = (string) malloc (strlen(p)+1);
				strcpy (sn->names[sn->depth-1], p);
#ifdef DEBUG_SCF
				fprintf (stderr, "%s.", p);
#endif
			}
		}
		free (buff);
#ifdef DEBUG_SCF
		fprintf (stderr, "(%d)\n", sn->depth);
#endif
	}
	return (sn);
}

/*
 */
static void
delete_section_name (scf_secnam *sn)
{
	while (sn->depth--) {
		free (sn->names[sn->depth]);
	}
	free (sn->names);
	free (sn);
}

/*
 * alloc RAM for a new section and initialize with
 * the section name: section.subsection[.*]
 */
static scf_section *
new_section (SCF *scf, const string name)
{
	scf_section *sec;

	sec = (scf_section *) calloc (1, sizeof (scf_section));
	if (!sec) {
		return (NULL);
	}
	sec->sn = parse_section_name (name);
	if (!sec->sn)
		return (NULL);
#ifdef DEBUG_SCF
	fprintf (stderr, "new_section() %s (%d)\n", sec->sn->names[0], sec->sn->depth);
#endif
	return (sec);
}

/*
 * free the RAM of a section, will also destroy all data entries
 */
static void
delete_section (scf_section *sec)
{
	scf_section *child;
	delete_section_name (sec->sn);
	while (sec->no_of_childs--) {
		child = sec->child;
		sec->child = child->neighbour;
		delete_section (child);
	}
	free (sec->child);
	free (sec);
}


/*
 * create and initialize a new SCF structure
 */
static SCF *
new_scf (void)
{
	SCF *scf;
	scf = (SCF *) calloc (1, sizeof (SCF));
	if (scf) {
		scf->id = ++last_id;
		scf->comment_char = SCF_COMMENT_CHAR;
		scf->key_value_separator = SCF_KVP_SEPARATOR;
		scf->array_chars[0] = '{';
		scf->array_chars[1] = '}';
		scf->array_separator= SCF_WHITE;
		scf->root = new_section (scf, SCF_ROOT);
	}
	return (scf);
}

/*
 * delete a SCF structure
 */
static void
delete_scf (SCF *scf)
{
	delete_section (scf->root);
	free (scf->fname);
	if (scf->id == last_id)
		last_id--;
	free (scf);
}

/*
 * add a SCF structure to the linked list
 */
static void
add_to_list (SCF *scf)
{
	if (first == NULL) {
		first = scf;
		last = scf;
		last->next = NULL;
		first->prev= NULL;
	} else {
		last->next = scf;
		scf->prev = last;
		last = scf;
	}
}

/*
 * remove a scf structure from the linked list of the SCFs
 */
static void
remove_from_list (SCF *scf)
{
	if (scf->prev != NULL) {
		scf->prev->next = scf->next;
	}
	if (scf->next != NULL) {
		scf->next->prev = scf->prev;
	}
	if (scf == first)
		first = scf->next;
	if (scf == last)
		last = scf->prev;
}

/*
 * find the corresponding SCF structure named by the id
 */
static SCF *
find_scf (int id)
{
	SCF *tmp;
	tmp = first;
	while (tmp) {
		if (tmp->id == id)
			return (tmp);
		tmp = tmp->next;
	}
	return (NULL);
}

/*
 */
static scf_entry *
new_entry (void)
{
	scf_entry *se;
	se = (scf_entry *) calloc (1, sizeof(scf_entry));
	return (se);
}

/*
 */
int
scf_set_mode (scf_id id, int mode, const char *kvps, const char *comm )
{
	SCF *scf;

	scf = find_scf (id);
	if (!scf) {
		return (SCF_NO_SCF);
	}
	switch (mode) {
		case SCF_STD:
			scf->key_value_separator = SCF_KVP_SEPARATOR ;
			scf->comment_char = SCF_COMMENT_CHAR;
			break;
		case SCF_CAP:
			scf->key_value_separator = ':';
			scf->comment_char = '#';
			break;
		case SCF_X11:
			scf->key_value_separator = ':';
			scf->comment_char = '!';
			break;
		case SCF_INI:
			scf->key_value_separator = '=';
			scf->comment_char = ';';
			break;
		default:
			fprintf (stderr, "scf_set_mode() Unknown mode: %d!\n", mode);
			return (SCF_FALSE);
			break;
	}
	scf->mode = mode;
	if (kvps)
		scf->key_value_separator = *kvps;
	if (comm)
		scf->comment_char = *comm;
	return (SCF_TRUE);
}


/*
 */
int
scf_set_array_mode (scf_id id, const char *aoc, const char *as)
{
	SCF *scf;

	scf = find_scf (id);
	if (!scf) {
		return (SCF_NO_SCF);
	}
	if (aoc) {
		if (*aoc == '\0') {
			scf->array_chars[0] = aoc[0];
			scf->array_chars[0] = aoc[0];
		} else {
			scf->array_chars[0] = aoc[0];
			scf->array_chars[1] = aoc[1];
		}
	}
	if (as) {
		scf->array_separator = *as;
	}
	return (SCF_TRUE);
}


/*
 */
const char *
scf_file_name (const char *pname, int flags)
{
	static char buff[PATH_MAX+NAME_MAX+1];
	char *p, *home;
	int len;

	*buff = '\0';
	if (!pname)
		return (NULL);
	p = strrchr (pname, '/');
	if (flags == SCF_PRIVAT) {
		home = getenv ("HOME");
		if (home) {
			len = strlen (home);
			if (len < PATH_MAX) {
				strcpy (buff, home);
			}
		}
		strcat (buff, "/.");
		if (p != NULL) {
			strcat (buff, p+1);
		} else {
			strcat (buff, pname);
		}
	} else {
		/* SCF_SYSTEM */
		if (p != NULL) {
			strncpy (buff, pname, p-pname+1);
			strcpy (buff+(p-pname+1), "../etc/");
			strcat (buff, p+1);
		} else {
			strcpy (buff, "../etc/");
			strcat (buff, pname);
		}
	}
	strcat (buff, ".scf");
#ifdef DEBUG_SCF
	fprintf (stderr, "scf_file_name(): %s\n", buff);
#endif
	return (buff);
}

/*
 * this must be called first!
 */
unsigned int
scf_init (const char *pname, const char *scf_file)
{
	SCF *scf;
	int len;

	scf = new_scf();
	if (!scf)
		return (SCF_FALSE);
	add_to_list (scf);
	if (scf_file == NULL) {
		scf_file = scf_file_name (pname, SCF_PRIVAT);
	} else if (scf_file == pname) {
		scf_file = scf_file_name (pname, SCF_SYSTEM);
	}
	len = strlen (scf_file);
	scf->fname = (char *) malloc (len+1);
	if (!scf->fname) {
		remove_from_list (scf);
		delete_scf (scf);
		return (SCF_FALSE);
	}
	strcpy (scf->fname, scf_file);
#ifdef DEBUG_SCF
	fprintf (stderr, "scf_init() %s\n", scf->fname);
#endif
	return (scf->id);
}

/*
 */
int
scf_fini (unsigned int id)
{
	SCF *scf;
	scf = find_scf (id);
	if (!scf)
		return (SCF_NO_SCF);
	if (scf->fp)
		fclose (scf->fp);
	remove_from_list (scf);
	delete_scf (scf);
	return (SCF_TRUE);
}

/*
 * for raw access
 */
int scf_open (unsigned int id, int mode) {
	SCF *scf;

	scf = find_scf(id);
	if (!scf) {
		return (SCF_NO_SCF);
	}
	switch (mode) {
		case SCF_READ:
			scf->fp = fopen (scf->fname, "rb");
			break;
		case SCF_WRITE:
			scf->fp = fopen (scf->fname, "wb");
			break;
		case SCF_APPEND:
			scf->fp = fopen (scf->fname, "wb+");
			break;
	}
	if (!scf->fp) {
#ifdef DEBUG_SCF
		perror (scf->fname);
#endif
		return (SCF_IO_ERR);
	}
	return (scf->id);
}

/*
 */
int
scf_close (unsigned int id)
{
	SCF *scf;

	scf = find_scf (id);
	if (!scf) {
		return (SCF_NO_SCF);
	}
	fclose (scf->fp);
	scf->fp = NULL;
	return (SCF_TRUE);
}

/*
 * write all the data out
 */
int
scf_sync (scf_id id) {
	SCF *scf;
	scf = find_scf (id);
	if (!scf)
		return (SCF_NO_SCF);
	scf->fp = fopen (scf->fname, "wb");
	if (!scf->fp)
		return (SCF_FALSE);
	fprintf (scf->fp, "%s\n\n", SCF_HEADER);
	fclose (scf->fp);
	return (SCF_TRUE);
}

/*
 */
int
scf_strip_eol (string buff)
{
	string p;
	int len;

	if (!buff) {
		/* null pointer
		 */
		return (SCF_NO_PTR);
	}
	len = strlen (buff);
	if (!len) {
		/* empty string
		 */
		return (SCF_NO_VAL);
	}
	p = buff+len-1;
	if ((*p == '\n') || (*p == '\r')) {
		*p-- = '\0';
		len--;
		if (p > buff) {
			if ((*p == '\n') || (*p == '\r')) {
				/* for dos files (\r\n)
				 */
				*p = '\0';
				len--;
			}
		}
		return (len);
	}
	return (SCF_NO_EOL);
}


/*
 * read the next line and strip of the newline characters at the end
 * of the line. returned string must be freed.
 */
static int
read_line (SCF *scf, unsigned char **rbuff)
{
	static unsigned char buff[MAX_LINE+2];
	int len;

	*rbuff = NULL;
	if (fgets (buff, MAX_LINE+1, scf->fp) == NULL)
		return (SCF_NO_VAL);

	len = scf_strip_eol (buff);
	if (len == SCF_NO_EOL) {
		fprintf (stderr, "Error: line to long!?\n");
		return (SCF_NO_EOL);
	}
	*rbuff = (unsigned char *) malloc (len+1);
	if (!*rbuff) {
		return (SCF_NO_RAM);
	}
	strcpy (*rbuff, buff);
#ifdef DEBUG_SCF
	fprintf (stderr, "read_line(): rbuff=%s\n", *rbuff);
#endif
	return (len);
}

/*
 */
static int
next_line (SCF *scf, string *rline)
{
	unsigned char *iline = NULL;
	unsigned char *oline = NULL;
	unsigned char *p;
	int ilen, olen = SCF_NO_VAL, spaces, first;

	*rline = NULL;
	first = SCF_TRUE;

	while (SCF_TRUE) {
		ilen = read_line (scf, &iline);
		if (first && ( ilen == SCF_NO_VAL)) {
			return (SCF_NO_VAL);
		}
		if (first && ( ilen == 0)) {
			/* skip empty lines */
			continue;
		}
		if (ilen == SCF_NO_RAM) {
			free (iline);
			free (rline);
			return (SCF_NO_RAM);
		}
		if (ilen > 0) {
			if (first) {
				oline = malloc (ilen+1);
				strcpy (oline, iline);
				olen = ilen;
				first = 0;
			} else {
				oline = realloc (oline, olen+ilen+1);
				p = iline;
				while (*p) {
					if ((*p == ' ') || (*p == '\t')) {
						p++;
						ilen--;
					} else
						break;
				}
				strcat (oline, p);
				olen = olen+ilen;
			}
			free (iline);
		}
		if (oline[olen-1] == '\\') {
			oline[olen-1] = '\0';
			olen--;
			continue;
		}
		break;
	}
	/* check for double :: in termcap line */
	if (scf->mode == SCF_CAP) {
		unsigned char *ep;
		p = oline;
		ep= p+olen;
		while (*p) {
			if ((*p == ':') && (*(p+1) == ':')) {
				memmove (p, p+1, ep-p);
				olen--;
			}
			p++;
		}
	}
	/* remove spaces */
	p = oline;
	spaces = 0;
	while (*p && ((*p == ' ') || (*p == '\t'))) {
		spaces++;
		p++;
	}
	if (spaces) {
		olen = olen - spaces;
		memmove (oline, oline+spaces, olen+1);
	}
	p = oline+olen-1;
	while (p > oline && (*p == ' ' || *p == '\t')) {
		olen--;
		*p-- = '\0';
	} 

	*rline = oline;
#ifdef DEBUG_SCF
	fprintf (stderr, "next_line() line=\"%s\"\n", oline);
#endif
	return (olen);
}

/*
 * for raw access: skippes comment and empty lines, merges breaked lines
 * with the '\' character and removes CR and NL characters at the end
 */
int
scf_next_line (unsigned int id, string *rline)
{
	SCF *scf;

	scf = find_scf (id);
	if (!scf)
		return (SCF_NO_SCF);
	return (next_line (scf, rline));
}

/*
 */
static int
strip_quotes (uchar *s, char c)
{
	int len = 0;

	if (!s || !*s)
		return (SCF_FALSE);
	len = strlen (s);
	if (*s == c) {
		if (s[len-1] == c) {
			s[len-1] = '\0';
			memmove (s, s+1, len-1);
			len -= 2;
		} else {
			fprintf (stderr, "strip_quotes() can't find ending quotes!\n");
			return (SCF_FALSE);
		}
	}
	return (len);
}

/*
 */
unsigned char *un_escape_val (SCF *scf, unsigned char *val) {
	unsigned char buff[256], *nval;
	int len0, len1, i, j, len;
	uid_t uid;
	pid_t pid;
	struct utsname uts;
	struct passwd *pw;

	if (!val)
		return (NULL);
	len0 = len1 = strlen (val);
	nval = (uchar *) malloc (len0+1);
	if (!nval)
		return (NULL);
	for (i = 0, j = 0; i < len0; i++) {
		if (val[i] == '\\') {
			switch (val[i+1]) {
				case '\\':
				case '"':
					nval[j++] = val[i+1];
					len1--;
					i++;
					break;
				case 'n':
					/* NL */
					nval[j++] = 0x0A;
					len1--;
					i++;
					break;
				case 'r':
					/* CR */
					nval[j++] = 0x0D;
					len1--;
					i++;
					break;
				case 'h':
					/* hostname */
					uname (&uts);
					len = strlen (uts.nodename);
					len1 = len1 - 2 + len;
					nval = realloc (nval, len1+1);
					strcpy (nval+j, uts.nodename);
					j += len;
					i++;
					break;
				case 'o':
					/* operating system name */
					uname (&uts);
					len = strlen (uts.sysname);
					len1 = len1 - 2 + len;
					nval = realloc (nval, len1+1);
					strcpy (nval+j, uts.sysname);
					j += len;
					i++;
					break;
				case 'm':
					/* machine type */
					uname (&uts);
					len = strlen (uts.machine);
					len1 = len1 - 2 + len;
					nval = realloc (nval, len1+1);
					strcpy (nval+j, uts.machine);
					j += len;
					i++;
					break;
				case 'd':
					/* home directory */
					uid = getuid();
					pw = getpwuid (uid);
					len = strlen (pw->pw_dir);
					len1 = len1 - 2 + len;
					nval = realloc (nval, len1+1);
					strcpy (nval+j, pw->pw_dir);
					j += len;
					i++;
					break;
				case 'u':
					/* login name */
					uid = getuid();
					pw = getpwuid (uid);
					len = strlen (pw->pw_name);
					len1 = len1 - 2 + len;
					nval = realloc (nval, len1+1);
					strcpy (nval+j, pw->pw_name);
					j += len;
					i++;
					break;
				case 'p':
					/* process id */
					pid = getpid ();
					sprintf (buff, "%d", pid);
					len = strlen (buff);
					len1 = len1 - 2 + len;
					nval = realloc (nval, len1+1);
					strcpy (nval+j, buff);
					j+= len;
					i++;
					break;
				default:
					nval[j++] = val[i];
					break;
			}
		} else {
			nval[j++] = val[i];
		}
	}
	nval[len1] = '\0';
	return (nval);
}

/*
 * check for a boolean value in string 's'
 * return values:
 *	0: the string contains no boolean value
 *	1 = boolean value is no
 *	2 = boolean value is yes
 */
static int
str_is_bool (const uchar *s, int len)
{
	uchar *p[] = { "FALSE", "NO", "TRUE", "YES" };
	int i;

	for (i = 0; i < 4; i++) {
		if (strncasecmp (s, p[i], len) == 0)
			return (i < 2 ? 1 : 2);
	}
	return (0);
}

/*
 * looks for pattern like "0xHH0xHH[..]" or "\OOO\OOO[..]"
 */
static int
str_is_binary (const uchar *s, int len)
{

	if (len < 8)
		return (0);
	if (*s == '\\') {
		/* octal */
		if (strchr (s+1, '\\'))
			return (1);
	} else if (*s == '0' && *(s+1) == 'x') {
		/* hex */
		if (strchr (s+2, 'x'))
			return (1);
	}
	return (0);
}

/*
 */
static int
add_array_element (scf_entry *en, string line, int type) {
	int len = 0;
	string p;
	scf_entry *new;

	new = new_entry ();
	new->type = type;
	en->val_int++;
	if (!en->array)
		en->array = (scf_entry **) malloc (en->val_int * sizeof (scf_entry *));
	else
		en->array = (scf_entry **) realloc (en->array, en->val_int * sizeof (scf_entry *));
	en->array[en->val_int-1] = new;
	switch (type) {
		case SCF_STRING:
			p = strrchr (line, '"');
			if (!p)
				return (0);
			break;
		case SCF_INT:
			p = line;
			while (*p && *p != ' ')
				p++;
			len = p - line;
			sscanf (line, "%d", &new->val_int);
#ifdef DEBUG_SCF
			fprintf (stderr, "add_array_element() %s->%d (len=%d)\n",
						line, new->val_int, len);
#endif
			break;
		default:
			printf ("oops\n");
			break;
	}
	return (len);
}

/*
 */
static int
key_value_pair (SCF *scf, uchar *line, scf_entry *entry)
{
	uchar *p, *s;
	int len;

	if (!line)
		return (SCF_FALSE);
	s = strchr (line, scf->key_value_separator);
	if (!s)
		return (SCF_FALSE);

	/* process the value part
	 */
	p = s + 1;
	while ((*p == ' ') || (*p == '\t')) {
		p++;
	}
	len = strlen (p);
	if (scf->mode & SCF_INI) {
		/* in this mode we use only strings
		 */
		entry->val_str = (uchar *) malloc (len+1);
		if (entry->val_str)
			strcpy (entry->val_str, p);
	} else if (*p == '"') {
		/* it's a string
		 */
		strip_quotes (p, '"');
		entry->val_str = un_escape_val (scf, p);
		entry->type = SCF_STRING;
	} else if ((entry->val_int = str_is_bool (p, len)) > 0) {
		/* it's a boolean
		 */
		if (entry->val_int == 1) {
			entry->val_int = 0;
		} else {
			entry->val_int = 1;
		}
		entry->type = SCF_BOOL;
	} else if (str_is_binary (p, len)) {
		/* it's a binary
		 */
		entry->type = SCF_BIN;
	} else if (*p == scf->array_chars[0]) {
		/* it's a array
		 */
		entry->type = SCF_ARRAY;
		p[len-1] = '\0';
		p++;
		/* count array elements
		 */
		while (*p) {
			while (*p == ' ')
				p++;
			if (*p == '"') {
				/* it's a string .. */
				len = add_array_element (entry, p+1, SCF_STRING);
				if (!len)
					goto STOP;
				p += len;
			} else {
				/* INT ? */
				len = add_array_element (entry, p, SCF_INT);
				if (!len)
					goto STOP;
				p += len;
			}
			while (*p && *p != ' ')
				p++;
			if (*(p+1) == '\0')
				break;
		}
		STOP:
	} else {
		/* it's a int
		 */
		sscanf (p, "%d", &entry->val_int);
		entry->type = SCF_INT;
#ifdef DEBUG_SCF
		fprintf (stderr, "kvp() %s->%d\n", p, entry->val_int);
#endif
	}

	/* process the key part
	 */
	p = s - 1;
	*(p+1) = '\0';
	while ((p > line) && ((*p == '\t') || (*p == ' '))) {
		*p = '\0';
		p--;
	}
	p = line;
	while ((*p == ' ') || (*p == '\t' )) {
		p++;
	}
	len = strlen (p);
	entry->key = (uchar *) malloc (len+1);
	if (!entry->key) {
		free (entry->val_str);
		return (SCF_FALSE);
	}
	strcpy (entry->key, p);
	return (SCF_TRUE);
}

/*
 * find the parent of section 'sec' starting at section 'root'
 */
scf_section *
find_parent_section (scf_section *root, scf_section *sec)
{
	scf_section *tsec, *t2sec;
	int i, level = 0;

	if (!root || !sec) {
		return (NULL);
	}
	if (sec->sn->depth == 1)
		return (root);

	tsec = root;
	NEXT:
#ifdef DEBUG_SCF
	fprintf (stderr, "find_parent_section() %s.%s (%d)\n",
				sec->sn->names[0], sec->sn->names[1], sec->sn->depth);
#endif
	while (level < (sec->sn->depth - 1)) {
		t2sec = tsec->child;
		for (i = 0; i < tsec->no_of_childs; i++) {
			if (strcmp (t2sec->sn->names[level], sec->sn->names[level]) == 0) {
				tsec = t2sec;
				level++;
				if (level == sec->sn->depth - 1) {
#ifdef DEBUG_SCF
					fprintf (stderr, "find_parent_section() %d %s\n",
						level, sec->sn->names[level]);
#endif
					return (tsec);
				}
				goto NEXT;
			}
			t2sec = t2sec->neighbour;
		}
		level++;
	}
	return (NULL);
}

/*
 * add a named section
 */
int
add_section (SCF *scf, scf_section *sec)
{
	scf_section *tsec, *t2sec;

	tsec = find_parent_section (scf->root, sec);
	if (tsec) {
		t2sec = tsec->child;
		if (!t2sec) {
			tsec->child = sec;
		} else {
			while (t2sec->neighbour) {
				t2sec = t2sec->neighbour;
			}
			t2sec->neighbour = sec;
		}
		sec->neighbour = NULL;
		tsec->no_of_childs++;
		return (SCF_TRUE);
	} else {
		fprintf (stderr, "can't find parent section\n");
	}
	return (SCF_FALSE);
}

/*
 * add a data entry to named section
 */
int
add_entry (scf_section *sec, scf_entry *node)
{
	scf_entry *ten;

	sec->no_of_entries++;
#ifdef DEBUG_SCF
	fprintf (stderr, "add_entry() sec=%s noe=%d\n",
			sec->sn->depth ? sec->sn->names[0]:"ROOT", sec->no_of_entries);
#endif
	ten = sec->entries;
	if (!ten) {
		sec->entries = node;
	} else {
		while (ten) {
			if (!ten->next)
				break;
			ten = ten->next;
		}
		ten->next = node;
	}
	node->next = NULL;
	return (SCF_TRUE);
}

/*
 * find the named section in the tree
 */
static scf_section *
find_section (SCF *scf, scf_secnam *sn)
{
	scf_section *sec = scf->root, *child;
	int level;

	if (sn->depth == 0) {
		return (sec);
	}
	level = 0;
	child = sec->child;
	while (child && (level < sn->depth))  {
		if (strcmp (child->sn->names[level], sn->names[level]) == 0) {
			if (child->sn->depth == sn->depth)
				return (child);
			else {
				child = child->child;
				level++;
				continue;
			}
		}
		child = child->neighbour;
	}
	return (NULL);
}

/*
 */
static scf_section *
find_section_by_name (SCF *scf, const string section)
{
	scf_section *sec = NULL;
	scf_secnam *sn;

	if (section == SCF_ROOT)
		return (scf->root);
	sn = parse_section_name (section);
	if (sn)
		sec = find_section (scf, sn);
	delete_section_name (sn);
	return (sec);
}

/*
 * used for INT and BOOL entries
 */
static int
get_int_val (SCF *scf, const string section, const string key, int type, int *rv)
{
	scf_section *sec;
	scf_entry *en;
	int i;

	if (!scf || !key)
		return (SCF_FALSE);
	sec = find_section_by_name (scf, section);
	if (!sec) {
#ifdef DEBUG_SCF
		fprintf (stderr, "get_int_val(): Can't find section: %s\n", section);
#endif
		return (SCF_FALSE);
	}
	en = sec->entries;
	for (i =0; i < sec->no_of_entries; i++) {
		if (en->type == type) {
#ifdef DEBUG_SCF
			fprintf (stderr, "** sec=%s noe=%d key=%s en->key=%s\n",
						sec->sn->depth?sec->sn->names[0]:"*",
						sec->no_of_entries, key, en->key);
#endif
			if (strcmp (en->key, key) == 0) {
				*rv = en->val_int;
				return (SCF_TRUE);
			}
		}
		en = en->next;
	}
	return (SCF_FALSE);
}

/*
 */
static string
get_string_val (SCF *scf, const string section, const char *key)
{
	scf_section *sec;
	scf_entry *en;
	int i;
	unsigned char *value;

	if (!scf || !key)
		return (NULL);
	sec = find_section_by_name (scf, section);
	if (!sec) {
#ifdef DEBUG_SCF
		fprintf (stderr, "Can't find section: %s\n", section);
#endif
		return (NULL);
	}
	en = sec->entries;
	for (i = 0; i < sec->no_of_entries; i++) {
#ifdef DEBUG_SCF
		fprintf (stderr, "get_string_val() i=%d %s %d\n", i, en->key, en->type);
#endif
		if (en->type == SCF_STRING) {
			if (strcmp (en->key, key) == 0)
			{
				value = un_escape_val (scf, en->val_str);
				return (value);
			}
		}
		en = en->next;
	}
	return (NULL);
}

/*
 */
static int
get_array_int_val (SCF *scf, const string section, const string key, int n, int *val)
{
	scf_section *sec;
	scf_entry *en;
	int i;

	if (!scf || !key)
		return (SCF_FALSE);
	sec = find_section_by_name (scf, section);
	if (!sec) {
		return (SCF_FALSE);
	}
	en = sec->entries;
	for (i = 0; i < sec->no_of_entries; i++) {
#ifdef DEBUG_SCF
		fprintf (stderr, "get_array_int_val() key=%s en->key=%s type=%d\n",
					key, en->key, en->type);
#endif
		if (en->type == SCF_ARRAY) {
			if (strcmp (en->key, key) == 0) {
				if (n <= en->val_int) {
					*val = en->array[n]->val_int;
					return (SCF_TRUE);
				}
			}
		}
		en = en->next;
	}
	return (SCF_FALSE);
}

/*
 */
static string
get_array_val (SCF *scf, const string section, const string key, int n)
{
	return (NULL);
}

/*
 * read a complete SCFF file into RAM
 */
int
scf_read (scf_id id)
{
	SCF *scf;
	int len, ptype;
	string line, pline;
	scf_section *base, *sec;
	scf_entry *entry;
	scf_secnam *sn;

	scf = find_scf (id);
	if (!scf)
		return (SCF_NO_SCF);
	scf->fp = fopen (scf->fname, "rb");
	if (!scf->fp) {
#ifdef DEBUG
		perror (scf->fname);
#endif
		return (SCF_FALSE);
	}
	sec = base = scf->root;

	pline = NULL;
	ptype = 0;
	while ((len = next_line (scf, &line)) > 0) {
		if (*line == scf->comment_char) {
			/* comment
			 */
#ifdef DEBUG_SCF
			fprintf (stderr, "C:%s\n", line);
#endif
			if (pline != NULL)
				free (pline);
			pline = strdup (line);
			ptype = SCF_COMMENT;

		} else if ((*line == '[') && (line[len-1] == ']')) {
			/* it's a section
			 */
#ifdef DEBUG_SCF
			fprintf (stderr, "S:%s\n", line);
#endif
			line[len-1] = '\0';
			sn = parse_section_name (line+1);
			if ((sec = find_section (scf, sn)) != NULL) {
				/* dupe */
				base = sec;
			} else {
				sec = new_section (scf, line+1);
				if (sec) {
					add_section (scf, sec);
					if (ptype == SCF_COMMENT) {
						sec->comment = pline;
						pline = NULL;
					}
					ptype = SCF_SECTION;
					base = sec;
				}
			}
			free (sn);
		} else {
			/* data entries ??
			 */
#ifdef DEBUG_SCF
			fprintf (stderr, "D:%s\n", line);
#endif
			entry = new_entry ();
			if (key_value_pair (scf, line, entry) == SCF_TRUE) {
			/* key value pairs
			 */
				add_entry (base, entry);
				if (ptype == SCF_COMMENT) {
					entry->comment = pline;
					pline = NULL;
				}
				ptype = entry->type;
			}
		}
		free (line);
	}
	if (pline)
		free (pline);
	fclose (scf->fp);
	scf->fp = NULL;
	return (SCF_TRUE);
}

/*
 * add a named section
 */
int
scf_add_section (scf_id id, char *sec, char *comment)
{
	SCF *scf;
	scf_section *se;

	scf = find_scf (id);
	if (!scf)
		return (SCF_FALSE);
	if (find_section_by_name (scf, sec) == NULL) {
		/* add it .. */
		se = new_section (scf, sec);
		if (se) {
			add_section (scf, se);
			if (comment)
			se->comment = strdup (comment);
		}
	}
	return (SCF_FALSE);
}

/*
 */
int
scf_has_section (scf_id id, char *sec)
{
	SCF *scf;
	scf = find_scf(id);
	if (!scf)
		return (SCF_FALSE);
	if (find_section_by_name (scf, sec) == NULL)
		return (SCF_FALSE);
	return (SCF_TRUE);
}

/*
 * not ready!
 */
int
scf_section_has_key (scf_id id, const char *sec, const char *key)
{
	return (SCF_FALSE);
}

/*
 */
string
scf_get_val (scf_id id, const string section, const string key)
{
	SCF *scf;
	scf = find_scf (id);
	if (!scf)
		return (NULL);
	return (get_string_val (scf, section, key));
}

/*
 */
string
scf_get_array_val (scf_id id, const string section, const string key, int n)
{
	SCF *scf;
	scf = find_scf (id);
	if (!scf)
		return (NULL);
	return (get_array_val (scf, section, key, n));
}

/*
 */
int
scf_get_array_int_val (scf_id id, const string section, const string key, int n, int *val)
{
	SCF *scf;

	scf = find_scf (id);
	if (!scf)
		return (SCF_FALSE);
	return (get_array_int_val (scf, section, key, n, val));
}

/*
 */
int
scf_get_bool_val (scf_id id, const string section, const string key, int *rv)
{
	SCF *scf;

	scf = find_scf (id);
	if (!scf)
		return (SCF_FALSE);
	return (get_int_val (scf, section, key, SCF_BOOL, rv));
}

/*
 */
int
scf_get_int_val (scf_id id, const string section, const string key, int *rv)
{
	SCF *scf;

	scf = find_scf (id);
	if (!scf)
		return (SCF_FALSE);
	return (get_int_val (scf, section, key, SCF_INT, rv));
}

/*
 */
double
scf_get_val_as_double (scf_id id, const string section, const string key)
{
	SCF *scf;
	unsigned char *s;
	double i;

	scf = find_scf (id);
	if (!scf)
		return (0);
	s = get_string_val (scf, section, key);
	if (!s)
		return (0);
	sscanf (s, "%lf", &i);
	free (s);
	return (i);
}


/*
 * changed the comment char for the named file (id)
 */
void
scf_set_comment_char (unsigned int id, char c)
{
	SCF *scf;

	scf = find_scf (id);
	if (scf)
		scf->comment_char = c;
}

/*
 */
void
scf_set_key_value_separator (unsigned int id, char c)
{
	SCF *scf;

	scf = find_scf (id);
	if (scf)
		scf->key_value_separator = c;
}

/*
 * returns the number of subsections or 0 if there are no
 * subsections
 */
int
scf_no_of_subsections (scf_id id, const string section)
{
	SCF *scf;
	scf_secnam *sn;
	scf_section *sec;

	scf =  find_scf (id);
	if (!scf)
		return (SCF_FALSE);
	sn = parse_section_name (section);
	sec = find_section (scf, sn);
	delete_section_name (sn);
	if (!sec)
		return (SCF_FALSE);
	return (sec->no_of_childs);
}

/*
 */
int
scf_get_type (scf_id id, const string section, const string key)
{
	SCF *scf;

	scf = find_scf (id);
	if (!scf)
		return (SCF_FALSE);
	return (SCF_FALSE);
}

/*
 * add a key if needed and add the value
 */
int
scf_set_val (scf_id id, string section, string key, string val, string comment)
{
	return (SCF_FALSE);
}

/*
 */
#ifndef HAS_STRDUP
char *
strdup (const char *s)
{
	int len;
	char *r;

	if (!s)
		return (NULL);
	len = strlen (s);
	r = (char *) malloc (len +1);
	if (!r)
		return (NULL);
	strcpy (r, s);
	return (r);
}
#endif

