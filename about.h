/*
 * xmixer:about.h
 *
 * Copyright (C) 1997-98 Rasca, Berlin
 * EMail: thron@gmx.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __ABOUT_H__
#define __ABOUT_H__
#define _ABOUT_ \
			"xmixer/gmixer, version " VERSION \
			"\n(c) rasca (thron@gmx.de), berlin 1997-99\n" \
			"published under the GNU GPL\n" \
			"http://home.pages.de/~rasca/xmixer/"
#endif

