/*
 * scf.h
 *
 * Copyright (C) 1997 Rasca, Berlin 1997-99
 * EMail: thron@gmx.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Library General Public License as published
 * by the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _SCF_H_
#define _SCF_H_	1

#define SCF_READ	1
#define SCF_WRITE	2
#define SCF_APPEND	4

#define SCF_STD		1
#define SCF_CAP		2
#define SCF_X11		4
#define SCF_INI		8
#define SCF_RAW		16
#define SCF_RES1	256

#define SCF_SYSTEM	1
#define SCF_PRIVAT	2

#define SCF_TRUE	1
#define SCF_FALSE	0
#define SCF_NO_PTR	-1
#define SCF_NO_EOL	-2
#define SCF_NO_RAM	-3
#define SCF_NO_VAL	-4
#define SCF_NO_SCF	-5
#define SCF_IO_ERR	-6

#define SCFF_VERSION	"1.0"
/* the root/default section */
#define SCF_ROOT		NULL
#define SCF_WHITE		'W'

#define scf_id	unsigned int
typedef unsigned char * string;
#ifndef uchar
#define uchar unsigned char
#endif

scf_id scf_init (const char *pname, const char *scf_file);
int scf_fini (scf_id id);

int scf_read (scf_id id);
int scf_add_section (scf_id id, char *name, char *comment);
int scf_has_section (scf_id id, char *sec);
int scf_section_has_key (scf_id id, const char *sec, const char *key);
#define scf_has_key(id, key) scf_section_has_key(id, SCF_ROOT, key)
int scf_get_type (scf_id, const string section, const string key);
string scf_get_val (scf_id id, const string section, const string key);
#define scf_val(id, key) scf_get_val(id, SCF_ROOT, key);
string scf_get_comment (scf_id id, const string sec, const string key);
int scf_get_int_val (scf_id id, const string sec, const string key, int *rv);
int scf_get_bool_val (scf_id id, const string sec, const string key, int *rv);
double scf_get_val_as_double (scf_id id, const string sec, const string key);
int scf_set_val (scf_id id, string sec, string key, string value, string comment);
int scf_open (scf_id id, int mode);
int scf_close (scf_id id);
int scf_sync (scf_id id);
int scf_set_mode (scf_id id, int mode, const char *kvps, const char *cs);
void scf_set_comment_char (unsigned int scfd, char c);
void scf_set_kvp_delimiter (unsigned int scfd, char c);
const char *scf_file_name (const char *pname, int flags);
int scf_strip_eol (string line);
int scf_next_line (scf_id id, string *line);
int scf_no_of_subsections (scf_id, const string sec);
string scf_get_array_val (scf_id, const string sec, const string key, int n);
int scf_get_array_int_val (scf_id, const string sec, const string key, int n, int *val);

/* */
#define SCF_COMMENT	1
#define SCF_SECTION	2
#define SCF_STRING	4
#define SCF_INT		8
#define SCF_BIN		16
#define SCF_BOOL	32
#define SCF_ARRAY	64	

#ifndef PATH_MAX
/* may be some systems don't have this predefined in limits.h!?
 */
#error "comment this out if you don't have a limits.h!"
#define PATH_MAX 1024
#endif
/* same ..
 */
#ifndef NAME_MAX
#define NAME_MAX 256
#endif

#endif	/* _SCF_H_ */

