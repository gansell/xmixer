/*
 * Box.h
 *
 * Copyright (C) 1997 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _XwBox_h
#define _XwBox_h

#include <X11/Core.h>
#include <X11/Xmu/Converters.h>

#ifndef XtNmargin
#define XtNmargin	"margin"
#endif
#ifndef XtCMargin
#define XtCMargin	"Margin"
#endif
#ifndef XtNresize
#define XtNresize		"resize"
#define XtCResize		"Resize"
#endif
#ifndef XtNuserData
#define XtNuserData			"userData"
#define XtCUserData			"UserData"
#endif
#define XtNacceptDrop	"acceptDrop"
#define XtCAcceptDrop	"AcceptDrop"
#define XtNexpand		"expand"
#define XtCExpand		"Expand"

#define XtNtabCallback	"tabCallback"
#define XtCTabCallback	"TabCallback"
#ifndef XtNorientation
#define XtNorientation	"orientation"
#define XTCOrientation	"Orientation"
#endif

/* class record constants
 */
extern WidgetClass xwBoxWidgetClass;

typedef struct _XwBoxClassRec *XwBoxWidgetClass;
typedef struct _XwBoxRec *XwBoxWidget;

#endif
