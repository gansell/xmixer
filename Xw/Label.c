/*
 * Label.c; vi: tabstop=4
 *
 * Copyright (C) 1998 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __FILE__
#define __FILE__ "Label.c"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include <Xw/LabelP.h>

/* resource list */
#define offset(field) XtOffsetOf (XwLabelRec, field)

static XtResource resources [] = {
	{	XtNlabel, XtCLabel, XtRString, sizeof(String),
		offset(label.label), XtRString, NULL },
	{	XtNfont, XtCFont, XtRFontStruct, sizeof (XFontStruct *),
		offset(label.font), XtRString, XtDefaultFont },
	{	XtNmargin, XtCMargin, XtRDimension, sizeof (Dimension),
		offset(label.margin), XtRImmediate, (XtPointer)2 },
	{	XtNleftPixmap, XtCLeftPixmap, XtRPixmap, sizeof (Pixmap),
		offset(label.left_pixmap), XtRImmediate, (XtPointer)NULL },
	/* base */
	{	XtNshadowType, XtCShadowType, XtRInt, sizeof(int),
		offset(base.shadow_type), XtRImmediate, (XtPointer)XtShadowFlat },
};

/*
 * action function declarations
 */

/*
 * action table
 */
static XtActionsRec actions [] = {
	};

/*
 * default translation table
 */
static char defaultTranslations [] = "";

/*
 * method function declarations
 */
static void
	ClassPartInitialize (WidgetClass w),
	Initialize (Widget t, Widget n, ArgList args, Cardinal *num_args),
	Destroy (Widget w),
	Resize (Widget w),
	Redisplay (Widget aw, XEvent *event, Region region);
static Boolean SetValues (Widget, Widget, Widget, ArgList, Cardinal *);

/*
 * tool functions
 */
static void DrawLabel (Widget w);
static void DrawText (Widget w, Boolean clear);

#define SuperClass ((WidgetClass)&xwBaseClassRec)

/*
 * class record initialization
 */
XwLabelClassRec xwLabelClassRec = {
	{
		/* core_class fields     */
		/* superclass            */ SuperClass,
		/* class_name            */ "XwLabel",
		/* widget_size           */ sizeof(XwLabelRec),
		/* class_initialize      */ NULL,
		/* class_part_initialize */ ClassPartInitialize,
		/* class_inited          */ False,
		/* initialize            */ Initialize,
		/* initialize_hook       */ NULL,
		/* realize               */ XtInheritRealize,
		/* actions               */ actions,
		/* num_actions           */ XtNumber(actions),
		/* resources             */ resources,
		/* num_resources         */ XtNumber(resources),
		/* xrm_class             */ NULLQUARK,
		/* compress_motion       */ True,
		/* compress_exposure     */ XtExposeCompressMultiple,
		/* compress_enterleave   */ True,
		/* visible_interest      */ True,
		/* destroy               */ Destroy,
		/* resize                */ Resize,
		/* expose                */ Redisplay,
		/* set_values            */ SetValues,
		/* set_values_hook       */ NULL,
		/* set_values_almost     */ XtInheritSetValuesAlmost,
		/* get_values_hook       */ NULL,
		/* accept_focus          */ NULL,
		/* version               */ XtVersion,
		/* callback_private      */ NULL,
		/* tm_table              */ defaultTranslations,
		/* query_geometry        */ XtInheritQueryGeometry,
		/* display_accelerator   */ XtInheritDisplayAccelerator,
		/* extension             */ NULL
	},
	{	/* base_class fields */
		XtInheritTabAction,
		NULL,
		NULL,
	},
	{	/* label fields */
		0
	}
};

WidgetClass xwLabelWidgetClass = (WidgetClass) &xwLabelClassRec;


/*
 * method function definitions
 */
#define FontHeight(f)  (int)(f->max_bounds.ascent + f->max_bounds.descent)
#define FontAscent(f)	(int)(f->max_bounds.ascent)
#define FontDescent(f)  (int)(f->max_bounds.descent)
#define TextWidth(f,s)	XTextWidth (f,s,strlen(s))

/*
 * class_part_initialize method
 */
static void
ClassPartInitialize (WidgetClass wc)
{
	XwLabelWidgetClass lc = (XwLabelWidgetClass) wc;
	if (lc->label_class.draw_label == XtInheritDrawLabel)
		lc->label_class.draw_label = DrawLabel;
}

/*
 */
static void
BuildText (XwLabelWidget lw) {
    int len;
    char *pos;

#ifdef DEBUG_LABEL
    printf ("%s: BuildText(%s) label=%s\n", __FILE__, lw->core.name, lw->label.label);
#endif
    lw->label.underline_pos = -1;
    len = strlen (lw->label.label);
    pos = strchr (lw->label.label, '&');
    if (pos) {
        if (*(pos+1) != '&') {
            lw->label.underline_pos = pos - lw->label.label;
        }
        len--;
    }
    lw->label.text = XtMalloc (len +1);
    lw->label.text[len] ='\0';
    if (pos) {
        if (pos == lw->label.label)
            strcpy (lw->label.text, lw->label.label+1);
        else {
            strncpy (lw->label.text, lw->label.label, pos - lw->label.label);
            strcpy (lw->label.text+(pos-lw->label.label), pos + 1);
        }
    } else {
        strcpy (lw->label.text, lw->label.label);
    }
	lw->label.start_x = lw->base.shadow_width + lw->label.margin;
	lw->label.start_y =
		(lw->core.height - FontHeight(lw->label.font) * lw->label.lines) / 2
		+ FontAscent(lw->label.font);
    if (lw->label.underline_pos > -1) {
        lw->label.underline_x = lw->label.start_x
            + (lw->label.underline_pos ?
            XTextWidth (lw->label.font, lw->label.text, lw->label.underline_pos)
            : 0);
        lw->label.underline_y = lw->label.start_y
            + FontDescent(lw->label.font);
        lw->label.underline_len = XTextWidth (lw->label.font, "&", 1);
    }
#ifdef DEBUG2
    printf ("%s: BuildText text=\"%s\"\n", __FILE__, lw->label.text);
#endif
}

/*
 */
static void
InitGC (XwLabelWidget lw)
{
	XGCValues gcv;
	XtGCMask mask;

	gcv.font = lw->label.font->fid;
	gcv.line_width = 1;
	gcv.foreground = lw->base.foreground_pixel;
	gcv.background = lw->core.background_pixel;
	gcv.graphics_exposures = False;
	mask = GCLineWidth | GCForeground | GCBackground|GCFont|GCGraphicsExposures;
	lw->label.sensitiveGC = XtGetGC ((Widget)lw, mask, &gcv);
	gcv.foreground = lw->base.not_sensitive_pixel;
	lw->label.insensitiveGC = XtGetGC ((Widget)lw, mask, &gcv);
#ifdef DEBUG_LABEL
	printf ("%s: InitGC(%s) sgc=%x nsgc=%x\n", __FILE__, lw->core.name,
				(int)lw->label.sensitiveGC, (int)lw->label.insensitiveGC);
#endif
}

/*
 * initialize Method
 */
static void
Initialize (Widget treq, Widget tnew, ArgList args, Cardinal *num_args)
{
	XwLabelWidget lw = (XwLabelWidget) tnew;
	char *s, *p;
	int width = 0, twidth = 0;
#ifdef DEBUG_LABEL
	printf ("%s: Initialize(%s) width=%d height=%d\n",
			__FILE__, lw->core.name, lw->core.width, lw->core.height);
#endif
	if (lw->label.left_pixmap != 0) {
		unsigned int bw;
		int x,y ;
		Window root;
		XGetGeometry (XtDisplay(lw), lw->label.left_pixmap, &root, &x, &y,
				&lw->label.lpm_width, &lw->label.lpm_height, &bw,
				&lw->label.lpm_depth);
	}
	if (lw->label.label == NULL)
		lw->label.label = lw->core.name;
	lw->label.lines = 1;
	s = lw->label.label;
	while (( p = strchr (s, '\n')) != NULL) {
		twidth = XTextWidth (lw->label.font, s, p-s);
		if (twidth > width) {
			width = twidth;
		}
		lw->label.lines++;
		s = p+1;
	}
	if (s != lw->label.label) {
		twidth = XTextWidth (lw->label.font, s, strlen(s));
		if (twidth > width) {
			width = twidth;
		}
	}
	if ( lw->label.lines == 1 ) {
		width = TextWidth (lw->label.font, lw->label.label);
	}

	if ( lw->core.height == 0 )
		lw->core.height = FontHeight (lw->label.font) * lw->label.lines
				+ 2 * lw->base.shadow_width
				+ 2 * lw->label.margin;
	if ( lw->core.width == 0 ) {
		lw->core.width = width + 2 * lw->base.shadow_width
				+ 2 * lw->label.margin;
	}
	BuildText (lw);
	if (lw->label.underline_pos > -1)
		lw->core.width -= TextWidth (lw->label.font, "&");

	if (lw->label.left_pixmap) {
		lw->core.width += lw->label.lpm_width + lw->label.margin;
		if ((lw->label.lpm_height + 2 * lw->base.shadow_width) > lw->core.height)
			lw->core.height = lw->label.lpm_height + 2 * lw->base.shadow_width;
		lw->label.start_x += lw->label.lpm_width + lw->label.margin;
		lw->label.lpm_y = lw->label.start_y - FontHeight(lw->label.font) + lw->label.margin;
	}
	InitGC (lw);
	if (lw->core.sensitive) {
		lw->label.textGC = lw->label.sensitiveGC;
	} else {
		lw->label.textGC = lw->label.insensitiveGC;
	}
}

/*
 * set_values method
 */
static Boolean
SetValues (Widget curr, Widget req, Widget reply, ArgList args, Cardinal *nargs)
{
	Boolean redraw = False, redraw_text = False;
	XwLabelWidget tlw = (XwLabelWidget) curr;
	XwLabelWidget new = (XwLabelWidget) reply;
	int i;
	
	for (i = 0; i < *nargs; i++) {
		if (strcmp (args[i].name, XtNlabel) == 0) {
			redraw_text = True;
			XtFree (new->label.text);
			BuildText (new);
		}
	}

	if ((tlw->base.not_sensitive_pixel != new->base.not_sensitive_pixel) ||
		(tlw->core.background_pixel != new->core.background_pixel) ||
		(tlw->base.foreground_pixel != new->base.foreground_pixel))
	{
		XtReleaseGC (curr, tlw->label.sensitiveGC);
		XtReleaseGC (curr, tlw->label.insensitiveGC);
		InitGC (new);
		redraw = True;
	}
	if (tlw->core.sensitive != new->core.sensitive) {
		if (new->core.sensitive) {
			new->label.textGC = tlw->label.sensitiveGC;
		} else {
			new->label.textGC = tlw->label.insensitiveGC;
		}
		redraw = True;
	}
	if ((tlw->core.width != new->core.width) ||
		(tlw->core.height!= new->core.height)) {
		if (!new->base.resize) {
			new->core.width = tlw->core.width;
			new->core.height= tlw->core.height;
		}
	}
#ifdef DEBUG_LABEL
	printf ("%s: SetValues(%s, ..) redraw=%d width=%d->%d\n",
				__FILE__, new->core.name, (int) redraw, tlw->core.width, new->core.width);
#endif
	if (!redraw && redraw_text)
		/* there is no need to redraw the frame/shadow */
		DrawText (reply, True);
	return (redraw);
}

/*
 * destroy method
 */
static void
Destroy (Widget w)
{
	XwLabelWidget tlw = (XwLabelWidget) w;
#ifdef DEBUG_LABEL
	printf ("%s: Destroy(%s)\n", __FILE__, w->core.name);
#endif
	XtFree (tlw->label.text);
	XtReleaseGC (w, tlw->label.sensitiveGC);
	XtReleaseGC (w, tlw->label.insensitiveGC);
}

/*
 * resize method
 */
static void
Resize (Widget w)
{
#ifdef DEBUG_LABEL
	XwLabelWidget tlw = (XwLabelWidget) w;
	printf ("%s: Resize(%s)\n", __FILE__, tlw->core.name);
#endif
}

/*
 * expose method
 */
static void
Redisplay (Widget aw, XEvent *event, Region region)
{
	XwLabelWidget w = (XwLabelWidget) aw;

#ifdef DEBUG_LABEL
	printf ("%s: Redisplay(%s[0x%x]) label='%s'[0x%x]\n", __FILE__,
		w->core.name, (int)w->core.name, w->label.label, (int)w->label.label);
#endif
	DrawLabel (aw);
	if (w->base.shadow_width > 0)
		(*SuperClass->core_class.expose)(aw, event, region);
}

/*
 * tool functions
 */

/*
 */
static void
DrawText (Widget w, Boolean clear)
{
	XwLabelWidget lw = (XwLabelWidget) w;
	int i, y, len;
	char *str, *p;

	if (clear)
	XClearArea (XtDisplay(w), XtWindow(w),
				lw->label.start_x, lw->base.shadow_width,
				w->core.width - lw->label.start_x -
					lw->label.margin - lw->base.shadow_width,
				w->core.height - 2*lw->base.shadow_width, False);
	str = lw->label.text;
	y = lw->label.start_y;
	for (i = 0; i < lw->label.lines; i++) {
		if ((p = strchr (str, '\n')) != NULL)
			len = p - str;
		else
			len = strlen (str);
		XDrawImageString (XtDisplay(w), XtWindow(w), lw->label.textGC,
							lw->label.start_x, y, str, len);
		if (p)
			str = p+1;
		y += FontHeight(lw->label.font);
	}
	if (lw->label.underline_pos > -1) {
		XDrawLine (XtDisplay(w), XtWindow(w), lw->label.textGC,
			lw->label.underline_x, lw->label.underline_y,
			lw->label.underline_x + lw->label.underline_len,
			lw->label.underline_y);
	}
}

/*
 */
static void
DrawLabel (Widget w)
{
	XwLabelWidget lw = (XwLabelWidget) w;

#ifdef DEBUG_LABEL
	printf ("%s: DrawLabel(%s) gc=%x\n", __FILE__, w->core.name,
				(int)lw->label.textGC);
#endif
	if (!XtIsRealized(w))
		return;
	if (lw->label.text)
		DrawText(w, False);
	if (lw->label.left_pixmap) {
		if (lw->label.lpm_depth == 1) {
			XCopyPlane (XtDisplay(lw), lw->label.left_pixmap, XtWindow(lw),
						lw->label.textGC,
						0, 0, lw->label.lpm_width, lw->label.lpm_height,
						lw->base.shadow_width + lw->label.margin,
						lw->label.lpm_y, 1L);
		} else {
			XCopyArea (XtDisplay(lw), lw->label.left_pixmap, XtWindow(lw),
						lw->label.textGC,
						0, 0, lw->label.lpm_width, lw->label.lpm_height,
						lw->base.shadow_width + lw->label.margin,
						lw->label.lpm_y);
		}
	}
}

/*
 * action function definitions
 */
/*EOF*/

