/*
 * Base.c; vi: tabstop=4
 *
 * Copyright (C) 1998 Rasca, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __FILE__
#define __FILE__ "Base.c"
#endif
#include <stdio.h>
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include <Xw/BaseP.h>

/* resource list */
#define offset(member) XtOffsetOf (XwBaseRec, member)

static XtResource resources [] = {
	/* base defaults
	 */
	{	XtNforeground, XtCForeground, XtRPixel, sizeof(Pixel),
		offset(base.foreground_pixel), XtRString, "black" },
	{	XtNhighlight, XtCHighlight, XtRPixel, sizeof(Pixel),
		offset(base.highlight_pixel), XtRString, "blue4" },
	{	XtNnotSensitiveColor, XtCNotSensitiveColor, XtRPixel, sizeof(Pixel),
		offset (base.not_sensitive_pixel), XtRString, "grey60" },
	{	XtNshadowTopColor, XtCShadowTopColor, XtRPixel, sizeof(Pixel),
		offset(base.shadow_top_pixel), XtRString, "grey95" },
	{	XtNshadowBottomColor, XtCShadowBottomColor, XtRPixel, sizeof(Pixel),
		offset (base.shadow_bottom_pixel), XtRString, "grey70" },
	{	XtNshadowWidth, XtCShadowWidth, XtRDimension, sizeof(Dimension),
		offset(base.shadow_width), XtRImmediate, (XtPointer)2 },
	{	XtNshadowType, XtCShadowType, XtRInt, sizeof(int),
		offset(base.shadow_type), XtRImmediate, (XtPointer)XtShadowNone },
	{	XtNresize, XtCResize, XtRBoolean, sizeof(Boolean),
		offset(base.resize), XtRImmediate, (XtPointer)0 },
	{	XtNacceptDrop, XtCAcceptDrop, XtRBoolean, sizeof(Boolean),
		offset(base.acceptDrop), XtRImmediate, (XtPointer)0 },
	{	XtNtabCallback, XtCCallback, XtRCallback, sizeof(XtPointer),
		offset(base.tab_callbacks), XtRCallback, NULL },
	{	XtNprev, XtCPrev, XtRWidget, sizeof (XtPointer),
		offset(base.prev), XtRImmediate, NULL},
	{	XtNnext, XtCNext, XtRWidget, sizeof (XtPointer),
		offset(base.next), XtRImmediate, NULL},
	{	XtNuserData, XtCUserData, XtRPointer, sizeof(XtPointer),
		offset(base.data), XtRImmediate, NULL },
	/* core defaults
	 */
	{	XtNborderWidth, XtCBorderWidth, XtRDimension, sizeof(Dimension),
		offset(core.border_width), XtRImmediate, (XtPointer)1 },
	{	XtNbackground, XtCBackground, XtRPixel, sizeof(Pixel),
		offset(core.background_pixel), XtRString, "grey90" },
	{	XtNborderColor, XtCBorderColor, XtRPixel, sizeof(Pixel),
		offset(core.border_pixel), XtRString, "grey90" },
};

/*
 * action function declarations
 */
static void Tab (Widget, XEvent*, String*, Cardinal*);
static void Focus (Widget, XEvent*, String *, Cardinal *);
static void GetFocus (Widget, XEvent*, String *, Cardinal *);

/*
 * action table
 */
static XtActionsRec actions [] = {
	{"tab",			Tab},
	{"focus",		Focus },
	{"get-focus",	GetFocus },
	};

/*
 * default translation table
 */
static char defaultTranslations [] = "\
<FocusIn>: focus(in)
<FocusOut>: focus(out)
<Key>Tab:	tab()";

/*
 * method function declarations
 */
static void
	ClassPartInitialize (WidgetClass wc),
	Initialize (Widget t, Widget n, ArgList args, Cardinal *num_args),
	Destroy (Widget w);
static void Resize (Widget w);
static void Redisplay (Widget aw, XEvent *event, Region region);
static Boolean SetValues (Widget, Widget, Widget, ArgList, Cardinal *);
/* */

#define SuperClass ((CoreWidgetClass)&coreClassRec)
/*
 * class record initialization
 */
XwBaseClassRec xwBaseClassRec = {
	{	/* core_class fields     */
		/* superclass            */ (WidgetClass) SuperClass,
		/* class_name            */ "XwBase",
		/* widget_size           */ sizeof(XwBaseRec),
		/* class_initialize      */ NULL,
		/* class_part_initialize */ ClassPartInitialize,
		/* class_inited          */ False,
		/* initialize            */ Initialize,
		/* initialize_hook       */ NULL,
		/* realize               */ XtInheritRealize,
		/* actions               */ actions,
		/* num_actions           */ XtNumber(actions),
		/* resources             */ resources,
		/* num_resources         */ XtNumber(resources),
		/* xrm_class             */ NULLQUARK,
		/* compress_motion       */ True,
		/* compress_exposure     */ XtExposeCompressMultiple,
		/* compress_enterleave   */ True,
		/* visible_interest      */ True,
		/* destroy               */ Destroy,
		/* resize                */ Resize,
		/* expose                */ Redisplay,
		/* set_values            */ SetValues,
		/* set_values_hook       */ NULL,
		/* set_values_almost     */ XtInheritSetValuesAlmost,
		/* get_values_hook       */ NULL,
		/* accept_focus          */ NULL,
		/* version               */ XtVersion,
		/* callback_private      */ NULL,
		/* tm_table              */ defaultTranslations,
		/* query_geometry		*/ XtInheritQueryGeometry,
		/* display_accelerator	*/ XtInheritDisplayAccelerator,
		/* extension			*/ NULL
	},
	{	/* base_class fields */
		/* tab_action			*/	Tab,
		/* focus_action			*/	Focus,
		/* getfocus_action		*/	GetFocus,
	},
};

WidgetClass xwBaseWidgetClass = (WidgetClass) &xwBaseClassRec;

/*
 * downward chained: first this one and then subclasses
 */
static void
ClassPartInitialize (WidgetClass wc)
{
	XwBaseWidgetClass c = (XwBaseWidgetClass) wc;
	if (c->base_class.tab_action == XtInheritTabAction)
		c->base_class.tab_action = Tab;
	if (c->base_class.focus_action == XtInheritFocusAction)
		c->base_class.focus_action = Focus;
	if (c->base_class.getfocus_action == XtInheritGetFocusAction)
		c->base_class.getfocus_action = GetFocus;
}

/*
 * method function definitions
 */
static void
InitGC (XwBaseWidget bw)
{
	Widget w = (Widget) bw;
	XGCValues gcv;
	XtGCMask mask;

#ifdef DEBUG_BASE
	printf ("%s: InitGC (%s)\n", __FILE__, bw->core.name);
#endif
	mask = GCLineStyle | GCLineWidth | GCForeground ;
	gcv.line_style = LineSolid;
	gcv.line_width = 0;

	gcv.foreground = bw->base.shadow_bottom_pixel;
	bw->base.downGC = XtGetGC (w, mask, &gcv);

	gcv.foreground = bw->base.shadow_top_pixel;
	bw->base.upGC = XtGetGC (w, mask, &gcv);

	gcv.foreground =BlackPixel(XtDisplay(w),XScreenNumberOfScreen(XtScreen(w)));
	bw->base.blackGC = XtGetGC (w, mask, &gcv);
}

/*
 * initialize Method
 */
static void
Initialize (Widget treq, Widget tnew, ArgList args, Cardinal *num_args)
{
	XwBaseWidget new = (XwBaseWidget) tnew;
#ifdef DEBUG_BASE
	printf ("%s: Initialize (%s)\n", __FILE__, new->core.name);
#endif
	InitGC (new);
}

/*
 * set_values method
 */
static Boolean
SetValues (Widget curr, Widget req, Widget reply, ArgList args, Cardinal *nargs)
{
	Boolean redraw = False;
	XwBaseWidget bw = (XwBaseWidget) curr;
	XwBaseWidget new= (XwBaseWidget) reply;
	
	if ((bw->base.shadow_top_pixel != new->base.shadow_top_pixel) ||
		(bw->base.shadow_bottom_pixel != new->base.shadow_bottom_pixel))
	{
		XtReleaseGC (curr, bw->base.upGC);
		XtReleaseGC (curr, bw->base.downGC);
		XtReleaseGC (curr, bw->base.blackGC);
		InitGC (new);
		redraw = True;
	}
	if ((bw->base.shadow_type  != new->base.shadow_type) ||
		(bw->base.shadow_width != new->base.shadow_width)) {
		redraw = True;
	}
#ifdef DEBUG_BASE
	printf("%s: SetValues (%s ..) rv=%d\n",__FILE__,new->core.name,(int)redraw);
#endif
	return (redraw);
}

/*
 * destroy method
 */
static void
Destroy (Widget w)
{
	XwBaseWidget bw = (XwBaseWidget) w;
#ifdef DEBUG_BASE
	printf ("%s: Destroy (%s)\n", __FILE__, w->core.name);
#endif
	XtReleaseGC (w, bw->base.upGC);
	XtReleaseGC (w, bw->base.downGC);
	XtReleaseGC (w, bw->base.blackGC);
}


/*
 * resize method
 */
static void
Resize (Widget w)
{
#ifdef DEBUG_BASE
	printf ("%s: Resize (%s)\n", __FILE__, w->core.name);
#endif
}

/*
 * expose method
 * redisplay the widget (draw 3d frame)
 */
static void
Redisplay (Widget w, XEvent *event, Region region)
{
	XwBaseWidget bw = (XwBaseWidget) w;
	int i;

#ifdef DEBUG_BASE
	printf ("%s: Redisplay (%s) sw=%d type=%d\n",
		__FILE__, w->core.name, bw->base.shadow_width, bw->base.shadow_type);
#endif

	if (!XtIsRealized(w))
		return;

	if (bw->base.shadow_width < 1)
		return;

/* seems not to work :(
	if ((region != NULL) && (!XEmptyRegion(region))) {
		int rc;

		rc = XRectInRegion (region, w->core.x, w->core.y,
				w->core.width, w->core.height);
		if ((rc != RectanglePart) && (rc != RectangleIn)) {
			return;
		}
	} */

	if (bw->base.shadow_type == XtShadowUp) {
		for (i = 0; i < bw->base.shadow_width; i++) {
			if (i < bw->base.shadow_width -1) {
				/* top horizontal line
				 */
				XDrawLine (XtDisplay(w), XtWindow(w), bw->base.upGC,
					i + 1, i, w->core.width - i - 1, i);
				/* left vertical line
				 */
	 			XDrawLine (XtDisplay(w), XtWindow(w), bw->base.upGC,
					i, i, i, w->core.height - i - 1);
			}
			if (!i) {
				/* draw black line */
				/* bottom horizontal line
				 */
				XDrawLine (XtDisplay(w), XtWindow(w), bw->base.blackGC,
					1, w->core.height-1, w->core.width-1, w->core.height-1);
				/* right vertical line
				 */
				XDrawLine (XtDisplay(w), XtWindow(w), bw->base.blackGC,
					w->core.width-1-i, i, w->core.width-1-i, w->core.height-i);
			} else {
				/* bottom horizontal line */
				XDrawLine (XtDisplay(w), XtWindow(w), bw->base.downGC,
					i + 1, w->core.height - 1- i,
					w->core.width - i - 1, w->core.height-1-i);
				/* right vertical line */
				XDrawLine (XtDisplay(w), XtWindow(w), bw->base.downGC,
					w->core.width-1-i, i,
					w->core.width-1-i, w->core.height-i-1);
			}
		}
	} else if (bw->base.shadow_type == XtShadowDown) {
		for (i = 0; i < bw->base.shadow_width; i++) {
			if (!i && bw->base.shadow_width > 1) {
				/* top horizontal line */
				XDrawLine (XtDisplay(w), XtWindow(w), bw->base.blackGC,
					i + 1, i, w->core.width - i - 1, i);
				/* left vertical line */
	 			XDrawLine (XtDisplay(w), XtWindow(w), bw->base.blackGC,
					i, i, i, w->core.height - i);
			} else {
				/* top horizontal line */
				XDrawLine (XtDisplay(w), XtWindow(w), bw->base.downGC,
					i + 1, i, w->core.width - i - 1, i);
				/* left vertical line */
	 			XDrawLine (XtDisplay(w), XtWindow(w), bw->base.downGC,
					i, i, i, w->core.height - i - 1);
			}
			if ((i < bw->base.shadow_width - 1) ||bw->base.shadow_width<2) {
				/* bottom horizontal line */
				XDrawLine (XtDisplay(w), XtWindow(w), bw->base.upGC,
					i + 1, w->core.height - 1- i, w->core.width - i - 1,
					w->core.height-1-i);
				/* right vertical line */
				XDrawLine (XtDisplay(w), XtWindow(w), bw->base.upGC,
					w->core.width-1-i, i, w->core.width-1-i, w->core.height-i);
			}
		}
	} else if (bw->base.shadow_type == XtShadowFlat) {
		GC top, bottom, tmp;
		int half = bw->base.shadow_width /2;
		top = bw->base.downGC;
		bottom = bw->base.upGC;
		for (i = 0; i < bw->base.shadow_width; i++) {
			if (i == half) {
				tmp = bottom;
				bottom = top;
				top = tmp;
			}
			/* top horizontal line */
			XDrawLine (XtDisplay(w), XtWindow(w), top,
				i + 1, i, w->core.width - i - 1, i);
			/* left vertical line */
	 		XDrawLine (XtDisplay(w), XtWindow(w), top,
				i, i, i, w->core.height - i - 2);
			/* bottom horizontal line */
			XDrawLine (XtDisplay(w), XtWindow(w), bottom,
				i + 1, w->core.height - 1- i, w->core.width - i - 1,
				w->core.height-1-i);
			/* right vertical line */
			XDrawLine (XtDisplay(w), XtWindow(w), bottom,
				w->core.width-1-i, i, w->core.width-1-i, w->core.height-i-1);
		}
	}
#ifdef DEBUG_BASE2
	printf ("%s:  Redisplay() ..end\n", __FILE__);
#endif
}


/*
 * action function definitions
 */


/*
 * called if TAB was pressed
 */
static void
Tab (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwBaseWidget bw = (XwBaseWidget) w, nw;
	static Time t = CurrentTime;
	
	/* let's pass the event - may be it's usefull .. */
#ifdef DEBUG_BASE
	printf ("%s: Tab('%s', ..)\n", __FILE__, w->core.name);
#endif
	XtCallCallbackList (w, ((XwBaseWidget)w)->base.tab_callbacks, event);
	if (*num_params) {
		nw = bw;
		while (1) {
			if (!XtIsXwWidget(nw))
				break;
			if (*params[0] == 'p') {
				/* previous */
				nw = (XwBaseWidget)nw->base.prev;
			} else {
				/* next */
				nw = (XwBaseWidget)nw->base.next;
			}
			if (!nw)
				break;
			if (XtCallAcceptFocus ((Widget)nw, &t)) {
				w = (Widget)nw;
				/* XtSetKeyboardFocus (XtParent(w), w); */
				break;
			}
		}
	}
}

/*
 * change border color
 */
/* ARGSUSED */
static void
Focus (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	Pixel c;
#ifdef DEBUG_BASE
	printf ("%s: Focus ('%s', .., %s)\n", __FILE__, w->core.name, *params);
#endif
	if (*params[0] == 'i') {
		XtVaGetValues (w, XtNforeground, &c, NULL);
	} else {
		XtVaGetValues (w, XtNbackground, &c, NULL);
	}
	XtVaSetValues (w, XtNborderColor, c, NULL);
}

/* ARGSUSED */
static void
GetFocus (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	static Time t = CurrentTime;
#ifdef DEBUG_BASE
    printf ("%s: GetFocus(%s)\n", __FILE__, XtName(w));
#endif
	if (XtCallAcceptFocus(w, &t)) {
		/* XtSetKeyboardFocus (XtParent(w), w); */
	}
}

/*EOF*/

