/*
 * vi, tabstop=4
 * TextField	A single line text entry widget
 *  -
 *  - this is a modified version of the original TextField Widget
 *  - by Robert McMullen ..
 *  -
 * Copyright (c) 1997 Rasca Gmelch
 * Copyright (c) 1995 Robert W. McMullen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Authors: Rob McMullen <rwmcm@orion.ae.utexas.edu>
 *          http://www.ae.utexas.edu/~rwmcm
 *          Rasca Gmelch <thron@gmx.de>
 *          http://home.pages.de/~rasca/
 */

#ifndef __FILE__
#define __FILE__ "Field.c"
#endif
#include <stdio.h>
#include <ctype.h>
#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <X11/Xatom.h>
#include <X11/Xmu/Xmu.h>
#include <Xw/FieldP.h>

#define offset(field) XtOffsetOf (XwFieldRec, field)
static XtResource resources[] =
{
	{XtNallowSelection, XtCBoolean, XtRBoolean, sizeof(Boolean),
		offset(text.allow_select), XtRString, "True"},
	{XtNonlyNumber, XtCBoolean, XtRBoolean, sizeof(Boolean),
		offset(text.only_number), XtRString, "False"},
	{XtNdisplayCaret, XtCBoolean, XtRBoolean, sizeof(Boolean),
		offset(text.show_cursor), XtRString, "True"},
	{XtNecho, XtCBoolean, XtRBoolean, sizeof(Boolean),
		offset(text.echo), XtRString, "True"},
	{XtNeditable, XtCBoolean, XtRBoolean, sizeof(Boolean),
		offset(text.editable), XtRString, "True"},
	{XtNfont, XtCFont, XtRFontStruct, sizeof(XFontStruct *),
		offset(text.font), XtRString, XtDefaultFont},
	{XtNinsertPosition, XtCInsertPosition, XtRInt, sizeof(int),
		offset(text.cursor_pos), XtRString, "0"},
	{XtNlength, XtCLength, XtRInt, sizeof(int),
		offset(text.max_length), XtRImmediate, (XtPointer)0},
	{XtNmargin, XtCMargin, XtRDimension, sizeof(Dimension),
		offset(text.margin), XtRImmediate, (XtPointer)2},
	{XtNpendingDelete, XtCBoolean, XtRBoolean, sizeof(Boolean),
		offset(text.pending_delete), XtRString, "True"},
	{XtNstring, XtCString, XtRString, sizeof(char *),
		offset(text.string), XtRString, NULL},
	{XtNactiveBg, XtCActiveBg, XtRPixel, sizeof(Pixel),
		offset(text.active_bg_pixel), XtRString, "grey95"},
	{XtNactivateCallback, XtCCallback, XtRCallback, sizeof(XtPointer),
		offset(text.activate_callback), XtRCallback, NULL},
	/* base fields */
	{XtNshadowType, XtCShadowType, XtRInt, sizeof(int),
		offset(base.shadow_type), XtRImmediate, (XtPointer)XtShadowDown},
};

#undef offset

static void
	TabAction (Widget aw, XEvent *ev, String *params, Cardinal *num_params);

static void
	Initialize (Widget treq, Widget tnew, ArgList args, Cardinal *num),
	Destroy (Widget w),
	Redisplay (Widget aw, XEvent * event, Region region),
	Resize (Widget w);
static Boolean
	SetValues (Widget, Widget, Widget, ArgList, Cardinal *),
	AcceptFocus (Widget, Time*);

static void
	Draw(),
	DrawInsert(),
	MassiveChangeDraw(),
	DrawTextReposition(),
	ClearHighlight(),
	DrawHighlight(),
	DrawCursor(),
	EraseCursor();
static Boolean PositionCursor(), MassiveCursorAdjust();
static void
	Activate (Widget w, XEvent *event, String *params, Cardinal *num_params),
	InsertChar (Widget w, XEvent *event, String *params, Cardinal *num_params),
	ForwardChar (Widget w, XEvent *event, String *params, Cardinal *num_params),
	BackwardChar (Widget w,XEvent *event, String *params, Cardinal *num_params),
	DeleteNext (Widget w, XEvent *event, String *params, Cardinal *num_params),
	DeletePrev (Widget w, XEvent *event, String *params, Cardinal *num_params),
	SelectStart (Widget w, XEvent *event, String *params, Cardinal *num_params),
	ExtendStart (Widget w, XEvent *event, String *params, Cardinal *num_params),
	ExtendAdjust (Widget, XEvent*, String*, Cardinal*),
	ExtendEnd (Widget w, XEvent *event, String *params, Cardinal *num_params),
	Unselect (Widget w, XEvent *event, String *params, Cardinal *num_params),
	InsertSelection (Widget, XEvent*, String*, Cardinal*),
	ExtendKeyLeft (Widget, XEvent*, String*, Cardinal*),
	ExtendKeyRight (Widget, XEvent*, String*, Cardinal *),
	MarkWordLeft (Widget, XEvent*, String*, Cardinal *),
	MarkWordRight (Widget, XEvent*, String*, Cardinal*),
	MarkToEnd (Widget w, XEvent *event, String *params, Cardinal *num_params),
	MarkToHome (Widget w, XEvent *event, String *params, Cardinal *num_params),
	GoWordLeft(Widget w, XEvent *event, String *params, Cardinal *num_params),
	GoWordRight(Widget w, XEvent *event, String *params, Cardinal *num_params),
	GoHome(Widget w, XEvent *event, String *params, Cardinal *num_params),
	GoEnd(Widget w, XEvent *event, String *params, Cardinal *num_params),
	ChangeWindow(Widget w, XEvent *event, String *params, Cardinal *num_params),
	GetFocus(Widget w, XEvent *event, String *params, Cardinal *num_params),
	Focus(Widget w, XEvent *event, String *params, Cardinal *num_params);

static char defaultTranslations[] = "\
!<Key>Left: backward-char()\n\
!<Key>Right: forward-char()\n\
!<Key>Home: go-home()\n\
!<Key>End: go-end()\n\
!Shift<Key>Right: extend-key-right()\n\
!Shift<Key>Left: extend-key-left()\n\
!Ctrl<Key>Left: go-word-left()\n\
!Ctrl<Key>Right: go-word-right()\n\
!Shift Ctrl<Key>Left: select-word-left()\n\
!Shift Ctrl<Key>Right: select-word-right()\n\
!Shift<Key>End: select-to-end()\n\
!Shift<Key>Home: select-to-home()\n\
<Key>Delete: delete-next-char()\n\
<Key>BackSpace: delete-previous-char()\n\
<Key>Return: activate()\n\
Shift<Btn1Down>: extend-start()\n\
!Ctrl<Btn1Up>(2): go-home() select-to-end()\n\
<Btn1Down>: get-focus() select-start()\n\
<Btn1Motion>: extend-adjust()\n\
<Btn1Up>: extend-end()\n\
<Btn2Down>: insert-selection()\n\
<Btn3Down>: extend-start()\n\
<Btn3Motion>: extend-adjust()\n\
<Btn3Up>: extend-end()\n\
<EnterWindow>: highlight()\n\
<LeaveWindow>: reset()\n\
<FocusIn>: focus(in) go-home() select-to-end() highlight()\n\
<FocusOut>: reset() unselect() focus(out)\n\
!Ctrl<Key>v: insert-selection()\n\
Shift<Key>Tab: reset() tab(prev)\n\
<Key>Tab: reset() tab(next)\n\
~Alt<Key>: insert-char()";

static XtActionsRec actions[] =
{
	{"insert-char",		InsertChar	},
	{"forward-char",	ForwardChar	},
	{"backward-char",	BackwardChar},
	{"delete-next-char", DeleteNext	},
	{"delete-previous-char", DeletePrev},
	{"activate", 		Activate	},
	{"extend-key-left",	ExtendKeyLeft},
	{"extend-key-right", ExtendKeyRight},
	{"select-word-left", MarkWordLeft},
	{"select-word-right", MarkWordRight},
	{"select-to-end",	MarkToEnd	},
	{"select-to-home",	MarkToHome	},
	{"go-word-left",	GoWordLeft	},
	{"go-word-right",	GoWordRight	},
	{"go-home",			GoHome		},
	{"go-end",			GoEnd		},
	{"select-start",	SelectStart	},
	{"extend-start",	ExtendStart	},
	{"extend-adjust",	ExtendAdjust},
	{"extend-end",		ExtendEnd	},
	{"unselect",		Unselect	},
	{"insert-selection", InsertSelection},
	{"highlight",		ChangeWindow},
	{"reset",			ChangeWindow},
	{"get-focus",		GetFocus	},
	{"focus",			Focus		},
	{"tab",				TabAction	},
};

#define SuperClass ((WidgetClass)&xwBaseClassRec)

FieldClassRec xwFieldClassRec = {
 {	/* core_class fields     */
	/* superclass            */ SuperClass,
	/* class_name            */ "XwField",
	/* widget_size           */ sizeof(XwFieldRec),
	/* class_initialize      */ NULL,
	/* class_part_initialize */ NULL,
	/* class_inited          */ False,
	/* initialize            */ Initialize,
	/* initialize_hook       */ NULL,
	/* realize               */ XtInheritRealize,
	/* actions               */ actions,
	/* num_actions           */ XtNumber(actions),
	/* resources             */ resources,
	/* num_resources         */ XtNumber(resources),
	/* xrm_class             */ NULLQUARK,
	/* compress_motion       */ True,
	/* compress_exposure     */ XtExposeCompressMultiple,
	/* compress_enterleave   */ True,
	/* visible_interest      */ True,
	/* destroy               */ Destroy,
	/* resize                */ Resize,
	/* expose                */ Redisplay,
	/* set_values            */ SetValues,
	/* set_values_hook       */ NULL,
	/* set_values_almost     */ XtInheritSetValuesAlmost,
	/* get_values_hook       */ NULL,
	/* accept_focus          */ AcceptFocus,
	/* version               */ XtVersion,
	/* callback_private      */ NULL,
	/* tm_table              */ defaultTranslations,
	/* query_geometry        */ XtInheritQueryGeometry,
	/* display_accelerator   */ XtInheritDisplayAccelerator,
	/* extension             */ NULL,
 },
 {	/* base_class */
	/* tab_action		*/	XtInheritTabAction,
	/* focus_action		*/	XtInheritFocusAction,
	/* getfocus_action	*/	XtInheritGetFocusAction,
 },
 {	/* field_class */
		0,
 }
};


WidgetClass xwFieldWidgetClass = (WidgetClass) & xwFieldClassRec;

/* Convenience macros */
#define Margin(w)		(int)(w->text.margin)
#define LeftMargin(w)	(int)(ShadowWidth(w) + w->text.margin)
#define RightMargin(w)	(int)(ShadowWidth(w) + w->text.margin)
#define TopMargin(w)    (int)(ShadowWidth(w) + w->text.margin)
#define BottomMargin(w) (int)(ShadowWidth(w) + w->text.margin)

/* Font functions */
#define FontHeight(f)  (int)(f->max_bounds.ascent + f->max_bounds.descent)
#define FontDescent(f) (int)(f->max_bounds.descent)
#define FontAscent(f)  (int)(f->max_bounds.ascent)
#define FontTextWidth(f,c,l) (int)XTextWidth(f, c, l)

/*
 * initialize graphic context
 */
static void
InitializeGC (FieldWidget w)
{
	XGCValues values;
	XtGCMask mask;

	values.fill_style = FillSolid;
	values.font = w->text.font->fid;
  	values.background = w->core.background_pixel;
	values.foreground = w->base.foreground_pixel;
	mask = GCFillStyle | GCForeground | GCBackground | GCFont;
	w->text.drawGC = XtGetGC((Widget) w, mask, &values);

	values.foreground = w->core.background_pixel;
	values.background = w->base.highlight_pixel;
	w->text.markGC = XtGetGC((Widget) w, mask, &values);

	values.line_style = LineSolid;
	values.line_width = 0;
	mask = GCLineStyle | GCLineWidth | GCForeground | GCBackground;
	w->text.eraseGC = XtGetGC((Widget) w, mask, &values);

	values.background = w->core.background_pixel;
	values.foreground = w->base.foreground_pixel;
	w->text.cursorGC = XtGetGC((Widget) w, mask, &values);

	w->text.x_offset = w->text.old_x_offset = LeftMargin(w);
	w->text.y_offset = (w->core.height - FontHeight(w->text.font)) / 2
				+ FontAscent(w->text.font);
}

/*
 * define the areas for the GCs which they should use..
 */
static void
ClipGC (FieldWidget w)
{
	XRectangle clip;

	clip.x = 0;
	clip.y = 0;
	clip.width = w->text.view_width;
	clip.height = w->core.height - TopMargin(w) - BottomMargin(w);

	XSetClipRectangles ( XtDisplay((Widget) w), w->text.drawGC,
		LeftMargin(w), TopMargin(w), &clip, 1, Unsorted );
	XSetClipRectangles ( XtDisplay((Widget) w), w->text.markGC,
		LeftMargin(w), TopMargin(w), &clip, 1, Unsorted );
	clip.height += 3;
	clip.width += (2* Margin(w));
	XSetClipRectangles ( XtDisplay((Widget) w), w->text.eraseGC,
		ShadowWidth(w), Margin(w) + ShadowWidth(w) - 1,
		&clip, 1, Unsorted );
	XSetClipRectangles ( XtDisplay((Widget) w), w->text.cursorGC,
		ShadowWidth(w), Margin(w) + ShadowWidth(w) - 1,
		&clip, 1, Unsorted );
}

/*
 * set the internal text variable and strip of which is
 * longer then max_length
 */
static void
SetString (FieldWidget w, String str)
{
	int len = 0;
	char * p;

	if (!str)
		str = "";
	p = str;
	while (*p++) {
		if (len == w->text.max_length)
			break;
		len++;
	}
	strncpy (w->text.text, str, len);
	w->text.text[len] = '\0';
	w->text.text_len = len;
	w->text.text_width = w->text.old_text_width =
		FontTextWidth (w->text.font, w->text.text, w->text.text_len);
	w->text.string = w->text.text;
#ifdef DEBUG_FIELD2
	printf ("SetString() str=%s text=%s len=%d max=%d\n",
			str, w->text.text, len, w->text.max_length);
#endif
}

/*
 * initialize method
 */
static void
Initialize (Widget treq, Widget tnew, ArgList args, Cardinal * num)
{
	XwFieldWidget new;
	int height;

	new = (XwFieldWidget) tnew;

	new->text.timer_id = (XtIntervalId) 0;
	new->text.time = 0;
	new->text.multi_click_time = XtGetMultiClickTime (XtDisplay((Widget) new));
	new->text.highlight_time = new->text.multi_click_time / 2;

	if (new->text.max_length == 0) {
		new->text.max_length = DEFAULT_MAX_LENGTH;
	}
	new->text.text = (char *) XtMalloc (new->text.max_length + 1);
	new->text.selection_text = NULL;
	SetString (new, new->text.string);

	if (new->text.cursor_pos > 0) {
		if (new->text.cursor_pos > new->text.text_len) {
			new->text.cursor_pos = new->text.text_len;
		}
	} else {
		new->text.cursor_pos = 0;
	}
	new->text.old_cursor_x = -1;
	new->text.marked_start = new->text.marked_end = -1;
	new->text.old_marked_start = new->text.old_marked_end = -1;
	/* Oops */
	new->text.marked_pivot_start = new->text.marked_pivot_end = 0;

	height = FontHeight (new->text.font);
	if (new->core.height == 0)
		new->core.height = (Dimension) height
					+ TopMargin(new) + BottomMargin(new);

	if (new->core.width == 0) {
		new->core.width = 120;
	}
	new->text.view_width = (int) new->core.width
					- LeftMargin(new) - RightMargin(new);
	InitializeGC (new);
	ClipGC (new);
}

/*
 * destroy method
 * XtWidgetProc
 */
static void
Destroy (Widget w)
{
	XtReleaseGC (w, ((FieldWidget)w)->text.drawGC);
	XtReleaseGC (w, ((FieldWidget)w)->text.markGC);
	XtReleaseGC (w, ((FieldWidget)w)->text.eraseGC);
	XtReleaseGC (w, ((FieldWidget)w)->text.cursorGC);
	if (((FieldWidget)w)->text.selection_text)
		XtFree(((FieldWidget)w)->text.selection_text);
	XtFree(((FieldWidget)w)->text.text);
}

/*
 * expose method
 */
/* XtExposeProc */
static void
Redisplay (Widget w, XEvent *event, Region region)
{
	FieldWidget fw = (FieldWidget) w;

	if (!XtIsRealized (w))
		return;
#ifdef DEBUG_FIELD
	fprintf (stderr, "%s: Redisplay (%s)\n", __FILE__, w->core.name);
#endif
	Draw (fw);
	(*(SuperClass)->core_class.expose)((Widget)w, (XEvent*) NULL, NULL);
}

/*
 * set_values method
 */
static Boolean
SetValues (Widget current, Widget request, Widget reply,
			ArgList args, Cardinal *nargs)
{
	FieldWidget w = (FieldWidget) current;
	FieldWidget new = (FieldWidget) reply;
	Boolean redraw = False;

	if ((w->base.foreground_pixel != new->base.foreground_pixel) ||
		(w->core.background_pixel != new->core.background_pixel) ||
		(w->text.font != new->text.font)) {
		XtReleaseGC ((Widget) w, w->text.drawGC);
		XtReleaseGC ((Widget) w, w->text.markGC);
		XtReleaseGC ((Widget) w, w->text.cursorGC);
		XtReleaseGC ((Widget) w, w->text.eraseGC);
		InitializeGC (new);
		ClipGC (new);
		redraw = True;
	}

	if ((w->text.cursor_pos != new->text.cursor_pos) ||
		(w->text.show_cursor != new->text.show_cursor)) {
		redraw = True;
	}
	if (w->text.string != new->text.string) {
		redraw = True;
		SetString (new, new->text.string);
		new->text.marked_start = new->text.marked_end = -1;
		new->text.cursor_pos = new->text.text_len;
	}
#ifdef DEBUG_FIELD
	printf ("%s: SetValues (%s) redraw=%d\n", __FILE__, new->core.name, redraw);
#endif
	return (redraw);
}

/*
 * accept_focus method
 */
static Boolean
AcceptFocus (Widget w, Time *t)
{
#ifdef DEBUG_FIELD
	printf ("%s: AcceptFocus(%s) v=%d t=%d\n", __FILE__, w->core.name, w->core.visible, (int)(*t));
#endif
	if (!XtIsRealized(w))
		return (False);
	if (!XtIsManaged(w))
		return (False);
	/* if (!XtIsSensitive(w)) */
		/* return (False); */
	return (True);
}

/*
 * XtWidgetProc
 */
static void
Resize (Widget aw)
{
	FieldWidget w = (FieldWidget) aw;
	int width;

	if (!XtIsRealized(aw))
		return;
#ifdef DEBUG_FIELD
	printf ("%s: Resize\n", __FILE__);
#endif
	width = w->core.width - LeftMargin(w) - RightMargin(w);
	if (width < 0)
		w->text.view_width = 0;
	else
		w->text.view_width = width;

	w->text.y_offset = ((int) w->core.height - FontHeight(w->text.font)) / 2
						+ FontAscent(w->text.font);
	w->text.x_offset = LeftMargin(w);
	ClipGC (w);
	MassiveChangeDraw(w);
}

/*
 */
static void
TextDelete (FieldWidget w, int start, int len)
{
	int i;

	if (len > 0) {
		for (i = start + len; i < w->text.text_len; i++)
			w->text.text[i - len] = w->text.text[i];
		w->text.text_len -= len;
		w->text.text_width = FontTextWidth(w->text.font, w->text.text, w->text.text_len);
		w->text.text[w->text.text_len] = 0;
	}
}

/*
 */
static void
TextDeleteHighlighted (FieldWidget w)
{
#ifdef DEBUG_FIELD
	printf ("TextDeleteHighlighted(%x) end=%d\n", (int)w, w->text.marked_end);
#endif
	if (w->text.marked_start >= 0) {
		TextDelete (w, w->text.marked_start,
			w->text.marked_end - w->text.marked_start);
		w->text.cursor_pos = w->text.marked_start;
		w->text.marked_start = w->text.marked_end = -1;
	}
}

/* returns value indicating if the text can be redrawn using the
 * fast method
 */
static Boolean
TextInsert (FieldWidget w, char *buf, int len)
{
	int i;
	Boolean fast_insert;

	fast_insert = True;
	if (len <= 0)
		return (True);

	if (w->text.marked_start >= 0) {
		fast_insert = False;
		if (w->text.pending_delete)
			TextDeleteHighlighted(w);
		else
			ClearHighlight(w);
	}

	if (w->text.only_number) {
		int n = len, t = 0;
		char *nbuf = XtMalloc (len+1);
		if (!nbuf)
			return (True);
		strncpy (nbuf, buf, len);
		for (i = 0; i < n; i++) {
			if (isdigit (buf[i])|| (buf[i] == '.') || (buf[i] == ',')) {
				nbuf[t] = buf[i];
			} else {
				len--;
			}
		}
		buf = nbuf;
	}
	if (w->text.text_len + len > w->text.max_length) {
		len = w->text.max_length - w->text.text_len;
	}
	for (i = w->text.text_len - 1; i >= w->text.cursor_pos; i--)
		w->text.text[i + len] = w->text.text[i];

	strncpy (&w->text.text[w->text.cursor_pos], buf, len);
	w->text.FastInsertCursorStart = w->text.cursor_pos;
	w->text.FastInsertTextLen = len;
	w->text.text_len += len;
	w->text.cursor_pos += len;

	w->text.text_width = FontTextWidth (w->text.font,
		w->text.text, w->text.text_len);
	w->text.text[w->text.text_len] = '\0';
	if (w->text.only_number) {
		XtFree (buf);
	}
	return fast_insert;
}

/*
 * find the position in the text from a pixel value
 */
static int
TextPixelToPos (FieldWidget w, int x)
{
	int i, tot, cur, pos;

	pos = 0;

	x -= (int) LeftMargin(w);

	/* check if the cursor is before the 1st character
	 */
	if (x <= 0)
		pos = 0;
	else if (x > FontTextWidth(w->text.font, w->text.text, w->text.text_len)) {
		/* OK, how 'bout after the last character
		 */
		pos = w->text.text_len;
	} else {
		/* must be in between somewhere...
		 */
		tot = 0;
		pos = -1;
		for (i = 0; i < w->text.text_len; i++) {
			cur = FontTextWidth(w->text.font, &w->text.text[i], 1);
			if (x < tot + (cur / 2)) {
				pos = i;
				break;
			}
			tot += cur;
		}
		if (pos < 0)
		pos = w->text.text_len;
	}
	return pos;
}


/*
 * Field Widget Action procedures
 */

/* ARGSUSED */
static void
GetFocus (Widget w, XEvent * event, String * params, Cardinal * num_params)
{
#ifdef DEBUG_FIELD
	fprintf (stderr, "%s: GetFocus(%s)\n", __FILE__, XtName(w));
#endif
	(*xwFieldClassRec.base_class.getfocus_action)(w,event, params, num_params);
}

/* ARGSUSED */
static void
Focus (Widget w, XEvent * event, String * params, Cardinal * num_params)
{
	(*xwFieldClassRec.base_class.focus_action)(w, event, params, num_params);
}


/* ARGSUSED */
static void
TabAction (Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
#ifdef DEBUG_FIELD
	fprintf (stderr, "%s:TabAction()\n", __FILE__);
#endif
	(*xwFieldClassRec.base_class.tab_action)(aw, event, params, num_params);
}

/*
 */
/* ARGSUSED */
static void
ChangeWindow (Widget w, XEvent * event, String * params, Cardinal * num_params)
{
	static Pixel bg;

	if (event->type == EnterNotify || event->type == FocusIn) {
		bg = w->core.background_pixel;
		XtVaSetValues (w, XtNbackground, ((FieldWidget)w)->text.active_bg_pixel, NULL);
	} else if (event->type == LeaveNotify || event->type == FocusOut) {
		XtVaSetValues (w, XtNbackground, bg, NULL);
	}
}

/* ARGSUSED */
static void
Activate (Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	FieldWidget w = (FieldWidget) aw;
	FieldReturnStruct ret;

	ret.reason = 0;
	ret.event = event;
	ret.string = w->text.text;

	XtCallCallbacks(aw, XtNactivateCallback, &ret);
}

/* ARGSUSED */
static void
ForwardChar (Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	FieldWidget w = (FieldWidget) aw;

#ifdef DEBUG_FIELD
	printf ("ForwardChar (0x%X)\n", (int)aw);
#endif
	if (!w->text.editable)
    	return;

	ClearHighlight (w);
	if (w->text.cursor_pos < w->text.text_len) {
		w->text.cursor_pos++;
		EraseCursor (w);
		if (PositionCursor (w))
			DrawTextReposition (w);
		DrawCursor (w);
	}
	w->text.marked_pivot_start =
		w->text.marked_pivot_end = w->text.cursor_pos;
}

/* ARGSUSED */
static void
BackwardChar (Widget aw, XEvent *event, String *params, Cardinal *num_params)
{
	FieldWidget w = (FieldWidget) aw;

	if (!w->text.editable)
		return;
#ifdef DEBUG_FIELD
	printf ("BackwardChar() xoff=%d cursor=%d\n",
			w->text.x_offset, w->text.cursor_pos);
#endif
	ClearHighlight (w);
	if (w->text.cursor_pos > 0) {
		w->text.cursor_pos--;
		EraseCursor (w);
		if (PositionCursor (w)) {
			DrawTextReposition (w);
		}
		DrawCursor (w);
	}
	w->text.marked_pivot_start =
		w->text.marked_pivot_end = w->text.cursor_pos;
}

/*
 * XtAction: insert a character at cursor position
 */
/* ARGSUSED */
static void
InsertChar (Widget aw, XEvent *event, String *params, Cardinal *num_params)
{
	FieldWidget w = (FieldWidget) aw;
	int len;

#define INSERTCHARBUFSIZ 32
	char buf[INSERTCHARBUFSIZ];

	if (!w->text.editable)
		return;

	len = XLookupString((XKeyEvent *) event, buf, BUFSIZ, NULL, NULL);
	if (len > 0) {
		EraseCursor(w);
		if (TextInsert(w, buf, len))
			DrawInsert(w);
		else
			Draw(w);
		w->text.marked_pivot_start =
			w->text.marked_pivot_end = w->text.cursor_pos;
	}
}

/* ARGSUSED */
static void
DeleteNext (Widget aw, XEvent *event, String *params, Cardinal *num_params)
{
  FieldWidget w = (FieldWidget) aw;

	if (!w->text.editable)
		return;

	if (w->text.marked_start >= 0 && w->text.pending_delete) {
		TextDeleteHighlighted(w);
		MassiveChangeDraw(w);
	} else if (w->text.cursor_pos < w->text.text_len) {
		ClearHighlight (w);
		TextDelete (w, w->text.cursor_pos, 1);
		Draw (w);
	}
	w->text.marked_pivot_start =
		w->text.marked_pivot_end = w->text.cursor_pos;
}

/* ARGSUSED */
static void
DeletePrev (Widget aw, XEvent * event, String * params, Cardinal * num_params) {
	FieldWidget w = (FieldWidget) aw;

	if (!w->text.editable)
		return;

	if (w->text.marked_start >= 0 && w->text.pending_delete) {
		TextDeleteHighlighted (w);
		MassiveChangeDraw (w);
	} else if (w->text.cursor_pos > 0) {
		ClearHighlight (w);
		TextDelete (w, w->text.cursor_pos - 1, 1);
		w->text.cursor_pos--;
		Draw (w);
	}
	w->text.marked_pivot_start =
		w->text.marked_pivot_end = w->text.cursor_pos;
}

/* ARGSUSED */
static void
MarkWordLeft (Widget aw, XEvent * event, String * params, Cardinal * num_params) {
  FieldWidget w = (FieldWidget) aw;
	int pos;

	if (!w->text.allow_select)
		return;
    pos = w->text.cursor_pos;
	while (pos-- > 0) {
		ExtendKeyLeft (aw, event, params, num_params);
		if (w->text.text[w->text.cursor_pos-1] == ' ')
			break;
	}
}

/* ARGSUSED */
static void
MarkWordRight (Widget aw, XEvent *event, String *params, Cardinal *num_params)
{
	FieldWidget w = (FieldWidget) aw;
	int pos;

#ifdef DEBUG_FIELD
	printf ("MarkWordRight(0x%d)\n", (int)aw);
#endif
	if (!w->text.allow_select)
		return;
    pos = w->text.cursor_pos;
	while (pos++ < w->text.text_len) {
		ExtendKeyRight (aw, event, params, num_params);
		if (w->text.text[w->text.cursor_pos-1] == ' ')
			break;
	}
}

/* ARGSUSED */
static void
MarkToEnd (Widget aw, XEvent *event, String *params, Cardinal *num_params) {
	FieldWidget w = (FieldWidget) aw;
#ifdef DEBUG_FIELD
	printf ("MarkToEnd(0x%X) pivot_end=%d cursor=%d\n",
		(int)aw, w->text.marked_pivot_end, w->text.cursor_pos);
#endif
	while (w->text.cursor_pos < w->text.text_len)
		ExtendKeyRight (aw, event, params, num_params);
}

/* ARGSUSED */
static void
MarkToHome (Widget aw, XEvent * event, String * params, Cardinal * num_params) {
	FieldWidget w = (FieldWidget) aw;
	while (w->text.cursor_pos > 0)
		ExtendKeyLeft (aw, event, params, num_params);
}

/* ARGSUSED */
static void
GoWordLeft (Widget aw, XEvent * event, String * params, Cardinal * num_params) {
	FieldWidget w = (FieldWidget) aw;
	int pos;
    pos = w->text.cursor_pos;
	while (pos-- > 0) {
		BackwardChar (aw, event, params, num_params);
		if (w->text.text[w->text.cursor_pos-1] == ' ')
			break;
	}
}

/* ARGSUSED */
static void
GoWordRight (Widget aw, XEvent * event, String * params, Cardinal * num_params) {
	FieldWidget w = (FieldWidget) aw;
	int pos = w->text.cursor_pos;

	while (pos++ < w->text.text_len) {
		ForwardChar (aw, event, params, num_params);
		if (w->text.text[w->text.cursor_pos-1] == ' ')
			break;
	}
}

/* ARGSUSED */
static void
GoHome (Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	FieldWidget w = (FieldWidget) aw;
    ClearHighlight(w);
	while (w->text.cursor_pos > 0)
		BackwardChar (aw, event, params, num_params);
}

/* ARGSUSED */
static void
GoEnd (Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	FieldWidget w = (FieldWidget) aw;
    ClearHighlight(w);
	while (w->text.cursor_pos < w->text.text_len)
		ForwardChar (aw, event, params, num_params);
}

/* ARGSUSED */
static void
SelectStart (Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	FieldWidget w = (FieldWidget) aw;

	if (!w->text.allow_select)
		return;

	w->text.cursor_pos = TextPixelToPos (w, event->xbutton.x);
	w->text.marked_pivot_start =
		w->text.marked_pivot_end = w->text.cursor_pos;

	if (w->text.marked_start >= 0) {
		ClearHighlight(w);
	} else {
		EraseCursor (w);
		DrawCursor (w);
	}
	if (event->xbutton.time - w->text.time < w->text.multi_click_time) {
		/* double click */
		int start;
		start = TextPixelToPos (w, event->xbutton.x);
#ifdef DEBUG_FIELD
		printf ("double click at pos %d!\n", start);
#endif
		/* move the pointer at the beginning of the word */
		while (start && w->text.text[start-1] != ' ') {
			BackwardChar(aw, event, NULL, NULL);
			start--;
		}
		MarkWordRight (aw, event, params, num_params);
	}
	w->text.time = event->xbutton.time;
}

/* ARGSUSED */
static void
ExtendStart (Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	FieldWidget w = (FieldWidget) aw;
	int pos;

	if (!w->text.allow_select)
		return;

	pos = TextPixelToPos (w, event->xbutton.x);

	EraseCursor (w);
	if (w->text.marked_start < 0) {
		w->text.marked_start =
		w->text.marked_end =
		w->text.marked_pivot_start =
		w->text.marked_pivot_end = w->text.cursor_pos;
	} else {
		w->text.marked_pivot_start = w->text.marked_start;
		w->text.marked_pivot_end = w->text.marked_end;
	}
	if (pos < w->text.marked_start) {
		w->text.marked_start = pos;
	} else {
		w->text.marked_end = pos;
  	}
	w->text.cursor_pos = pos;
#ifdef DEBUG_FIELD
	printf ("ExtendStart(0x%X): %d - %d\n",
			(int)w, w->text.marked_start, w->text.marked_end);
#endif
	DrawHighlight (w);
	DrawCursor (w);
}

/* ARGSUSED */
static void
Unselect (Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
	FieldWidget w = (FieldWidget) aw;
	ClearHighlight (w);
}

/*
 */
static void
ExtendHighlight (FieldWidget w)
{
	int x, pos;

	if (!w->text.allow_select)
		return;

	x = w->text.timer_x;
	pos = TextPixelToPos (w, x);

	if (x < (int) LeftMargin(w)) {
		pos = TextPixelToPos (w, (int) 0);
		if (pos > 0)
			pos--;
		else if (pos == w->text.cursor_pos)
			return;
	} else if (x > (int) (LeftMargin(w) + w->text.view_width)) {
		pos = TextPixelToPos(w, (int) (w->text.margin + w->text.view_width));
		if (pos < w->text.text_len)
			pos++;
		else if (pos == w->text.cursor_pos)
			return;
	}
	if (pos == w->text.cursor_pos)
		return;

	EraseCursor (w);
	if (pos <= w->text.marked_pivot_start) {
		w->text.marked_start = pos;
		w->text.marked_end = w->text.marked_pivot_end;
	} else {
		w->text.marked_start = w->text.marked_pivot_start;
		w->text.marked_end = pos;
	}
	w->text.cursor_pos = pos;
#ifdef DEBUG_FIELD
	printf ("ExtendHighlight(0x%X): x=%d pos=%d  %d - %d\n",
			(int)w, x, pos, w->text.marked_start, w->text.marked_end);
#endif
	if (PositionCursor(w))
		DrawTextReposition (w);
	DrawHighlight (w);
	DrawCursor (w);
}

/*
 */
static void
ExtendTimer (XtPointer client_data, XtIntervalId * idp)
{
	FieldWidget w = (FieldWidget) client_data;

	ExtendHighlight(w);
	w->text.timer_id = XtAppAddTimeOut (
			XtWidgetToApplicationContext((Widget) w),
			(unsigned long) w->text.highlight_time,
			ExtendTimer,
			(XtPointer) w);
}

/* ARGSUSED */
static void
ExtendAdjust (Widget aw, XEvent *event, String *params, Cardinal *num_params)
{
	FieldWidget w = (FieldWidget) aw;

	if (!w->text.allow_select)
		return;
#ifdef DEBUG_FIELD
	printf ("ExtendAdjust(0x%x): xbutton.x = %d, pivot_end=%d\n",
				(int)w, event->xbutton.x, w->text.marked_pivot_end);
#endif

	w->text.timer_x = event->xbutton.x;

	if (event->xbutton.x < LeftMargin(w) || event->xbutton.x > LeftMargin(w) + w->text.view_width) {
		if (w->text.timer_id)
			ExtendHighlight(w);
		else
			ExtendTimer((XtPointer) w, (XtIntervalId) 0);
	} else {
		if (w->text.timer_id) {
			XtRemoveTimeOut(w->text.timer_id);
			w->text.timer_id = (XtIntervalId) 0;
		}
		ExtendHighlight(w);
	}
}

/*
 */
#define LEFT  1
#define RIGHT 2

static void
ExtendKeyHighlight (FieldWidget w, int direction) {
#ifdef DEBUG_FIELD
	printf("ExtendKeyHighlight(0x%X): %s, pivot_end = %d\n",
		(int)w, direction == LEFT ? "left" : "right", w->text.marked_pivot_end);
#endif
	if (direction == LEFT) {
		if (w->text.cursor_pos > 0)
			w->text.cursor_pos--;
		else
			return;
	} else {
		if (w->text.cursor_pos < w->text.text_len)
			w->text.cursor_pos++;
		else
			return;
	}
	EraseCursor (w);
	if (PositionCursor(w))
		DrawTextReposition (w);
	DrawCursor (w);
	if (w->text.cursor_pos <= w->text.marked_pivot_start) {
		w->text.marked_start = w->text.cursor_pos;
		w->text.marked_end = w->text.marked_pivot_end;
#ifdef DEBUG_FIELD
		printf("ExtendKeyHighlight(): if : end=%d\n", w->text.marked_end);
#endif
	} else {
		w->text.marked_start = w->text.marked_pivot_start;
		w->text.marked_end = w->text.cursor_pos;
#ifdef DEBUG_FIELD
		printf("ExtendKeyHighlight(): else : end=%d\n", w->text.marked_end);
#endif
	}
	DrawHighlight (w);
}

/* ARGSUSED */
static void
ExtendKeyLeft (Widget aw, XEvent *event, String *params, Cardinal *num_params)
{
	FieldWidget w = (FieldWidget) aw;
#ifdef DEBUG_FIELD
	printf ("ExtendKeyLeft(0x%X)\n", (int)aw);
#endif
	if (!w->text.allow_select)
		return;
	ExtendKeyHighlight (w, LEFT);
	ExtendEnd (aw, event, params, num_params);
}

/* ARGSUSED */
static void
ExtendKeyRight (Widget aw, XEvent *event, String *params, Cardinal *num_params)
{
	FieldWidget w = (FieldWidget) aw;
#ifdef DEBUG_FIELD
	printf ("ExtendKeyRight(0x%X) pivot_end=%d\n", (int)aw, w->text.marked_pivot_end);
#endif
	if (!w->text.allow_select)
		return;
	ExtendKeyHighlight (w, RIGHT);
	ExtendEnd (aw, event, params, num_params);
}

/* ARGSUSED */
static Boolean
ConvertSelection(Widget aw, Atom * selection, Atom * target, Atom * type,
  XtPointer * value, unsigned long *length, int *format)
{
  FieldWidget w = (FieldWidget) aw;
  XSelectionRequestEvent *req = XtGetSelectionRequest(aw, *selection, NULL);

  if (*target == XA_TARGETS(XtDisplay(aw))) {
    Atom *targetP, *std_targets;
    unsigned long std_length;

    XmuConvertStandardSelection(aw, req->time, selection,
      target, type, (XPointer *) & std_targets,
      &std_length, format);

    *value = XtMalloc((unsigned) sizeof(Atom) * (std_length + 1));
    targetP = *(Atom **) value;
    *length = std_length + 1;
    *targetP++ = XA_STRING;
    memmove((char *) targetP, (char *) std_targets, sizeof(Atom) * std_length);
    XtFree((char *) std_targets);
    *type = XA_ATOM;
    *format = sizeof(Atom) * 8;
    return True;
  }
  else if (*target == XA_STRING) {
    *length = (long) w->text.selection_len;
    *value = w->text.selection_text;
    *type = XA_STRING;
    *format = 8;
    return True;
  }
  return False;
}

/* ARGSUSED */
static void
LoseSelection(Widget aw, Atom * selection)
{
  FieldWidget w = (FieldWidget) aw;

  ClearHighlight(w);
}

/* ARGSUSED */
static void
ExtendEnd (Widget w, XEvent * event, String * params, Cardinal * num_params)
{
	FieldWidget fw = (FieldWidget) w;
	int len;

	if (!fw->text.allow_select)
		return;

	if (fw->text.timer_id) {
		XtRemoveTimeOut(fw->text.timer_id);
		fw->text.timer_id = (XtIntervalId) 0;
	}
	len = fw->text.marked_end - fw->text.marked_start;
	if (len > 0) {
		fw->text.selection_len = len;
		if (fw->text.selection_text)
			XtFree(fw->text.selection_text);
		fw->text.selection_text = XtMalloc(len);
		strncpy (fw->text.selection_text,
					&fw->text.text[fw->text.marked_start], len);

		XtOwnSelection (w, XA_PRIMARY, event->xbutton.time,
						ConvertSelection, LoseSelection, NULL);
		XChangeProperty (XtDisplay(w), DefaultRootWindow(XtDisplay(w)),
						XA_CUT_BUFFER0, XA_STRING, 8, PropModeReplace,
						(unsigned char *) fw->text.selection_text, len);
	}
}

/* ARGSUSED */
static void
RequestSelection(Widget aw, XtPointer client, Atom * selection, Atom * type,
  XtPointer value, unsigned long *length, int *format)
{
  FieldWidget w = (FieldWidget) aw;

  if ((value == NULL) || (*length == 0)) {
#ifdef DEBUG_FIELD
    printf("RequestSelection: no selection\n");
#endif
  }
  else {
    int savex;

    ClearHighlight(w);
    savex = w->text.old_cursor_x;
    w->text.cursor_pos = (int) client;
#ifdef DEBUG_FIELD
    printf("RequestSelection: inserting %s length=%d at pos: %d\n",
      (char *) value, (int) (*length), w->text.cursor_pos);
#endif
    TextInsert(w, (char *) value, (int) (*length));
    w->text.old_cursor_x = savex;
    Draw(w);
  }
}

/* ARGSUSED */
static void
InsertSelection(Widget aw, XEvent * event, String * params, Cardinal * num_params)
{
  FieldWidget w = (FieldWidget) aw;
  int pos;

  if (!w->text.allow_select)
    return;

  pos = TextPixelToPos (w, event->xbutton.x);
#ifdef DEBUG_FIELD
  printf("InsertSelection: event at pos: %d\n", pos);
#endif
  XtGetSelectionValue(aw, XA_PRIMARY, XA_STRING,
    RequestSelection,
    (XtPointer) pos, event->xbutton.time);
}


/*
 * TextField private drawing functions
 */

/*
 */
static Boolean
PositionCursor (FieldWidget w)
{
	int x, start, end;
	Boolean moved;

	moved = False;
	if (w->text.cursor_pos < 0)
		w->text.cursor_pos = 0;
	else if (w->text.cursor_pos > w->text.text_len)
		w->text.cursor_pos = w->text.text_len;

	x = FontTextWidth (w->text.font, w->text.text, w->text.cursor_pos);
	start = - w->text.x_offset;
	end = start + w->text.view_width;
	if (x < start) {
		w->text.x_offset = -x + LeftMargin(w);
		moved = True;
	} else if (x > end) {
		w->text.x_offset = w->text.view_width - x + LeftMargin(w);
		moved = True;
	}
#ifdef DEBUG_FIELD
	printf ("PositionCursor() moved=%d off=%d\n", moved, w->text.x_offset);
#endif
	return moved;
}

static Boolean
MassiveCursorAdjust (FieldWidget w)
{
	int start, end, last;
	Boolean moved = False;

	end = FontTextWidth (w->text.font, w->text.text, w->text.cursor_pos);
	if (w->text.marked_start >= 0)
		start = FontTextWidth(w->text.font, w->text.text, w->text.marked_start);
  	else
		start = end;

	if (end < w->text.view_width) {
		if (w->text.x_offset < 0) {
			w->text.x_offset = LeftMargin(w);
			moved = True;
		}
	} else if (start >= w->text.x_offset && end < w->text.x_offset + w->text.view_width)
		return moved;
	else {
    	last = FontTextWidth(w->text.font, w->text.text, w->text.text_len);
		if (start - end > w->text.view_width) {
			if (last - end > w->text.view_width)
				w->text.x_offset = w->text.view_width - last + LeftMargin(w);
			else
				w->text.x_offset = w->text.view_width - end + LeftMargin(w);
		} else if (end > w->text.view_width)
			w->text.x_offset = w->text.view_width - end + LeftMargin(w);
		else
			w->text.x_offset = LeftMargin(w);
		moved = True;
  	}
	return moved;
}

/*
 * Actually draw a range of text onto the widget
 */
static void
DrawText (FieldWidget w, int start, int end, Boolean highlight)
{
	int x;
	GC gc;

	if (!w->text.echo)
		return;
#ifdef DEBUG_FIELD
	printf ("DrawText (0x%X): start=%d end=%d highlight=%d xoff=%d\n",
			(int)w, start, end, highlight, w->text.x_offset);
#endif

	if ((w->text.text_len <= 0) ||
		(start < 0)) {
		return;
	}
	if (end < start) {
		int temp;

		temp = start;
		start = end;
		end = temp;
	}
	if (end <= w->text.text_len) {
		x = /* w->text.Margin +*/ w->text.x_offset +
		FontTextWidth (w->text.font, w->text.text, start);
		if (highlight)
			gc = w->text.markGC;
		else
			gc = w->text.drawGC;
#ifdef DEBUG_FIELD
		printf("DrawText(): x=%d start=%d end=%d\n", x, start, end);
#endif
		XDrawImageString (XtDisplay(w), XtWindow(w), gc, x, w->text.y_offset,
			&w->text.text[start], end - start);
	}
#ifdef DEBUG_FIELD2
	printf("DrawText: end-start=%d end=%d start=%d\n", end-start, end, start);
#endif
}

static void
DrawTextRange (FieldWidget w, int start, int end)
{
#ifdef DEBUG_FIELD
	printf ("DrawTextRange(): len=%d w=0x%X\n", w->text.text_len, (int)w);
#endif
	if (!w->text.echo || (w->text.text_len <= 0))
		return;

	if (start < 0)
		return;
	else if (end < start) {
		int temp;

		temp = start;
		start = end;
		end = temp;
	}
#ifdef DEBUG_FIELD
	printf ("DrawTextRange() start=%d end=%d\n", start, end);
#endif

/* If there is no highlighting, or the refresh area doesn't cross the */
/* the highlight borders, just redraw it. */
    if ((w->text.marked_start < 0) ||
		(start >= w->text.marked_end) ||
		(end <= w->text.marked_start)) {
		DrawText (w, start, end, False);
	} else {

/* OK, the refresh area crosses one or both highlight borders. */
		int clip;

		while (start < end) {
			if (start < w->text.marked_start) {
				if (end <= w->text.marked_start)
					clip = end;
				else
					clip = w->text.marked_start;
				DrawText(w, start, clip, False);
				start = clip;
			} else if (start < w->text.marked_end) {
				if (end <= w->text.marked_end)
					clip = end;
				else
					clip = w->text.marked_end;
				DrawText(w, start, clip, True);
				start = clip;
			} else {
				DrawText(w, start, end, False);
				start = end;
			}
		}
	}
}

/*
 */
static void
DrawTextReposition (FieldWidget w)
{
	int xsrc, xdest, width, start, end;

	if (!w->text.echo)
		return;

	xsrc = xdest = LeftMargin(w);

	if (w->text.x_offset < w->text.old_x_offset) {
		xsrc = w->text.old_x_offset - w->text.x_offset;
		width = w->text.view_width - xsrc + 1;

		/* Need to redraw some characters at the end.
		 */
		end = TextPixelToPos (w, w->text.view_width);
		start = TextPixelToPos (w, w->text.view_width - xsrc);
	} else if (w->text.x_offset > w->text.old_x_offset) {
		xdest = w->text.x_offset - w->text.old_x_offset;
		width = w->text.view_width - xdest + 1;

		/* Need to redraw some characters at the beginning.
		 */
		start = TextPixelToPos (w, LeftMargin(w));
		end = TextPixelToPos (w, xdest);
	} else
		return;

	if (width > 0) {
#ifdef DEBUG_FIELD
		printf("DrawTextReposition(): xoff=%d old=%d src=%d dest=%d width=%d refresh %d-%d\n",
			w->text.x_offset, w->text.old_x_offset, xsrc, xdest, width, start, end);
#endif
		XCopyArea (XtDisplay(w), XtWindow(w), XtWindow(w),
			w->text.drawGC, xsrc, 0,
			(unsigned int) width, (unsigned int) w->core.height,
			xdest, 0);
		DrawTextRange (w, start, end);
	}
	w->text.old_x_offset = w->text.x_offset;
}

/*
 */
static void
DrawTextWithCopyArea (FieldWidget w)
{
	int x, insert_width;
	int xsrc, xdest, width;

	if (!w->text.echo)
		return;

	x = w->text.x_offset;
	insert_width = FontTextWidth (w->text.font,
						&w->text.text[w->text.FastInsertCursorStart],
						w->text.FastInsertTextLen);
	if (PositionCursor(w)) {
		/*
		 *  if the text is scrolled, then:
		 * 1.  the cursor is at the end
		 * 2.  the copy will move to the left.
		 */
		xsrc = 0;
		width = w->text.old_cursor_x + x;
		xdest = w->text.view_width - (x + w->text.old_cursor_x) - insert_width;
		XCopyArea (XtDisplay(w), XtWindow(w), XtWindow(w),
					w->text.drawGC,
					w->text.margin + xsrc, 0,
					(unsigned int) width, (unsigned int) w->core.height,
					w->text.margin + xdest, 0);
#ifdef DEBUG_FIELD
    printf("DrawTextWithCopyArea(): x=%d xsrc=%d xdest=%d width=%d\n", x, xsrc, xdest, width);
#endif
	} else {
		/*
		 * the text hasn't been scrolled, so:
		 * 1.  the text left of the cursor won't change
		 * 2.  the stuff after the cursor will be moved right.
		 */
		xsrc = FontTextWidth(w->text.font, w->text.text, w->text.FastInsertCursorStart) + x;
		width = w->text.view_width - xsrc;
		xdest = xsrc + insert_width;
		XCopyArea(XtDisplay(w), XtWindow(w), XtWindow(w),
			w->text.drawGC,
			w->text.margin + xsrc, 0,
			(unsigned int) width, (unsigned int) w->core.height,
			w->text.margin + xdest, 0);
#ifdef DEBUG_FIELD
		printf("DrawInsert: x=%d xsrc=%d xdest=%d width=%d\n", x, xsrc, xdest, width);
#endif
	}
	DrawTextRange (w, w->text.FastInsertCursorStart,
		w->text.FastInsertCursorStart + w->text.FastInsertTextLen);
	if (w->text.max_length > 0) {
		/*
		 * This is pretty much a hack:
		 * clear everything to end of window if this is a
		 * fixed length TextField
		 */
		xsrc = w->text.x_offset + w->text.text_width;
		width = w->text.view_width - xsrc;
		XClearArea(XtDisplay(w), XtWindow(w),
			w->text.margin + xsrc, ShadowWidth(w),
			(unsigned int) width, w->core.height - (2*ShadowWidth(w)), False);
	} else if (w->text.text_width < w->text.old_text_width) {
		XClearArea(XtDisplay(w), XtWindow(w),
			w->text.margin + w->text.x_offset + w->text.text_width, ShadowWidth(w),
			w->text.old_text_width - w->text.text_width + 1,
			w->core.height - (2*ShadowWidth(w)), False);
	}
	w->text.old_text_width = w->text.text_width;
	w->text.old_x_offset = w->text.x_offset;
}

/*
 */
static void
DrawAllText (FieldWidget w)
{
#ifdef DEBUG_FIELD
	printf ("DrawAllText() xoff=%d echo=%d\n", w->text.x_offset, w->text.echo);
#endif
	if (!w->text.echo)
		return;

	DrawTextRange (w, 0, w->text.text_len);
	if (w->text.text_width < w->text.old_text_width) {
		XClearArea (XtDisplay(w), XtWindow(w),
			w->text.x_offset + w->text.text_width, ShadowWidth(w),
			w->text.old_text_width - w->text.text_width + 1,
			w->core.height - (2*ShadowWidth(w)), False);
	}
	w->text.old_text_width = w->text.text_width;
	w->text.old_x_offset = w->text.x_offset;
	w->text.old_marked_start = w->text.marked_start;
	w->text.old_marked_end = w->text.marked_end;
}

/*
 * Draw an I-beam cursor
 */
static void
DrawIBeamCursor (FieldWidget w, int x, GC gc)
{
#ifdef DEBUG_FIELD2
	printf ("DrawIBeamCursor(x=%d)\n", x);
#endif
	XDrawLine (XtDisplay(w), XtWindow(w), gc,
		x, w->text.y_offset - FontAscent(w->text.font)  - 1,
		x, w->text.y_offset + FontDescent(w->text.font) + 0);

	XDrawLine (XtDisplay(w), XtWindow(w), gc,
		x - 2, w->text.y_offset - FontAscent(w->text.font) -1,
		x + 2, w->text.y_offset - FontAscent(w->text.font) -1);

	XDrawLine (XtDisplay(w), XtWindow(w), gc,
		x - 2, w->text.y_offset + FontDescent(w->text.font),
		x + 2, w->text.y_offset + FontDescent(w->text.font));
}

/*
 */
static void
DrawCursor (FieldWidget w)
{
	int x;
	GC gc;

	if (w->text.show_cursor) {
		x = FontTextWidth (w->text.font, w->text.text, w->text.cursor_pos);
		x+= LeftMargin (w);
#ifdef DEBUG_FIELD
		printf ("DrawCursor() xoff=%d x=%d\n", w->text.x_offset, x);
#endif
		w->text.old_cursor_pos = w->text.cursor_pos;
		w->text.old_cursor_x = x;
		gc = w->text.cursorGC;
		DrawIBeamCursor (w, x>2?x-1:2, gc);
  	}
}

/*
 */
static void
EraseCursor (FieldWidget w)
{
	int x;

	if (w->text.show_cursor && w->text.old_cursor_x >= 0) {
		x = w->text.old_cursor_x;
		DrawIBeamCursor (w, x>2?x-1:2, w->text.eraseGC);

		/* Little hack to fix up the character that might have been affected
		 * by erasing the old cursor.
		 */
		if (w->text.old_cursor_pos < w->text.text_len)
			DrawTextRange (w, w->text.old_cursor_pos -1, w->text.old_cursor_pos + 1);
	}
}

static void
ClearHighlight (FieldWidget w)
{
	if (!w->text.echo)
		return;

	if (w->text.marked_start >= 0) {
    	EraseCursor (w);
		DrawText (w, w->text.marked_start, w->text.marked_end, False);
		DrawCursor (w);
		w->text.marked_start = w->text.marked_end = -1;
	}
  	w->text.old_marked_start = w->text.old_marked_end = -1;
}

/*
 */
static void
DrawHighlight (FieldWidget w)
{
	if (!w->text.echo)
		return;
#ifdef DEBUG_FIELD
	printf ("DrawHighlight(0x%X): hlStart=%d hlEnd=%d\n",
		(int)w, w->text.marked_start, w->text.marked_end);
#endif

	if (w->text.old_marked_start < 0) {
		DrawText (w, w->text.marked_start, w->text.marked_end, True);
	} else {
		DrawText (w, w->text.marked_start, w->text.old_marked_start,
			(w->text.marked_start < w->text.old_marked_start));
		DrawText (w, w->text.marked_end, w->text.old_marked_end,
			(w->text.marked_end > w->text.old_marked_end));
	}
	w->text.old_marked_start = w->text.marked_start;
	w->text.old_marked_end = w->text.marked_end;
}

/*
 * Special redraw function after a text insertion
 */
static void
DrawInsert (FieldWidget w)
{
#ifdef DEBUG_FIELD
	printf ("DrawInsert()\n");
#endif
	/* EraseCursor must be called before this
	 */
	DrawTextWithCopyArea(w);
	DrawCursor(w);
}

/*
 * Redraw the entire widget, but don't scroll the window much
 */
static void
Draw (FieldWidget w)
{
	EraseCursor (w);
	PositionCursor (w);
	DrawAllText (w);
	DrawCursor (w);
}

/*
 * Like Draw(), but has different rules about scrolling the window to
 * place the cursor in a good place
 */
static void
MassiveChangeDraw (FieldWidget w)
{
	EraseCursor (w);
	MassiveCursorAdjust (w);
	DrawAllText (w);
	DrawCursor (w);
}


/*
 * Motif-like TextField public functions
 *
 * Note that this set of functions is only a subset of the functions available
 * in the real Motif XmTextField.
 */

Boolean
XwFieldGetEditable (Widget w)
{
	FieldWidget fw = (FieldWidget) w;

	if (!XtIsTextField(w))
		return 0;

	return fw->text.editable;
}

int
XwFieldGetInsertionPosition(Widget aw)
{
	FieldWidget w = (FieldWidget) aw;

	if (!XtIsTextField(aw))
		return 0;

	return w->text.cursor_pos;
}

char *
XwFieldGetString (Widget aw)
{
	FieldWidget w = (FieldWidget) aw;
	char *ret;

	if (!XtIsTextField(aw)) {
		ret = XtMalloc(1);
		*ret = '\0';
		return ret;
	}

	ret = XtMalloc(w->text.text_len + 1);
	strncpy(ret, w->text.text, w->text.text_len);
	ret[w->text.text_len] = '\0';
	return ret;
}

/*
 */
void
XwFieldInsert(Widget aw, int pos, char *str)
{
	FieldWidget w = (FieldWidget) aw;
	int len;

	if (!XtIsTextField(aw))
		return;

	if (str && ((len=strlen(str)) > 0) && pos >= 0 && pos <= w->text.text_len) {
		w->text.marked_start = w->text.marked_end = pos;
		TextInsert(w, str, len);
		MassiveChangeDraw(w);
	}
}

void
XwFieldReplace(Widget aw, int first, int last, char *str)
{
	FieldWidget w = (FieldWidget) aw;
	int len;

	if (!XtIsTextField(aw))
		return;

	if (str) {
		len = strlen(str);
		if (last > w->text.text_len)
			last = w->text.text_len;
		if (first <= last) {
			w->text.marked_start = first;
			w->text.marked_end = last;
			TextDeleteHighlighted(w);
			TextInsert(w, str, len);
			MassiveChangeDraw(w);
		}
	}
}

void
XwFieldSetEditable(Widget aw, Boolean editable)
{
	FieldWidget w = (FieldWidget) aw;

	if (!XtIsTextField(aw))
		return;

	w->text.editable = editable;
}

void
XwFieldSetInsertionPosition (Widget aw, int pos)
{
	FieldWidget w = (FieldWidget) aw;

	if (!XtIsTextField(aw))
		return;

	if (pos >= 0 && pos <= w->text.text_len) {
		w->text.cursor_pos = pos;
		MassiveChangeDraw(w);
	}
}

/* ARGSUSED */
void
XwFieldSetSelection (Widget aw, int start, int end, Time time)
{
	FieldWidget w = (FieldWidget) aw;

	if (!XtIsTextField(aw))
		return;

	if (end < start) {
		int temp;
		temp = start;
		start = end;
		end = temp;
	}
	if (start < 0)
		start = 0;
	if (end > w->text.text_len)
		end = w->text.text_len;
	w->text.marked_start = start;
	w->text.marked_end = w->text.cursor_pos = end;
	MassiveChangeDraw (w);
}

/*
 * set the text field value
 */
void
XwFieldSetString (Widget w, char *str)
{
	FieldWidget fw = (FieldWidget) w;

	if (!str)
		str = "(null)";

	if (!XtIsTextField(w))
		return;
	if (!XtIsRealized(w)) {
		fprintf (stderr, "XwFieldSetString not allowed for non realized Widgets\n");
		return;
	}

	fw->text.marked_start = 0;
	fw->text.marked_end = fw->text.text_len;
	if (XtIsRealized(w)) {
		TextDeleteHighlighted (fw);
	}
	SetString (fw, str);
	{
	TextInsert (fw, fw->text.text, fw->text.text_len);
	MassiveChangeDraw (fw);
	}
}

