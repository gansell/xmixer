/*
 * Label.h
 *
 * Copyright (C) 1997 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _Label_h
#define _Label_h

#include <Xw/Base.h>

/* class record constants */

#ifndef XtNmargin
#define XtNmargin	"margin"
#endif
#ifndef XtCMargin
#define XtCMargin	"Margin"
#endif
#ifndef XtNresize
#define XtNresize	"resize"
#define XtCResize	"Resize"
#endif

#define XtNleftPixmap "leftPixmap"
#define XtCLeftPixmap "LeftPixmap"

extern WidgetClass xwLabelWidgetClass;

typedef struct _XwLabelClassRec *XwLabelWidgetClass;
typedef struct _XwLabelRec *XwLabelWidget;

#endif
