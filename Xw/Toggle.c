/*
 * Toggle.c, vi: tabstop=4
 *
 * Copyright (C) 1998 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __FILE__
#define __FILE__ "Toggle.c"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include <X11/Xlib.h>
#include <Xw/ToggleP.h>

#define offset(field) XtOffsetOf (XwToggleRec, field)

/* resource list
 */
static XtResource resources [] = {
	/* toggle */
	{ XtNstate, XtCState, XtRBoolean, sizeof(Boolean),
		offset(toggle.state), XtRBoolean, (XtPointer)0 },
	{ XtNdoubleClickCallback, XtCCallback, XtRCallback, sizeof(XtPointer),
		offset(toggle.double_callbacks), XtRCallback, (XtPointer)NULL },
	/* button */
	{ XtNactiveColor, XtCActiveColor, XtRPixel, sizeof(Pixel),
		offset(button.active_pixel), XtRString, "white" },
	{ XtNaccel, XtCAccel, XtRString, sizeof(String),
		offset(button.accel), XtRImmediate, "toggle() notify()" },
	/* label */
	{ XtNlabel, XtCLabel, XtRString, sizeof(String),
		offset (label.label), XtRImmediate, NULL },
	/* base */
	{ XtNshadowType, XtCShadowType, XtRInt, sizeof(int),
		offset(base.shadow_type), XtRImmediate, (XtPointer)XtShadowUp },
 };

/*
 * action function declarations
 */
static void Highlight (Widget, XEvent*, String*, Cardinal *),
	Unhighlight (Widget, XEvent*, String*, Cardinal *),
	Focus (Widget, XEvent*, String*, Cardinal *),
	GetFocus (Widget, XEvent*, String*, Cardinal *),
	Reset (Widget, XEvent*, String*, Cardinal *),
	Set (Widget, XEvent*, String*, Cardinal *),
	Toggle (Widget, XEvent*, String*, Cardinal *),
	Notify (Widget, XEvent*, String*, Cardinal *),
	Unset (Widget, XEvent*, String*, Cardinal *),
	Popup (Widget, XEvent*, String*, Cardinal *),
	Tab (Widget, XEvent*, String*, Cardinal *);

/*
 * action table
 */
static XtActionsRec actions [] = {
	{"highlight",	Highlight	},
	{"unhighlight",	Unhighlight	},
	{"focus",		Focus		},
	{"get-focus",	GetFocus	},
	{"reset",		Reset		},
	{"set",			Set			},
	{"unset",		Unset		},
	{"toggle",		Toggle		},
	{"notify",		Notify		},
	{"popup",		Popup		},
	{"tab",			Tab			},	/* execute the XtNtabCallback functions */
	};

/*
 * default translation table
 */
static char defaultTranslations [] = "\
<EnterWindow>: highlight()\n\
<LeaveWindow>: unhighlight()\n\
<FocusIn>: focus(in)\n\
<FocusOut>: focus(out)\n\
<Btn1Down>,<Btn1Up>: toggle() notify() get-focus()\n\
<Btn2Down>,<Btn2Up>: toggle() notify() get-focus()\n\
<Btn3Down>: notify() popup()\n\
Shift<Key>Tab: tab(prev)\n\
<Key>Tab: tab(next)\n\
<Key>space: toggle() notify()";

/*
 * method function declarations
 */

static void
	Initialize (Widget treq, Widget tnew, ArgList args, Cardinal *nums),
	ClassInitialize (void);
static Boolean
	SetValues (Widget, Widget, Widget, ArgList, Cardinal* ),
	AcceptFocus (Widget w, Time *t);
static void
	Destroy (Widget w),
	Resize (Widget w),
	Redisplay (Widget w, XEvent *event, Region region);

/*
 * tool functions
 */
static void DrawToggle (Widget w);

/*
 * class record initialization
 */
#define BaseClass ((WidgetClass)&xwBaseClassRec)
#define SuperClass ((WidgetClass)&xwButtonClassRec)
#define ThisClass ((WidgetClass)&xwToggleClassRec)
XwToggleClassRec xwToggleClassRec = {
	{
		/* core_class fields     */
		/* superclass            */ SuperClass,
		/* class_name            */ "XwToggle",
		/* widget_size           */ sizeof(XwToggleRec),
		/* class_initialize      */ ClassInitialize,
		/* class_part_initialize */ NULL,
		/* class_inited          */ False,
		/* initialize            */ Initialize,
		/* initialize_hook       */ NULL,
		/* realize               */ XtInheritRealize,
		/* actions               */ actions,
		/* num_actions           */ XtNumber(actions),
		/* resources             */ resources,
		/* num_resources         */ XtNumber(resources),
		/* xrm_class             */ NULLQUARK,
		/* compress_motion       */ True,
		/* compress_exposure     */ XtExposeCompressMultiple,
		/* compress_enterleave   */ True,
		/* visible_interest      */ True,
		/* destroy               */ Destroy,
		/* resize                */ Resize,
		/* expose                */ Redisplay,
		/* set_values            */ SetValues,
		/* set_values_hook       */ NULL,
		/* set_values_almost     */ XtInheritSetValuesAlmost,
		/* get_values_hook       */ NULL,
		/* accept_focus          */ AcceptFocus,
		/* version               */ XtVersion,
		/* callback_private      */ NULL,
		/* tm_table              */ defaultTranslations,
		/* query_geometry        */ XtInheritQueryGeometry,
		/* display_accelerator   */ XtInheritDisplayAccelerator,
		/* extension             */ NULL
	},
	{	/* base_class fields */
		XtInheritTabAction,
		XtInheritFocusAction,
		XtInheritGetFocusAction,
	},
	{	/* label_class fields */
		XtInheritDrawLabel,
	},
	{	/* button_class fields */
		XtInheritDrawButton,
		XtInheritPopupMenu,
	},
	{	/* toggle_class fields */
		0,
	},
};

WidgetClass xwToggleWidgetClass = ThisClass;


/*
 * method function definitions
 */
#define FontHeight(f)  (int)(f->max_bounds.ascent + f->max_bounds.descent)
#define FontAscent(f)	(int)(f->max_bounds.ascent)
#define FontDescent(f)	(int)(f->max_bounds.descent)
#define TextWidth(f,s)	XTextWidth (f,s,strlen(s))

/*
 */
static void
ClassInitialize (void)
{
	XtRegisterGrabAction (Popup, True,
                ButtonPressMask | ButtonReleaseMask,
                GrabModeAsync, GrabModeAsync);
}

/*
 * initialize Method
 */
static void
Initialize (Widget treq, Widget tnew, ArgList args, Cardinal *num_args)
{
	XwToggleWidget new = (XwToggleWidget) tnew;
#ifdef DEBUG_TOGGLE
	printf ("%s: Initialize(%s) state=%d\n",
		__FILE__, new->core.name, new->toggle.state);
#endif
	if (new->base.shadow_type != XtShadowNone) {
		if (new->toggle.state > 0)
			new->base.shadow_type = XtShadowDown;
		else
			new->base.shadow_type = XtShadowUp;
	}
	new->toggle.multi_click_time = XtGetMultiClickTime (XtDisplay(new)) * 1.2;
	new->toggle.time =0;
}

/*
 * set_values method
 */
static Boolean
SetValues (Widget current, Widget request, Widget super,
			ArgList args, Cardinal *nargs)
{
	Boolean redraw = False;
	XwToggleWidget tw = (XwToggleWidget) current;
	XwToggleWidget newi = (XwToggleWidget) super;
	
	if (tw->toggle.state != newi->toggle.state) {
		redraw = True;
		if (newi->base.shadow_type != XtShadowNone) {
			if (newi->toggle.state) {
				newi->base.shadow_type = XtShadowDown;
			} else {
				newi->base.shadow_type = XtShadowUp;
			}
		}
	}
	if (tw->core.sensitive != newi->core.sensitive) {
		redraw = True;
	}
#ifdef DEBUG_TOGGLE
	printf ("%s: SetValues(%s ..) redraw=%d\n", __FILE__, newi->core.name, (int)redraw);
#endif
	return (redraw);
}

/*
 * destroy method
 */
/* ARGSUSED */
static void
Destroy (Widget w)
{
#ifdef DEBUG_TOGGLE
	printf ("%s: Destroy (%s)\n", __FILE__, w->core.name);
#endif
}

/*
 * resize method
 */
/* ARGSUSED */
static void
Resize (Widget w)
{
#ifdef DEBUG_TOGGLE
	printf ("%s: Resize(%s)\n", __FILE__, w->core.name);
#endif
}

/*
 * redisplay method
 */
static void
Redisplay (Widget w, XEvent *event, Region region)
{
#ifdef DEBUG_TOGGLE
	printf ("%s: Redisplay(%s)\n", __FILE__, w->core.name);
#endif
	if (!XtIsRealized(w))
		return;
	if (!((XwToggleWidget)w)->base.shadow_width) {
		DrawToggle (w);
	} else {
		(*xwButtonClassRec.core_class.expose)(w, event, region);
	}
}

/*
 */
static Boolean
AcceptFocus (Widget w, Time *t)
{
#ifdef DEBUG_TOGGLE
	printf ("%s: AcceptFocus(%s)\n", __FILE__, w->core.name);
#endif
	if (!XtIsRealized(w))
		return (False);
	if (!XtIsManaged(w))
		return (False);
	if (!XtIsSensitive(w))
		return (False);
	return (True);
}


/*
 * action function definitions
 */

/* ARGSUSED */
static void
Highlight (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	/* static Pixel bg; */
	XwToggleWidget tw = (XwToggleWidget) w;
#ifdef DEBUG_TOGGLE
	printf ("%s: Highlight(%s) event=%d\n", __FILE__, tw->label.text, event->type);
#endif
	if (!tw->core.sensitive)
		return;
	tw->label.textGC = tw->button.focusGC;
	tw->button.bgGC = tw->button.focus_bg_gc;
	DrawToggle (w);
}

/* ARGSUSED */
static void
Unhighlight (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	/* static Pixel bg; */
	XwToggleWidget tw = (XwToggleWidget) w;
#ifdef DEBUG_TOGGLE
	printf ("%s: Unhighlight(%s) event=%d\n", __FILE__, tw->label.text, event->type);
#endif
	if (!tw->core.sensitive)
		return;
	tw->label.textGC = tw->label.sensitiveGC;
	tw->button.bgGC = tw->button.std_bg_gc;
	DrawToggle (w);
}

/* ARGSUSED */
static void
Reset (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwToggleWidget bw = (XwToggleWidget) w;
#ifdef DEBUG_TOGGLE
	printf ("%s:Reset(%s)\n", __FILE__, bw->label.text);
#endif
	if (bw->toggle.state) {
		Unset (w, event, params, num_params);
	}
	Unhighlight (w, event, params, num_params);
}

/* ARGSUSED */
static void
Notify (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwToggleWidget tw = (XwToggleWidget) w;
	Boolean doubleClick = False;
#ifdef DEBUG_TOGGLE
	printf ("%s: Notify(%s)\n", __FILE__, tw->label.text);
#endif
	if (event->type == ButtonRelease) {
		if ( event->xbutton.time - tw->toggle.time
				< tw->toggle.multi_click_time ) {
			/* double click
			 */
			doubleClick = True;
			if (!tw->toggle.state) {
				tw->toggle.state = True;
				XtVaSetValues (w, XtNshadowType, XtShadowDown, NULL);
			}
			XtCallCallbackList (w, tw->toggle.double_callbacks, event);
		}
		tw->toggle.time = event->xbutton.time;
	}
	if (!doubleClick)
		XtCallCallbackList (w, tw->button.notify_callbacks, (XtPointer)event);
}

/* ARGSUSED */
static void
Set (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwToggleWidget bw = (XwToggleWidget) w;
#ifdef DEBUG_TOGGLE
	printf ("%s:Set(%s)\n", __FILE__, bw->label.text);
#endif
	if (bw->toggle.state)
		return;
	bw->toggle.state = True;
	XtVaSetValues (w, XtNshadowType, XtShadowDown, NULL);
}

static void
Unset (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwToggleWidget bw = (XwToggleWidget) w;

#ifdef DEBUG_TOGGLE
	printf ("%s:Unset(%s)\n", __FILE__, bw->label.text);
#endif
	if (!bw->toggle.state)
		return;
	bw->toggle.state = False;
	XtVaSetValues (w, XtNshadowType, XtShadowUp, NULL);
}

/*
 */
static void
Popup (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
#ifdef DEBUG_TOGGLE
	printf ("%s: Popup(%s)\n", __FILE__, w->core.name);
#endif
	(*xwToggleClassRec.button_class.popup_menu)(w, event, params, num_params);
}


/*
 */
static void
Toggle (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwToggleWidget tw = (XwToggleWidget) w;
#ifdef DEBUG_TOGGLE
	printf ("%s: Toggle(%s)\n", __FILE__, tw->label.text);
#endif

	if (tw->toggle.state) {
		tw->toggle.state = False;
		XtVaSetValues (w, XtNshadowType, XtShadowUp, NULL);
	} else {
		tw->toggle.state = True;
		XtVaSetValues (w, XtNshadowType, XtShadowDown, NULL);
	}
}


/*
 * called if tab is pressed
 */

/* ARGSUSED */
static void
Tab (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
#ifdef DEBUG_TOGGLE
    fprintf (stderr, "%s: Tab_action()\n", __FILE__);
#endif
    (*xwToggleClassRec.base_class.tab_action)(w, event, params, num_params);
}

/* ARGSUSED */
static void
Focus (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
#ifdef DEBUG_TOGGLE
    fprintf (stderr, "%s: Focus()\n", __FILE__);
#endif
    (*xwToggleClassRec.base_class.focus_action)(w, event, params, num_params);
}

/* ARGSUSED */
static void
GetFocus (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
#ifdef DEBUG_TOGGLE
    fprintf (stderr, "%s: GetFocus()\n", __FILE__);
#endif
    (*xwToggleClassRec.base_class.getfocus_action)(w,event, params, num_params);
}

/* tool functions
 */
static void
DrawToggle (Widget w)
{
	XwToggleWidget tw = (XwToggleWidget) w;
	GC gc;
#ifdef DEBUG_TOGGLE
	printf ("%s: DrawToggle(%s)\n", __FILE__, w->core.name);
#endif
	if (!XtIsRealized(w))
		return;
	(*xwToggleClassRec.button_class.draw_button)(w);
	if (!tw->base.shadow_width) {
		if (tw->toggle.state) {
			/* draw frame */
			gc = tw->label.textGC;
		} else {
			gc = tw->button.bgGC;
		}
		XDrawRectangle (XtDisplay(w), XtWindow(w), gc, 0, 0,
			tw->core.width-1, tw->core.height-1);
	}
}

/*
 * for external usage
 */
/*
 */
Boolean
XwIsSet (Widget w)
{
	XwToggleWidget tw = (XwToggleWidget) w;
	return ((Boolean)tw->toggle.state);
}
/*EOF*/

