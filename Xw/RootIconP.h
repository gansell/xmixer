/*
 * RootIconP.h
 *
 * Copyright (C) 1998 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __RootIconP_h__
#define __RootIconP_h__

/* include superclass private header file */
#include <X11/CoreP.h>
#include <X11/ShellP.h>
#include <Xw/RootIcon.h>

typedef struct {
    int empty;
} XwRootClassPart;

typedef struct _XwRootClassRec {
    CoreClassPart			core_class;
	CompositeClassPart		composite_class;
	ShellClassPart			shell_class;
	OverrideShellClassPart	override_shell_class;
    XwRootClassPart			root_class;
} XwRootClassRec;

typedef struct {
    /* resources */
    char* resource;
    /* private state */
} XwRootPart;

typedef struct _RootWindowRec {
    CorePart		core;
	CompositePart	composite;
    XwRootPart		root;
} XwRootRec;

extern XwRootClassRec xwRootClassRec;

#endif /* __RootIconP_h__ */

