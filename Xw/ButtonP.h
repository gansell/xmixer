/*
 * ButtonP.h
 *
 * Copyright (C) 1997 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _ButtonP_h
#define _ButtonP_h

#include <X11/CoreP.h>
#include <Xw/BaseP.h>
#include <Xw/LabelP.h>
#include <Xw/Button.h>

#define XtInheritDrawButton ((XtWidgetProc) _XtInherit)
#define XtInheritPopupMenu  ((XtActionProc) _XtInherit)

/* new fields for the base widget class record
 */
typedef struct {
	XtWidgetProc draw_button;
	XtActionProc popup_menu;
} XwButtonClassPart;

/*
 * full class record declaration
 */
typedef struct _XwButtonClassRec {
	CoreClassPart		core_class;
	XwBaseClassPart		base_class;
	XwLabelClassPart	label_class;
	XwButtonClassPart	button_class;
} XwButtonClassRec;

/*
 * instance definitions
 */

typedef struct {
	/* resources */
	Pixel active_pixel;
	Boolean popup_at_mouse;
	String menu_name;			/* if used as menu button .. */
	Pixmap pic;
	XtCallbackList notify_callbacks;

	/* private Widget state
	 */
	unsigned int pic_width;
	unsigned int pic_height;
	unsigned int pic_depth;
	GC bgGC;			/* we need this for filling the background */
	GC focus_bg_gc;
	GC std_bg_gc;
	GC focusGC;			/* for the highlighted text */
	Boolean set;
	String accel;
} XwButtonPart;

/*
 * full instance record declaration
 */
typedef struct _XwButtonRec {
	CorePart		core;
	XwBasePart		base;
	XwLabelPart		label;
	XwButtonPart	button;
} XwButtonRec;

extern XwButtonClassRec xwButtonClassRec;

#endif

