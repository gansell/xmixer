/*
 * LabelP.h
 *
 * Copyright (C) 1997 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _LabelP_h
#define _LabelP_h

#include <X11/CoreP.h>
#include <Xw/BaseP.h>
#include <Xw/Label.h>

#define XtInheritDrawLabel ((XtWidgetProc) _XtInherit)

/* new fields for the base widget class record
 */
typedef struct {
	XtWidgetProc draw_label;
} XwLabelClassPart;

/*
 * full class record declaration
 */
typedef struct _XwLabelClassRec {
	CoreClassPart	core_class;
	XwBaseClassPart	base_class;
	XwLabelClassPart	label_class;
} XwLabelClassRec;

/*
 * instance definitions
 */

typedef struct {
	/* resources */
	String label;
	XFontStruct *font;
	Dimension margin;
	Pixmap left_pixmap;

	/* private Widget state */
	GC textGC;		/* gc for drawing the text */
	GC sensitiveGC;
	GC insensitiveGC;
	String text;	/* the label text */
	int underline_pos;	/* position of the underlined char in the string */
	int underline_x;
	int underline_y;
	int underline_len;
	int start_x;	/* starting points for the text draw function */
	int start_y;
	int lines;		/* number of text lines of the label */
	unsigned int lpm_y;
	unsigned int lpm_depth;	/* depth of the left pixmap */
	unsigned int lpm_width;	/* width .. */
	unsigned int lpm_height;	/* height .. */
} XwLabelPart;

/*
 * full instance record declaration
 */
typedef struct _XwLabelRec {
	CorePart		core;
	XwBasePart		base;
	XwLabelPart		label;
} XwLabelRec;

extern XwLabelClassRec xwLabelClassRec;

#endif

