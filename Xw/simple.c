/*
 * simple.c	A simple test program for the Xw Widget Set
 *
 * Copyright (c) 1998 Rasca Gmelch
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  The author makes no representations about the suitability
 * of this software for any purpose.  It is provided "as is" without express
 * or implied warranty.
 *
 * THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL
 * THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <stdio.h>
#include <Xw/Box.h>
#include <Xw/Label.h>
#include <Xw/Button.h>
#include <Xw/Toggle.h>
#include <Xw/Field.h>

static Widget toplevel, box;
static XtAppContext app_con;

void
Quit (Widget w, XtPointer client, XtPointer call)
{
	XtVaSetValues ((Widget)client, XtNsensitive, True, NULL);
	XtVaSetValues ((Widget)w, XtNsensitive, False, NULL);
	XtUnmapWidget (toplevel);
	XtDestroyWidget (toplevel);
	exit(0);
}

void
tabCallback (Widget w, XtPointer client, XtPointer call)
{
	printf ("tab was pressed\n");
}

static char *fallback[] = {
	"*XwLabel*font: -*-helvetica-*",
	"*quit.label: &Quit",
	"*toggle.label: &Toggle",
	"*button.label: &Button",
	"*box.background: yellow",
	NULL};

/*
 */
int
main (int argc, char *argv[])
{
	Widget quit, toggle, field1, field2, field3;

	toplevel = XtVaAppInitialize (&app_con, "XSimple",
				NULL, 0,
			     &argc, argv, fallback,
				XtNinput, True,
				NULL);

	box = XtVaCreateManagedWidget("box", xwBoxWidgetClass, toplevel, NULL);
	quit = XtVaCreateManagedWidget ("quit", xwButtonWidgetClass, box, NULL);
	toggle = XtVaCreateManagedWidget ("toggle", xwToggleWidgetClass, box, NULL);
	field1 = XtVaCreateManagedWidget ("&edit", xwFieldWidgetClass, box, NULL);
	field2 = XtVaCreateManagedWidget ("&editNum", xwFieldWidgetClass, box,NULL);
	field3 = XtVaCreateManagedWidget ("&editHid", xwFieldWidgetClass, box,NULL);

	XtVaSetValues (quit, XtNprev, field3, XtNnext, toggle, NULL);
	XtVaSetValues (toggle, XtNprev, quit, XtNnext, field1, NULL);
	XtVaSetValues (field1, XtNprev, toggle, XtNnext, field2, NULL);
	XtVaSetValues(field2,XtNprev,field1,XtNnext,field3,XtNonlyNumber,True,NULL);
	XtVaSetValues (field3, XtNprev, field2, XtNnext, quit, XtNecho, False,NULL);
	XtAddCallback (quit, XtNcallback, Quit, (XtPointer) toggle);
	XtAddCallback (field1, XtNtabCallback, tabCallback, (XtPointer) field2);
	XtAddCallback (field2, XtNtabCallback, tabCallback, (XtPointer) field3);
	XtAddCallback (field3, XtNtabCallback, tabCallback, (XtPointer) quit);

	XtRealizeWidget(toplevel);
	XtInstallAllAccelerators (field1, box);
	XtInstallAllAccelerators (field2, box);
	XtInstallAllAccelerators (field3, box);
	XtInstallAllAccelerators (box, box);
	XtInstallAllAccelerators (quit, box);
	XtInstallAllAccelerators (toggle, box);
	XtAppMainLoop(app_con);
	return (0);
}

