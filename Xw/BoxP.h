/*
 * BoxP.h
 *
 * Copyright (C) 1998 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __BoxP_h__
#define __BoxP_h__

#include <X11/CoreP.h>
#include <Xw/Box.h>

/* new fields for the box widget class record
 */
typedef struct {
	/* XtWidgetProc draw_frame; */
	XtActionProc tab_action;
} XwBoxClassPart;

#define XtInheritTabAction ((XtActionProc) _XtInherit)

/*
 * full class record declaration
 */
typedef struct _XwBoxClassRec {
	CoreClassPart		core_class;
	CompositeClassPart	composite_class;
	XwBoxClassPart		box_class;
} XwBoxClassRec;

/*
 * instance definitions
 */
typedef struct {
	/* resources */
	Dimension margin;
	Boolean resize;
	Boolean acceptDrop;
	XtCallbackList tab_callbacks;
	XtPointer data;		/* general purpose data pointer */
	Dimension hspace, vspace;
	XtOrientation orientation;
	Boolean expand;

	/* private state */
	Dimension pref_width;	/* preferred width and height */
	Dimension pref_height;
} XwBoxPart;

/*
 * full instance record declaration
 */
typedef struct _XwBoxRec {
	CorePart		core;
	CompositePart	composite;
	XwBoxPart		box;
} XwBoxRec;

extern XwBoxClassRec xwBoxClassRec;

#endif

