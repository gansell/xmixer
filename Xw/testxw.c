/*
 * textxw.c	A simple test program for the Xw Widget Set
 *
 * Copyright (c) 1998 Rasca Gmelch
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  The author makes no representations about the suitability
 * of this software for any purpose.  It is provided "as is" without express
 * or implied warranty.
 *
 * THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS.  IN NO EVENT SHALL
 * THE AUTHOR BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
 * SOFTWARE.
 */

#include <stdio.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <Xw/Box.h>
#include <Xw/Label.h>
#include <Xw/Button.h>
#include <Xw/Toggle.h>
#include <Xw/Field.h>
#include "testxw.xbm"

Widget toplevel, form, t1, t2;
XtAppContext app_con;

void
Quit (Widget w, XtPointer client, XtPointer call)
{
	XtVaSetValues ((Widget)client, XtNsensitive, True, NULL);
	XtVaSetValues ((Widget)w, XtNsensitive, False, NULL);
	XtUnmapWidget (toplevel);
	XtDestroyWidget (toplevel);
	exit(0);
}

void
Toggle (Widget w, XtPointer client, XtPointer call)
{
	printf ("  toggle pressed\n");
}

void
TestCallback (Widget w, XtPointer client, XtPointer call)
{
	FieldReturnStruct *ret;
	char *str, *s, *val;

	ret = (FieldReturnStruct *) call;
	printf("ret->string=%s\n", ret->string);
	str = XwFieldGetString(w);
	printf("TEXT: item=%s\n", str);
	s = str;
	while (*s)
		*s++ = '*';
	XwFieldSetString(w, str);
	XwFieldInsert(w, 4, "4");
	XwFieldReplace(w, 8, 10, "XXX");
	XwFieldSetSelection (w, 5, 10, (Time)0);
	XtVaGetValues(w, XtNstring, &val, NULL);
	printf("GetValues: %s\n", val);
	XtFree(str);
}

/*
 */
void
EchoCallback (w, client, call)
Widget w;
XtPointer client;
XtPointer call;
{
  FieldReturnStruct *ret;

  ret = (FieldReturnStruct *) call;
  printf("ret->string=%s\n", ret->string);
}

static char *fallback_resources[] = {
"*XwLabel*font: -*-helvetica-*",
"*background: #e0e0e0",
"*variablewidth*font: -adobe-helvetica-medium-r-normal--*-120-*",
"*monospaced*font: -*-courier-medium-r-*-*-14-*-*-*-*-*-*",
"*form*font: -*-fixed-bold-r-normal-*-13-*",
"*Form*top: ChainTop",
"*Form*bottom: ChainBottom",
"*Form*left: ChainLeft",
"*Form*right: ChainRight",
"*quit.label: &Quit",
"*toggle.label: &Toggle",
/* "*quit.accelerators: Alt<KeyPress>q: set() notify() unset()", */
	NULL
};

/*
 */
int
main (int argc, char **argv)
{
	Widget quit, label, toggle, pic, box;
	Pixmap pic_bm;
	Display *disp;
	Window root;

	toplevel = XtVaAppInitialize (&app_con, "XTextxw", NULL, 0,
			     &argc, argv, fallback_resources, XtNinput, True, NULL);

	disp = XtDisplay (toplevel);
	root = RootWindow (disp, DefaultScreen (disp));
	pic_bm = XCreateBitmapFromData (disp, root,
				testxw_bits, testxw_width, testxw_height);

	form = XtVaCreateManagedWidget("form", xwBoxWidgetClass, toplevel,
				XtNexpand, True, XtNorientation, XtorientVertical, NULL);
	box = XtVaCreateManagedWidget("box1", xwBoxWidgetClass, form,
				XtNorientation, XtorientHorizontal, NULL);
	quit = XtVaCreateManagedWidget ("quit", xwButtonWidgetClass, box,
				NULL);
	label= XtVaCreateManagedWidget("label", xwLabelWidgetClass, box,
				XtNlabel, "This &is an example \n2 line label ..", NULL);
	toggle = XtVaCreateManagedWidget ("toggle", xwToggleWidgetClass, box,
				XtNstate, True, NULL);
	pic = XtVaCreateManagedWidget ("pic", xwButtonWidgetClass, box,
				XtNbitmap, pic_bm, NULL);
	XtAddCallback (quit, XtNcallback, Quit, (XtPointer) toggle);
	XtAddCallback(toggle, XtNcallback, Toggle, (XtPointer) quit);

	box = XtVaCreateManagedWidget("box2", xwBoxWidgetClass, form,
				XtNorientation, XtorientVertical, XtNexpand, True, NULL);
	/* t1 = XtVaCreateManagedWidget("variablewidth", xwFieldWidgetClass, box, */
				/* XtNstring, "A Free alternative to the Motif XmTextField", */
				/* NULL); */
/*	XtAddCallback(t1, XtNactivateCallback, EchoCallback, (XtPointer) NULL); */

/*	t2 = XtVaCreateManagedWidget("monospaced", xwFieldWidgetClass, form,
			       XtNstring, "Fixed Length",
			       XtNinsertPosition, 0,
			       XtNlength, 16,
			       XtNfromVert, t1,
			       NULL);
	XtAddCallback(t2, XtNactivateCallback, EchoCallback, (XtPointer) NULL); */

	XtRealizeWidget(toplevel);
	/* XtInstallAllAccelerators (form, quit); */
	/* XtInstallAllAccelerators (form, toggle); */
	XtAppMainLoop(app_con);
	return (0);
}

