/*
 * Field	A single line text entry widget, based on Rob's TextField Widget
 *          but heavily changed
 *
 * Copyright (c) 1997-98 Rasca
 * Copyright (c) 1995 Robert W. McMullen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Authors: Rob McMullen <rwmcm@orion.ae.utexas.edu>
 *          http://www.ae.utexas.edu/~rwmcm
 *          Rasca
 *          http://home.pages.de/~rasca/
 */

#ifndef __FieldP_H__
#define __FieldP_H__

#include <X11/CoreP.h>
#include <Xw/BaseP.h>
#include <Xw/Field.h>

#define DEFAULT_MAX_LENGTH	256

typedef struct {
  int dummy;			/* keep compiler happy with dummy field */
} XwFieldClassPart;

typedef struct _FieldClassRec {
	CoreClassPart core_class;
	XwBaseClassPart base_class;
	XwFieldClassPart field_class;
} FieldClassRec;


typedef struct {
	/* public stuff ... */
	XFontStruct *font;
	Dimension margin;
	unsigned int max_length;
	Boolean echo;
	Boolean editable;
	Boolean show_cursor;
	Boolean allow_select;
	Boolean pending_delete;
	Boolean only_number;
	String string;
	Pixel active_bg_pixel;
	Pixel normal_bg_pixel;
	XtCallbackList activate_callback;

	/* private stuff ... */
	GC drawGC;			/* GC for drawing and copying */
	GC markGC;			/* GC for highlighting text */
	GC cursorGC;		/* GC for cursor (not clipped like drawGC) */
	GC eraseGC;			/* GC for erasing (not clipped) */

	int cursor_pos;			/* text position of cursor */
	int old_cursor_pos;		/* previous position */
	int old_cursor_x;		/* previous pixel pos of cursor */
	int marked_start;		/* text pos of leftmost highlight pos */
	int marked_end;		/* text pos of rightmost highlight pos */
	int marked_pivot_start;/* left pivot pos for ExtendHighlight */
	int marked_pivot_end;	/* right ... */
	int old_marked_start;	/* save data */
	int old_marked_end;

	String text;			/* pointer to the text */
	int text_len;			/* current length of text */

	String selection_text;	/* pointer to text selection, when needed */
	int selection_len;		/* length */

	int FastInsertCursorStart;	/* data storage for some text optimization */
	int FastInsertTextLen;

	Dimension view_width;	/* visible width of text in pixel */
	int x_offset;			/* offset from x=0 to start of text string */
	int old_x_offset;
	int y_offset;			/* y pixel offset to baseline of font */
	int text_width;			/* char width of text */
	int old_text_width;

	XtIntervalId timer_id;	/* timer for double click test */
	int timer_x;			/* save event x pos */
	int highlight_time;		/* time delay for scrolling */
	int multi_click_time;	/* local storage for XtGetMultiClickTime */
	int time;
} XwFieldPart;

typedef struct _FieldRec {
	CorePart	core;
	XwBasePart	base;
	XwFieldPart	text;
} XwFieldRec;

extern FieldClassRec xwFieldClassRec;

#endif /* _FieldP_H */

