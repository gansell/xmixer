/*
 * ToggleP.h
 *
 * Copyright (C) 1997 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _ToggleP_h
#define _ToggleP_h

#include <X11/CoreP.h>
#include <Xw/BaseP.h>
#include <Xw/LabelP.h>
#include <Xw/ButtonP.h>
#include <Xw/Toggle.h>

/* new fields for the base widget class record
 */
typedef struct {
	int makecchappy;
} XwToggleClassPart;

/*
 * full class record declaration
 */
typedef struct _XwToggleClassRec {
	CoreClassPart		core_class;
	XwBaseClassPart		base_class;
	XwLabelClassPart	label_class;
	XwButtonClassPart	button_class;
	XwToggleClassPart	toggle_class;
} XwToggleClassRec;

/*
 * instance definitions
 */

typedef struct {
	/* resources */
	XtCallbackList double_callbacks;
	/* Pixel active_pixel; */
	Boolean state;

	/* private Widget state
	 */
	int multi_click_time;
	int time;
} XwTogglePart;

/*
 * full instance record declaration
 */
typedef struct _XwToggleRec {
	CorePart		core;
	XwBasePart		base;
	XwLabelPart		label;
	XwButtonPart	button;
	XwTogglePart	toggle;
} XwToggleRec;

extern XwToggleClassRec xwToggleClassRec;

#endif

