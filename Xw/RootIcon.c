/*
 * RootIcon.c
 *
 * Copyright (C) 1998 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include "RootIconP.h"

static void
	Initialize (Widget t, Widget n, ArgList args, Cardinal *num_args),
	Realize (Widget w, XtValueMask *vmask, XSetWindowAttributes *attributes);

#define SuperClass ((CompositeWidgetClass)&overrideShellClassRec)
XwRootClassRec xwRootClassRec = {
  { /* core fields */
    /* superclass		*/	(WidgetClass) SuperClass,
    /* class_name		*/	"XwRootIcon",
    /* widget_size		*/	sizeof(XwRootRec),
    /* class_initialize	*/	NULL,
    /* class_part_initialize*/	NULL,
    /* class_inited		*/	FALSE,
    /* initialize		*/	Initialize,
    /* initialize_hook	*/	NULL,
    /* realize			*/	Realize,
    /* actions			*/	NULL,
    /* num_actions		*/	0,
    /* resources		*/	NULL,
    /* num_resources	*/	0,
    /* xrm_class		*/	NULLQUARK,
    /* compress_motion	*/	TRUE,
    /* compress_exposure*/	TRUE,
    /* compress_enterleave*/	TRUE,
    /* visible_interest	*/	FALSE,
    /* destroy			*/	NULL,
    /* resize			*/	NULL,
    /* expose			*/	NULL,
    /* set_values		*/	NULL,
    /* set_values_hook	*/	NULL,
    /* set_values_almost*/	XtInheritSetValuesAlmost,
    /* get_values_hook	*/	NULL,
    /* accept_focus		*/	NULL,
    /* version			*/	XtVersion,
    /* callback_private	*/	NULL,
    /* tm_table			*/	NULL,
    /* query_geometry	*/	XtInheritQueryGeometry,
    /* display_accelerator	*/	XtInheritDisplayAccelerator,
    /* extension		*/	NULL
	},
	{	/* composite */
		NULL,
		NULL,
		XtInheritInsertChild,
		XtInheritDeleteChild,
		NULL,
	},
	{	/* shell */
		/* extension */		NULL,
	},
	{	/* override */
		/* extension */		NULL,
	},
	{	/* rootIcon fields */
		/* empty */			0
	}
};

WidgetClass xwRootWidgetClass = (WidgetClass)&xwRootClassRec;

/*
 */
static void
Initialize (Widget t, Widget n, ArgList args, Cardinal *num_args)
{
	/* n->core.width = 1024; */
	/* n->core.height= 768; */
}

/*ARGSUSED*/
static void
Realize (w, value_mask, attributes)
    Widget	w;
    XtValueMask *value_mask;
    XSetWindowAttributes *attributes;
{
    w->core.window = RootWindowOfScreen(w->core.screen);
}

