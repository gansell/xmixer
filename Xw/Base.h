/*
 * Base.h
 *
 * Copyright (C) 1997 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _Base_h
#define _Base_h

#ifndef XtNforeground
#define XtNforeground	"foreground"
#endif
#ifndef XtCForeground
#define XtCForeground	"Foreground"
#endif
#ifndef XtNhighlight
#define XtNhighlight	"highlight"
#endif
#ifndef XtCHighlight
#define XtCHighlight	"Highlight"
#endif
#ifndef XtNshadowTopColor
#define XtNshadowTopColor	"shadowTopColor"
#endif
#ifndef XtCShadowTopColor
#define XtCShadowTopColor	"ShadowTopColor"
#endif
#ifndef XtNshadowBottomColor
#define XtNshadowBottomColor	"shadowBottomColor"
#endif
#ifndef XtCShadowBottomColor
#define XtCShadowBottomColor	"ShadowBottomColor"
#endif
#ifndef XtNnotSensitiveColor
#define XtNnotSensitiveColor	"notSensitiveColor"
#endif
#ifndef XtCNotSensitiveColor
#define	XtCNotSensitiveColor	"NotSensitiveColor"
#endif
#ifndef XtNshadowWidth
#define XtNshadowWidth	"shadowWidth"
#endif
#ifndef XtCShadowWidth
#define XtCShadowWidth	"ShadowWidth"
#endif
#ifndef XtNshadowType
#define XtNshadowType	"shadowType"
#endif
#ifndef XtCShadowType
#define XtCShadowType	"ShadowType"
#endif
#ifndef XtNresize
#define XtNresize		"resize"
#define XtCResize		"Resize"
#endif
#ifndef XtNuserData
#define XtNuserData			"userData"
#define XtCUserData			"UserData"
#endif
#define XtNacceptDrop	"acceptDrop"
#define XtCAcceptDrop	"AcceptDrop"

#define XtNtabCallback	"tabCallback"
#define XtCTabCallback	"TabCallback"

#define XtNprev			"prev"
#define XtCPrev			"Prev"
#define XtNnext			"next"
#define XtCNext			"Next"

#define XtShadowNone	0
#define XtShadowUp		1
#define XtShadowDown	2
#define XtShadowFlat	3

#ifndef ShadowWidth
#define ShadowWidth(w)	(w->base.shadow_width)
#endif

/* class record constants */
extern WidgetClass xwBaseWidgetClass;

typedef struct _XwBaseClassRec *XwBaseWidgetClass;
typedef struct _XwBaseRec *XwBaseWidget;

#define XtIsXwWidget(w) XtIsSubclass((Widget)w, xwBaseWidgetClass)

#endif
