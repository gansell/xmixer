/*
 * Box.c; vi: tabstop=4
 *
 * Copyright (C) 1998 Rasca, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __FILE__
#define __FILE__ "Box.c"
#endif
#include <stdio.h>
#include <X11/IntrinsicP.h>
#include <X11/StringDefs.h>
#include <Xw/BoxP.h>

#ifndef max
#define max(a,b) (a > b ? a : b)
#endif

/* resource list */
#define offset(field) XtOffsetOf (XwBoxRec, field)

static XtResource resources [] = {
	/* box defaults
	 */
	{	XtNmargin, XtCMargin, XtRDimension, sizeof(Dimension),
		offset(box.margin), XtRImmediate, (XtPointer)4 },
	{	XtNresize, XtCResize, XtRBoolean, sizeof(Boolean),
		offset(box.resize), XtRImmediate, (XtPointer)0 },
	{	XtNacceptDrop, XtCAcceptDrop, XtRBoolean, sizeof(Boolean),
		offset(box.acceptDrop), XtRImmediate, (XtPointer)0 },
	{	XtNtabCallback, XtCTabCallback, XtRCallback, sizeof(XtPointer),
		offset(box.tab_callbacks), XtRCallback, NULL },
	{	XtNuserData, XtCUserData, XtRPointer, sizeof(XtPointer),
		offset(box.data), XtRImmediate, NULL },
	{	XtNhSpace, XtCHSpace, XtRDimension, sizeof(Dimension),
		offset(box.hspace), XtRImmediate, (XtPointer)4 },
	{	XtNvSpace, XtCVSpace, XtRDimension, sizeof(Dimension),
		offset(box.vspace), XtRImmediate, (XtPointer)4 },
	{	XtNorientation, XtCOrientation, XtROrientation, sizeof(XtOrientation),
		offset(box.orientation), XtRImmediate, (XtPointer)XtorientVertical },
	{	XtNexpand, XtCExpand, XtRBoolean, sizeof(Boolean),
		offset(box.expand), XtRString, (XtPointer)"False" },
	/* core defaults
	 */
	{	XtNborderWidth, XtCBorderWidth, XtRDimension, sizeof(Dimension),
		offset(core.border_width), XtRImmediate, (XtPointer)0 },
	{	XtNbackground, XtCBackground, XtRPixel, sizeof(Pixel),
		offset(core.background_pixel), XtRString, "grey90" },
};

/*
 * action function declarations
 */
static void Tab (Widget, XEvent*, String*, Cardinal*);
static void FocusToChild (Widget, XEvent*, String*, Cardinal*);

/*
 * action table
 */
static XtActionsRec actions [] = {
	{"tab",				Tab},
	{"focusToChild",	FocusToChild},
	};

/*
 * default translation table
 */
static char defaultTranslations [] = "\
	<Key>Tab:	tab()\n\
	<FocusIn>:	focusToChild()";

/*
 * method function declarations
 */
static void
	ClassPartInitialize (WidgetClass wc),
	Initialize (Widget t, Widget n, ArgList args, Cardinal *num_args),
	Realize (Widget w, Mask *valMask, XSetWindowAttributes *attrib),
	Resize (Widget w);
static Boolean
	SetValues (Widget, Widget, Widget, ArgList, Cardinal*),
	AcceptFocus (Widget w, Time *t);
static XtGeometryResult
	QueryGeometry (Widget, XtWidgetGeometry*, XtWidgetGeometry*);
/* */
static XtGeometryResult
	GeometryManager (Widget, XtWidgetGeometry*, XtWidgetGeometry*);
static void
	ChangeManaged (Widget);

#define SuperClass ((CompositeWidgetClass)&compositeClassRec)
/*
 * class record initialization
 */
XwBoxClassRec xwBoxClassRec = {
	{	/* core_class fields     */
		/* superclass            */ (WidgetClass) SuperClass,
		/* class_name            */ "XwBox",
		/* widget_size           */ sizeof(XwBoxRec),
		/* class_initialize      */ NULL,
		/* class_part_initialize */ ClassPartInitialize,
		/* class_inited          */ False,
		/* initialize            */ Initialize,
		/* initialize_hook       */ NULL,
		/* realize               */ Realize,
		/* actions               */ actions,
		/* num_actions           */ XtNumber(actions),
		/* resources             */ resources,
		/* num_resources         */ XtNumber(resources),
		/* xrm_class             */ NULLQUARK,
		/* compress_motion       */ True,
		/* compress_exposure     */ True,
		/* compress_enterleave   */ True,
		/* visible_interest      */ False,
		/* destroy               */ NULL,
		/* resize                */ Resize,
		/* expose                */ NULL,
		/* set_values            */ SetValues,
		/* set_values_hook       */ NULL,
		/* set_values_almost     */ XtInheritSetValuesAlmost,
		/* get_values_hook       */ NULL,
		/* accept_focus			*/ AcceptFocus,
		/* version				*/ XtVersion,
		/* callback_private		*/ NULL,
		/* tm_table				*/ defaultTranslations,
		/* query_geometry		*/ QueryGeometry,
		/* display_accelerator	*/ XtInheritDisplayAccelerator,
		/* extension			*/ NULL,
	},
	{	/* composite_class fields */
		/* geometry_manager		*/	GeometryManager,
		/* change_managed		*/	ChangeManaged,
		/* insert_child			*/	XtInheritInsertChild,
		/* delete_child			*/	XtInheritDeleteChild,
		/* extension			*/	NULL,
	},
	{	/* box_class fields		*/
		/* tab_action			*/ Tab,
	},
};

WidgetClass xwBoxWidgetClass = (WidgetClass) &xwBoxClassRec;

#define WidgetWidth(w) (w->core.width + 2 * w->core.border_width)
#define WidgetHeight(w) (w->core.height + 2 * w->core.border_width)
/*
 *
 */
static void
Layout (XwBoxWidget bw, Dimension width, Dimension height, Boolean resize)
{
	int i;
	Widget child;
	Position x, y;
	Dimension pwidth, pheight;
	x = y = bw->box.margin;
	pwidth  = 0;
	pheight = 0;

	if (bw->box.orientation == XtorientVertical) {
		for (i = 0; i < bw->composite.num_children; i++) {
			child = bw->composite.children[i];
			if (XtIsManaged (child)) {
				pheight += WidgetHeight(child);
				if (i)
					pheight += bw->box.vspace;
				pwidth = max(pwidth, WidgetWidth(child));
				XtMoveWidget (child, x, y);
				y += WidgetHeight(child) + bw->box.vspace;
			}
		}
		pwidth += 2 * bw->box.margin;
		pheight += 2 * bw->box.margin;
	} else { /* horizontal layout */
		for (i = 0; i < bw->composite.num_children; i++) {
			child = bw->composite.children[i];
			if (XtIsManaged (child)) {
				pwidth += WidgetWidth(child);
				if (i)
					pwidth += bw->box.hspace;
				pheight = max(pheight, WidgetHeight(child));
				XtMoveWidget (child, x, y);
				x += WidgetWidth(child) + bw->box.hspace;
			}
		}
		pheight += 2 * bw->box.margin;
		pwidth += 2 * bw->box.margin;
	}
	if (bw->box.expand) {
		if (bw->box.orientation == XtorientVertical) {
			Dimension child_width;
			if (resize)
				child_width = width - 2 * bw->box.margin;
			else
				child_width = pwidth - 2 * bw->box.margin;
			for (i = 0; i < bw->composite.num_children; i++) {
				child = bw->composite.children[i];
				if (XtIsManaged (child)) {
					XtResizeWidget (child,
						child_width - 2 * child->core.border_width,
						child->core.height, child->core.border_width);
				}
			}
		} else {
			/* horizontal alignment */
			Dimension child_height;
			if (resize)
				child_height = height - 2 * bw->box.margin;
			else
				child_height = pheight - 2 * bw->box.margin;
			for (i = 0; i < bw->composite.num_children; i++) {
				child = bw->composite.children[i];
				if (XtIsManaged (child)) {
					XtResizeWidget (child, child->core.width,
						child_height - 2 * child->core.border_width,
						child->core.border_width);
				}
			}
		}
	}
	bw->box.pref_width = pwidth;
	bw->box.pref_height= pheight;
#ifdef DEBUG_BOX
	printf ("%s:  Layout (%s) geom=%dx%d core=%dx%d preferred=%dx%d\n",__FILE__,
			bw->core.name, width, height, bw->core.width, bw->core.height,
			pwidth, pheight);
#endif
}

/*
 * method function definitions
 */

/*
 * class_part_initialize method
 * downward chained: first this one and then subclasses
 */
static void
ClassPartInitialize (WidgetClass wc) {
	XwBoxWidgetClass c = (XwBoxWidgetClass) wc;
	if (c->box_class.tab_action == XtInheritTabAction)
		c->box_class.tab_action = Tab;
}

/*
 * initialize method
 */
static void
Initialize (Widget treq, Widget tnew, ArgList args, Cardinal *num_args)
{
	XwBoxWidget new_box = (XwBoxWidget) tnew;
#ifdef DEBUG_BOX
	printf ("%s: Initialize (%s) childs=%d\n",
			__FILE__, new_box->core.name, new_box->composite.num_children);
#endif
	new_box->box.pref_width = new_box->box.margin;
	new_box->box.pref_height= new_box->box.margin;
	if (new_box->core.width == 0)
		new_box->core.width = new_box->box.pref_width;
	if (new_box->core.height == 0)
		new_box->core.height = new_box->box.pref_height;
}

/*
 * realize method
 */
static void
Realize (Widget w, Mask *valMask, XSetWindowAttributes *attrib)
{
#ifdef DEBUG_BOX
	printf ("%s: Realize (%s) core=%dx%d\n",
			__FILE__, w->core.name, w->core.width, w->core.height);
#endif
	attrib->bit_gravity = NorthWestGravity;
    *valMask |= CWBitGravity;

	XtCreateWindow (w, (unsigned)InputOutput, (Visual *)CopyFromParent,
		*valMask, attrib);
}

/*
 * resize method
 * move or resize childs to fit into the box
 */
static void
Resize (Widget w)
{
	XwBoxWidget bw = (XwBoxWidget) w;
#ifdef DEBUG_BOX
	printf ("%s: Resize (%s) children=%d, core=%dx%d\n",
				__FILE__, w->core.name, bw->composite.num_children,
				w->core.width, w->core.height);
#endif
	Layout (bw, w->core.width, w->core.height, TRUE);
}

/*
 * set_values method
 */
static Boolean
SetValues (Widget curr, Widget req, Widget reply, ArgList args, Cardinal *nargs)
{
	Boolean redraw = False;
	/* XwBoxWidget bw = (XwBoxWidget) curr; */
#ifdef DEBUG_BOX
	XwBoxWidget new_box= (XwBoxWidget) reply;
	printf ("%s: SetValues (%s ..) rv=%d\n", __FILE__, new_box->core.name, (int)redraw);
#endif
	return (redraw);
}

/*
 * accept_focus method
 */
static Boolean
AcceptFocus (Widget w, Time *t)
{
#ifdef DEBUG_BOX
	printf ("%s: AcceptFocus (%s ..)\n", __FILE__, w->core.name);
#endif
	if (!XtIsRealized(w))
		return (False);
	if (!XtIsManaged(w))
		return (False);
	if (!XtIsSensitive(w))
		return (False);
	return (True);
}

/*
 * query_geometry method
 * called by the parent to give a preferred size
 */
static XtGeometryResult
QueryGeometry (Widget w, XtWidgetGeometry *request, XtWidgetGeometry *reply)
{
	XtGeometryResult result = XtGeometryYes;
	XwBoxWidget bw = (XwBoxWidget) w;

	request->request_mode &= CWWidth | CWHeight;
	if ( request->request_mode == 0 )
		/* parent isn't going to change width or height
		 */
		return (XtGeometryYes);

	if (request->request_mode & CWWidth) {
		if (request->width != bw->box.pref_width) {
			result = XtGeometryAlmost;
			reply->width = bw->box.pref_width;
			reply->request_mode |= CWWidth;
		} 
	}
	if (request->request_mode & CWHeight) {
		if (request->height != bw->box.pref_height) {
			result = XtGeometryAlmost;
			reply->height = bw->box.pref_height;
			reply->request_mode |= CWHeight;
		} 
	}
#ifdef DEBUG_BOX
	reply->width = bw->box.pref_width;
	reply->height = bw->box.pref_height;
	printf ("%s: QueryGeometry (%s, ..) req_w=%s req_h=%s req=%dx%d reply=%dx%d\n",
		__FILE__, w->core.name,
		(request->request_mode & CWWidth) ? "yes" : "no",
		(request->request_mode & CWHeight) ? "yes" : "no",
		request->width, request->height,
		reply->width, reply->height);
#endif
	return (result);
}

/*
 * geometry_manager method
 * handle resize requests from the childs
 */
static XtGeometryResult
GeometryManager (Widget w, XtWidgetGeometry *req, XtWidgetGeometry *reply)
{
	XwBoxWidget bw = (XwBoxWidget) XtParent(w);
	XtGeometryResult result = XtGeometryYes;
	Dimension width, height;

	req->request_mode &= CWWidth | CWHeight;
	if (!req->request_mode)
		return (result);

	reply->request_mode = 0;

	if (req->request_mode & CWWidth) {
		/* client is asking about a new width
		 */
		width = bw->core.width - bw->box.margin * 2 - w->core.border_width * 2;
		if (bw->box.expand && (bw->box.orientation == XtorientVertical)) {
			/* horizontal expansion */
			if (req->width != width) {
				reply->width = width;
				reply->request_mode = CWWidth;
				result = XtGeometryAlmost;
			}
		} else if (req->width > width) {
			reply->width = width;
			reply->request_mode = CWWidth;
			result = XtGeometryAlmost;
		}
	}
	if (req->request_mode & CWHeight) {
		/* client is asking about a new height
		 */
		height = bw->core.height - bw->box.margin * 2 - w->core.border_width * 2;
		if (bw->box.expand && (bw->box.orientation == XtorientHorizontal)) {
			/* vertical expansion */
			if (req->height != height) {
				reply->height = height;
				reply->request_mode = CWHeight;
				result = XtGeometryAlmost;
			}
		} else if (req->height > height) {
			reply->height = height;
			reply->request_mode = CWHeight;
			result = XtGeometryAlmost;
		}
	}
#ifdef DEBUG_BOX
	printf ("%s: GeometryManager (%s, req=%dx%d, rep=%dx%d) box=%s result=%d\n",
			__FILE__, w->core.name, req->width, req->height,
			reply->width, reply->height, XtName((Widget)bw), result);
#endif
	return (result);
}

/*
 * change_managed method
 * change layout of the children when a child is managed or unmanaged
 */
static void
ChangeManaged (Widget w)
{
	Dimension width, height;
	XwBoxWidget bw = (XwBoxWidget) w;
	XtGeometryResult result;
#ifdef DEBUG_BOX
	printf ("%s: ChangeManaged (%s) childs=%d core=%dx%d realized=%d\n",
			__FILE__, w->core.name, ((XwBoxWidget)w)->composite.num_children,
			w->core.width, w->core.height, XtIsRealized(w));
#endif
	Layout (bw, w->core.width, w->core.height, FALSE);
	/* */
	if ((bw->box.pref_width != w->core.width) ||
		(bw->box.pref_height != w->core.height)) {
		result = XtMakeResizeRequest (w,
					bw->box.pref_width, bw->box.pref_height,
					&width, &height);
#ifdef DEBUG_BOX
		printf ("%s: +ChangeManaged(%s) req=%dx%d reply=%dx%d result=%d\n",
			__FILE__, w->core.name,
			bw->box.pref_width, bw->box.pref_height,
			width, height, result);
#endif
		if (result == XtGeometryAlmost) {
			Layout (bw, width, height, TRUE);
			bw->box.pref_width = width;
			bw->box.pref_height= height;
			result = XtMakeResizeRequest (w,
					bw->box.pref_width, bw->box.pref_height,
					&width, &height);
		}
	}
}

/*
 * action function definitions
 */

/*
 * called if TAB was pressed
 */
static void
Tab (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
#ifdef DEBUG_BOX
	printf ("%s: Tab()\n", __FILE__);
#endif
	XtCallCallbackList (w, ((XwBoxWidget)w)->box.tab_callbacks, event);
}

/*
 */
static void
FocusToChild (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	int i;
	XwBoxWidget bw = (XwBoxWidget) w;
	Widget child;
	Time time = CurrentTime;
#ifdef DEBUG_BOX
	printf ("%s: FocusToChild()\n", __FILE__);
#endif
	for (i = 0; i < bw->composite.num_children; i++) {
		child = bw->composite.children[i];
		if (XtIsManaged (child)) {
			if (XtCallAcceptFocus(child, &time)) {
				/* printf (" *setting focus to %s\n", child->core.name); */
				/* XtSetKeyboardFocus (w, child); */
				break;
			}
		}
	}
}

/*EOF*/

