/*
 * Button.h
 *
 * Copyright (C) 1997 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _Button_h
#define _Button_h

#define XtNactiveColor	"activeColor"
#define XtCActiveColor	"ActiveColor"

#ifndef XtNmenuName
#define XtNmenuName	"menuName"
#define XtCMenuName	"MenuName"
#endif
#ifndef XtNaccel
#define XtNaccel	"accel"
#define XtCAccel	"Accel"
#endif
#ifndef XtNbitmap
#define XtNbitmap	"bitmap"
#define XtCBitmap	"Bitmap"
#endif
#ifndef XtNpixmap
#define XtNpixmap	"pixmap"
#endif

#define XtNpopupAtMouse	"popupAtMouse"
#define XtCPopupAtMouse "PopupAtMouse"

/* class record constants */
extern WidgetClass xwButtonWidgetClass;

typedef struct _XwButtonClassRec *XwButtonWidgetClass;
typedef struct _XwButtonRec *XwButtonWidget;

#endif
