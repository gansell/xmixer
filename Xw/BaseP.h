/*
 * BaseP.h
 *
 * Copyright (C) 1998 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _BaseP_h
#define _BaseP_h

#include <X11/CoreP.h>
#include <Xw/Base.h>

/* new fields for the base widget class record
 */
typedef struct {
	/* XtWidgetProc draw_frame; */
	XtActionProc tab_action;
	XtActionProc focus_action;
	XtActionProc getfocus_action;
} XwBaseClassPart;

#define XtInheritTabAction ((XtActionProc) _XtInherit)
#define XtInheritFocusAction ((XtActionProc) _XtInherit)
#define XtInheritGetFocusAction ((XtActionProc) _XtInherit)

/*
 * full class record declaration
 */
typedef struct _XwBaseClassRec {
	CoreClassPart	core_class;
	XwBaseClassPart	base_class;
} XwBaseClassRec;

/*
 * instance definitions
 */
typedef struct {
	/* resources */
	Pixel foreground_pixel;
	Pixel highlight_pixel;
	Pixel shadow_top_pixel;
	Pixel shadow_bottom_pixel;
	Pixel not_sensitive_pixel;
	Dimension shadow_width;
	int shadow_type;
	Boolean resize;
	Boolean acceptDrop;
	XtCallbackList tab_callbacks;
	Widget prev;
	Widget next;
	XtPointer data;		/* general purpose data pointer */

	/* private Widget state */
	GC downGC;
	GC upGC;
	GC blackGC;
} XwBasePart;

/*
 * full instance record declaration
 */
typedef struct _XwBaseRec {
	CorePart	core;
	XwBasePart	base;
} XwBaseRec;

extern XwBaseClassRec xwBaseClassRec;

#endif

