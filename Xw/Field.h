/*
 * Field.h, a single line text entry widget, based on Rob's TextField Widget
 *
 * Copyright (c) 1997 Rasca M. Gmelch
 * Copyright (c) 1995 Robert W. McMullen
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 *
 * Author: Rob McMullen <rwmcm@orion.ae.utexas.edu>
 *         http://www.ae.utexas.edu/~rwmcm
 */

#ifndef __Field_H__
#define __Field_H__

#include <Xw/Base.h>

#define _TextField_WIDGET_VERSION	1.2

#ifndef XtIsTextField
#define XtIsTextField(w) XtIsSubclass((Widget)w, xwFieldWidgetClass)
#endif 

/* Athena style resource names */
/* class record constants */

#ifndef XtNecho
#define XtNecho			"echo"
#endif
#ifndef XtNpendingDelete
#define XtNpendingDelete	"pendingDelete"
#endif
#ifndef XtNlength
#define XtNlength		"length"
#endif
#ifndef XtNstring
#define XtNstring		"string"
#endif
#ifndef XtNinsertPosition
#define XtNinsertPosition	"insertPosition"
#endif
#ifndef XtNdisplayCaret
#define XtNdisplayCaret		"displayCaret"
#endif
#ifndef XtNeditable
#define XtNeditable		"editable"
#endif
#define XtNmargin		"margin"
#define XtNcursorWidth		"cursorWidth"
#define XtNallowSelection	"allowSelection"
#define XtNactivateCallback	"activateCallback"
#define XtNactiveBg			"activeBackground"
#define XtCActiveBg			"ActiveBackground"
#define XtNonlyNumber		"onlyNumber"
#define XtCOnlyNumber		"OnlyNumber"

extern WidgetClass xwFieldWidgetClass;

typedef struct _FieldClassRec *FieldWidgetClass;
typedef struct _FieldRec      *FieldWidget, *XwFieldWidget;

typedef struct _TextFieldReturnStruct {
	int	reason;		/* Motif compatibility */
	XEvent	*event;
	char	*string;
} FieldReturnStruct;

/*
** Public function declarations
*/
#if __STDC__ || defined(__cplusplus)
#define P_(s) s
#else
#define P_(s) ()
#endif

/* TextField.c */
Boolean FieldGetEditable P_((Widget aw));
int FieldGetInsertionPosition P_((Widget aw));
char *XwFieldGetString P_((Widget aw));
void XwFieldInsert P_((Widget aw, int pos, char *str));
void XwFieldReplace P_((Widget aw, int first, int last, char *str));
void XwFieldSetEditable P_((Widget aw, Boolean editable));
void XwFieldSetInsertionPosition P_((Widget aw, int pos));
void XwFieldSetSelection P_((Widget aw, int start, int end, Time time));
void XwFieldSetString P_((Widget aw, char *str));
#undef P_
#endif /* _Field_H */

