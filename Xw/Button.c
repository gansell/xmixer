/*
 * Button.c, vi: tabstop=4
 *
 * Copyright (C) 1998 Rasca Gmelch, Berlin
 * EMail: thron@gmx.de
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef __FILE__
#define __FILE__ "Button.c"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <X11/StringDefs.h>
#include <X11/IntrinsicP.h>
#include <Xw/ButtonP.h>

#define offset(field) XtOffsetOf (XwButtonRec, field)

/* resource list
 */
static XtResource resources [] = {
	/* button */
	{ XtNactiveColor, XtCActiveColor, XtRPixel, sizeof(Pixel),
		offset(button.active_pixel), XtRString, "white" },
	{ XtNpopupAtMouse, XtCPopupAtMouse, XtRBoolean, sizeof(Boolean),
		offset(button.popup_at_mouse), XtRString, (XtPointer)"False" },
	{ XtNcallback, XtCCallback, XtRCallback, sizeof(XtPointer),
		offset(button.notify_callbacks), XtRCallback, (XtPointer)NULL },
	{ XtNmenuName, XtCMenuName, XtRString, sizeof(String),
		offset(button.menu_name), XtRImmediate, NULL },
	{ XtNbitmap, XtCBitmap, XtRPixmap, sizeof(Pixmap),
		offset(button.pic), XtRImmediate, (XtPointer)None },
	{ XtNaccel, XtCAccel, XtRString, sizeof(String),
		offset(button.accel), XtRImmediate, "set() notify() unset()" },
	/* label */
	{ XtNlabel, XtCLabel, XtRString, sizeof(String),
		offset (label.label), XtRImmediate, NULL },
	/* base */
	{ XtNshadowType, XtCShadowType, XtRInt, sizeof(int),
		offset(base.shadow_type), XtRImmediate, (XtPointer)XtShadowUp },
 };

/*
 * action function declarations
 */
static void
	Highlight (Widget, XEvent*, String*, Cardinal *),
	Unhighlight (Widget, XEvent*, String*, Cardinal *),
	Focus (Widget, XEvent*, String*, Cardinal *),
	Reset (Widget, XEvent*, String*, Cardinal *),
	Set (Widget, XEvent*, String*, Cardinal *),
	Notify (Widget, XEvent*, String*, Cardinal *),
	Unset (Widget, XEvent*, String*, Cardinal *),
	Tab (Widget, XEvent*, String*, Cardinal *),
	Popup (Widget, XEvent*, String*, Cardinal *),
	GetFocus (Widget, XEvent*, String*, Cardinal *),
	GetPointer (Widget, XEvent*, String*, Cardinal *);

/*
 * action table
 */
static XtActionsRec actions [] =
{
	{"highlight",	Highlight	},
	{"unhighlight",	Unhighlight	},
	{"focus",		Focus		},
	{"reset",		Reset		},
	{"set",			Set			},
	{"unset",		Unset		},
	{"notify",		Notify		},
	{"get-focus",	GetFocus	},
	{"get-pointer",	GetPointer	},
	{"tab",			Tab			},	/* execute the XtNtabCallback functions */
	{"popup",		Popup		},	/* don't forget to set XtNmenuName */
};

/*
 * default translation table
 */
static char defaultTranslations [] = "\
<EnterWindow>: highlight(in)\n\
<LeaveWindow>: reset()\n\
<FocusIn>: focus(in)\n\
<FocusOut>: focus(out)\n\
<Btn1Down>: set()\n\
<Btn1Up>: notify() unset() get-focus()\n\
<Btn3Down>: set() popup() reset()\n\
Shift<Key>Tab: tab(prev)\n\
<Key>Tab: tab(next)\n\
<Key>Return: set() notify() unset()";

/*
 * method function declarations
 */

static void
	ClassInitialize (void),
	ClassPartInitialize (WidgetClass wc),
	Initialize (Widget treq, Widget tnew, ArgList args, Cardinal *nums),
	Destroy (Widget w),
	Resize (Widget w),
	Redisplay (Widget w, XEvent *event, Region region);
static Boolean
	SetValues (Widget, Widget, Widget, ArgList, Cardinal*),
	AcceptFocus(Widget, Time*);

/*
 * tool functions
 */
static void DrawButton (Widget w);

/*
 * class record initialization
 */
#define BaseClass ((WidgetClass)&xwBaseClassRec)
#define SuperClass ((WidgetClass)&xwLabelClassRec)
#define ThisClass ((WidgetClass)&xwButtonClassRec)
XwButtonClassRec xwButtonClassRec = {
	{
		/* core_class fields     */
		/* superclass            */ SuperClass,
		/* class_name            */ "XwButton",
		/* widget_size           */ sizeof(XwButtonRec),
		/* class_initialize      */ ClassInitialize,
		/* class_part_initialize */ ClassPartInitialize,
		/* class_inited          */ False,
		/* initialize            */ Initialize,
		/* initialize_hook       */ NULL,
		/* realize               */ XtInheritRealize,
		/* actions               */ actions,
		/* num_actions           */ XtNumber(actions),
		/* resources             */ resources,
		/* num_resources         */ XtNumber(resources),
		/* xrm_class             */ NULLQUARK,
		/* compress_motion       */ True,
		/* compress_exposure     */ XtExposeCompressMultiple,
		/* compress_enterleave   */ True,
		/* visible_interest      */ True,
		/* destroy               */ Destroy,
		/* resize                */ Resize,
		/* expose                */ Redisplay,
		/* set_values            */ SetValues,
		/* set_values_hook       */ NULL,
		/* set_values_almost     */ XtInheritSetValuesAlmost,
		/* get_values_hook       */ NULL,
		/* accept_focus          */ AcceptFocus,
		/* version               */ XtVersion,
		/* callback_private      */ NULL,
		/* tm_table              */ defaultTranslations,
		/* query_geometry        */ XtInheritQueryGeometry,
		/* display_accelerator   */ XtInheritDisplayAccelerator,
		/* extension             */ NULL
	},
	{	/* base_class fields */
		XtInheritTabAction,
		XtInheritFocusAction,
		XtInheritGetFocusAction,
	},
	{	/* label_class fields */
		XtInheritDrawLabel,
	},
	{	/* button_class fields */
		0,
		0,
	},
};

WidgetClass xwButtonWidgetClass = ThisClass;


/*
 * method function definitions
 */
#define	Margin(w)		(int)(w->base.shadow_width + w->label.margin)
#define FontHeight(f)  (int)(f->max_bounds.ascent + f->max_bounds.descent)
#define FontAscent(f)	(int)(f->max_bounds.ascent)
#define FontDescent(f)	(int)(f->max_bounds.descent)
#define TextWidth(f,s)	XTextWidth (f,s,strlen(s))

static void
RegAccel (XwButtonWidget bw) {
	char *pos;
	char transl_str[64];
	XtTranslations tlt;
	XtAccelerators acc;

#ifdef DEBUG_BUTTON
	printf ("%s:  RegAccel (%s) label=%s accel=%s\n", __FILE__, ((Widget)bw)->core.name,
			bw->label.label, bw->button.accel);
#endif
	if (bw->label.underline_pos > -1) {
		pos = bw->label.label + bw->label.underline_pos;
		if (*(pos+1) != '&') {
			sprintf (transl_str, "<Key>%c: %s", *(pos+1), bw->button.accel);
			tlt = XtParseTranslationTable (transl_str);
			XtAugmentTranslations ((Widget)bw, tlt);
			sprintf (transl_str, "Alt<Key>%c: %s", *(pos+1), bw->button.accel);
			acc = XtParseAcceleratorTable (transl_str);
			bw->core.accelerators = acc;
		}
	}
}

/*
 */
static void
InitGC (XwButtonWidget w)
{
	XGCValues gc_vals;
	XtGCMask gc_mask;

	gc_vals.fill_style = FillSolid;
	gc_vals.foreground = w->core.background_pixel;
	gc_mask = GCFillStyle | GCForeground;
	w->button.std_bg_gc = XtGetGC ((Widget) w, gc_mask, &gc_vals);
	gc_vals.foreground = w->button.active_pixel;
	w->button.focus_bg_gc = XtGetGC ((Widget) w, gc_mask, &gc_vals);

	gc_vals.background = w->button.active_pixel;
	gc_vals.foreground = w->base.foreground_pixel;
	gc_vals.font = w->label.font->fid;
	gc_mask = GCBackground | GCForeground | GCFont;
	w->button.focusGC = XtGetGC ((Widget)w, gc_mask, &gc_vals);
}

/*
 * initialize Method
 */
static void
Initialize (Widget treq, Widget tnew, ArgList args, Cardinal *num_args)
{
	XwButtonWidget new = (XwButtonWidget) tnew;
#ifdef DEBUG_BUTTON
	printf ("%s: Initialize (%s)\n", __FILE__, new->core.name);
#endif
	if (new->button.pic) {
		int x, y;
		unsigned int width, height, bw;
		Window root;
		XGetGeometry (XtDisplay (new), new->button.pic, &root, &x, &y,
						&width, &height, &bw, &new->button.pic_depth);
#ifdef DEBUG_BUTTON
		printf ("%s: pixmap x=%d y=%d depth=%d\n",
				__FILE__,x, y, new->button.pic_depth);
#endif
		new->core.width = width + 2 * Margin(new);
		new->button.pic_width = width;
		new->core.height = height + 2 * Margin(new);
		new->button.pic_height = height;
	}
	RegAccel (new);
	new->button.set = False;
	InitGC (new);
	new->button.bgGC = new->button.std_bg_gc;
}

/*
 * upward chained
 */
static void
ClassInitialize (void)
{
	XtRegisterGrabAction (Popup, True,
				ButtonPressMask | ButtonReleaseMask,
				GrabModeAsync, GrabModeAsync);
}

/*
 * downward chained
 */
static void
ClassPartInitialize (WidgetClass wc)
{
	XwButtonWidgetClass c = (XwButtonWidgetClass) wc;
	if (c->button_class.draw_button == XtInheritDrawButton)
		c->button_class.draw_button = DrawButton;
	if (c->button_class.popup_menu == XtInheritPopupMenu)
		c->button_class.popup_menu = Popup;
}

/*
 * set_values method
 */
static Boolean
SetValues (Widget curr, Widget req, Widget reply, ArgList args, Cardinal *nargs)
{
	Boolean redraw = False;
	XwButtonWidget bw = (XwButtonWidget) curr;
	XwButtonWidget new = (XwButtonWidget) reply;
	
	if (bw->button.set != new->button.set) {
		redraw = True;
	}
	if (bw->core.sensitive != new->core.sensitive) {
		if (new->core.sensitive == False) {
			new->button.bgGC = new->button.std_bg_gc;
		}
		redraw = True;
	}
	if (bw->label.text != new->label.text) {
		RegAccel (new);
		redraw = True;
	}
	if (bw->button.pic != new->button.pic) {
		int x, y;
		unsigned int bw;
		Window root;
		XGetGeometry (XtDisplay (new), new->button.pic, &root, &x, &y,
						&new->button.pic_width, &new->button.pic_height,
						&bw, &new->button.pic_depth);
		redraw = True;
	}
#ifdef DEBUG_BUTTON
	printf ("%s: SetValues (%s ..) redraw=%d\n", __FILE__, new->core.name, (int)redraw);
#endif
	return (redraw);
}

/* ARGSUSED */
static void
Destroy (Widget w)
{
	XwButtonWidget bw = (XwButtonWidget) w;
#ifdef DEBUG_BUTTON
	printf ("%s: Destroy (%s)\n", __FILE__, w->core.name);
#endif
	XtReleaseGC (w, bw->button.std_bg_gc);
	XtReleaseGC (w, bw->button.focus_bg_gc);
}

/*
 * resize method
 */
static void
Resize (Widget w)
{
}

/*
 * expose method
 */
static void
Redisplay (Widget w, XEvent *event, Region region)
{
	XwButtonWidget bw = (XwButtonWidget) w;

#ifdef DEBUG_BUTTON
	printf ("%s: Redisplay (%s) ul_pos=%d x=%d y=%d len=%d oa=%d\n", __FILE__, w->core.name,
			bw->label.underline_pos, bw->label.underline_x,
			bw->label.underline_y, bw->label.underline_len, bw->core.height);
#endif
	DrawButton (w);
	if (bw->base.shadow_width > 0)
		(*BaseClass->core_class.expose)(w, event, region);
}

/*
 */
static Boolean
AcceptFocus (Widget w, Time *t)
{
#ifdef DEBUG_BUTTON
	printf ("%s: AcceptFocus (%s)\n", __FILE__, w->core.name);
#endif
	if (!XtIsRealized(w))
		return (False);
	if (!XtIsManaged(w))
		return (False);
	if (!XtIsSensitive(w))
		return (False);
	return (True);
}


/*
 * action function definitions
 */

/* ARGSUSED */
static void
Highlight (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwButtonWidget bw = (XwButtonWidget) w;
#ifdef DEBUG_BUTTON
	printf ("%s: Highlight(%s) event=%d\n",__FILE__,bw->label.text,event->type);
#endif
	if (bw->core.sensitive) {
		bw->label.textGC = bw->button.focusGC;
		bw->button.bgGC  = bw->button.focus_bg_gc;
		DrawButton (w);
	}
}

/* ARGSUSED */
static void
Unhighlight (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwButtonWidget bw = (XwButtonWidget) w;
#ifdef DEBUG_BUTTON
	printf ("%s: Unhighlight(%s) event=%d\n", __FILE__,
				bw->label.text, event->type);
#endif
	bw->label.textGC = bw->label.sensitiveGC;
	bw->button.bgGC = bw->button.std_bg_gc;
	DrawButton (w);
}

/*
 */
/* ARGSUSED */
static void
Reset (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwButtonWidget bw = (XwButtonWidget) w;
#ifdef DEBUG_BUTTON
	printf ("%s: Reset(%s)\n", __FILE__, bw->label.text);
#endif
	if (bw->button.set) {
		Unset (w, event, params, num_params);
	}
	Unhighlight (w, event, params, num_params);
}

/* ARGSUSED */
static void
Notify (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwButtonWidget bw = (XwButtonWidget) w;
#ifdef DEBUG_BUTTON
	printf ("%s: Notify(%s)\n", __FILE__, bw->label.text);
#endif
	if (bw->button.set)
		XtCallCallbackList (w, bw->button.notify_callbacks, (XtPointer)event);
}

/*
 */
/* ARGSUSED */
static void
Set (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwButtonWidget bw = (XwButtonWidget) w;
#ifdef DEBUG_BUTTON
	printf ("%s: Set (%s, ..)\n", __FILE__, bw->label.text);
#endif

	if (bw->button.set)
		return;
	bw->button.set = True;
	if (XtIsRealized(w)) {
		XtVaSetValues (w, XtNshadowType, XtShadowDown, NULL);
	}
}

/* ARGSUSED */
static void
Unset (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwButtonWidget bw = (XwButtonWidget) w;

#ifdef DEBUG_BUTTON
	printf ("%s: Unset(%s)\n", __FILE__, bw->label.text);
#endif
	if (!bw->button.set)
		return;
	bw->button.set = False;
	if (XtIsRealized(w))
		XtVaSetValues (w, XtNshadowType, XtShadowUp, NULL);
}

/*
 */
static void
GetFocus (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
#ifdef DEBUG_BUTTON
	printf ("%s: GetFocus(%s)\n", __FILE__, XtName(w));
#endif
    (*xwButtonClassRec.base_class.getfocus_action)(w,event, params, num_params);
}

/*
 */
static void
GetPointer (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
#ifdef DEBUG
	printf ("%s: GetPointer(%s)\n", __FILE__, ((XwButtonWidget)w)->label.text);
#endif
	XWarpPointer (XtDisplay(w), None, XtWindow(w),
		0, 0, 0, 0, w->core.width/2, w->core.height/2);
}

/*
 * for usage as menu-button
 */
static void
Popup (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
	XwButtonWidget bw = (XwButtonWidget) w;
#ifdef DEBUG_BUTTON
	printf ("%s: Popup ('%s')\n", __FILE__, bw->label.text);
#endif
	/* check if button is used as menu button .. */
	if (bw->button.menu_name) {
		Widget menu;
		Position x,y;
		menu = XtNameToWidget (w, bw->button.menu_name);
		if (!menu)
			menu = XtNameToWidget (XtParent(w), bw->button.menu_name);
		if (menu) {
#ifdef DEBUG_BUTTON
		printf ("%s: Popup ('..') menu=%s\n", __FILE__, bw->button.menu_name);
#endif
			if (!XtIsRealized(menu))
				XtRealizeWidget(menu);
			XtTranslateCoords (w, 0, 0, &x, &y);
			if (bw->button.popup_at_mouse && (event->type & ButtonPress)) {
				y += event->xbutton.y;
				x += event->xbutton.x;
			} else {
				y = y + w->core.height + 2 * w->core.border_width;
			}
			XtVaSetValues (menu, XtNx, x, XtNy, y, NULL);
			XtPopupSpringLoaded (menu);
		}
	}
}

/*
 * actions
 */

/* ARGSUSED */
static void
Tab (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
#ifdef DEBUG_BUTTON
    fprintf (stderr, "%s: Tab_action()\n", __FILE__);
#endif
    (*xwButtonClassRec.base_class.tab_action)(w, event, params, num_params);
}

/*
 */
static void
Focus (Widget w, XEvent *event, String *params, Cardinal *num_params)
{
    (*xwButtonClassRec.base_class.focus_action)(w, event, params, num_params);
}



/* tool functions
 */
static void
DrawButton (Widget w)
{
	XwButtonWidget bw = (XwButtonWidget) w;

	if (!XtIsRealized(w))
		return;

	XFillRectangle (XtDisplay (w), XtWindow(w), bw->button.bgGC,
					ShadowWidth(bw), ShadowWidth(bw),
					bw->core.width - (2*ShadowWidth(bw)),
					bw->core.height - (2*ShadowWidth(bw)));
#ifdef DEBUG_BUTTON
	printf ("%s: DrawButton(%s)\n", __FILE__, w->core.name);
#endif
	if (!bw->button.pic) {
 	   (*xwButtonClassRec.label_class.draw_label)(w);
	} else {
#ifdef DEBUG_BUTTON
	printf ("%s: DrawButton(%s) depth=%d\n", __FILE__, w->core.name,
			bw->button.pic_depth);
#endif
		if (bw->button.pic_depth < 2)
			XCopyPlane (XtDisplay(w), bw->button.pic, XtWindow(w),
				bw->label.textGC,
				0, 0, bw->button.pic_width, bw->button.pic_height,
				Margin(bw), Margin(bw), 1L);
		else
			XCopyArea (XtDisplay(w), bw->button.pic, XtWindow(w),
				bw->label.textGC,
				0, 0, bw->button.pic_width, bw->button.pic_height,
				Margin(bw), Margin(bw));
	}
}

/*EOF*/
