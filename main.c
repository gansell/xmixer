/*
 * xmixer:main.c, tabstop=4
 *
 * Copyright (C) 1997 Rasca, Berlin
 * EMail: thron@gmx.de
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#include <stdio.h>
#include <stdlib.h>		/* free() */
#include <getopt.h>
#include <limits.h>
#include "gui.h"
#include "mixer.h"
#include "scf.h"

#define TRUE	1
#define FALSE	0

/*
 * give a short help how to use it and terminate
 */
void
usage (const char *prg)
{
	fprintf (stderr,
		"Usage: %s "
		"[-m mixer_device] [-q] [-n] [-d <display>] [-V] -[h?]\n",
		prg);
	exit(1);
}

/*
 * read the configuration file
 */
int
parse_and_eval_scf (const char *prog_name, const string mixer)
{
	scf_id scf;
	int nod, i, left =0 , right =0;
	string name;

	scf = scf_init (prog_name, NULL);
	if (scf == SCF_FALSE) {
		fprintf (stderr, "Can't init scf handle!\n");
		return (FALSE);
	}
	scf_read (scf);
	nod = mixer_num_of_devs (1);
	for (i = 0; i < nod; i++) {
		name = (const string) mixer_get_name (1, i);
		if (scf_get_array_int_val (scf, mixer, name, 0, &left) == SCF_TRUE)
			mixer_set_vol_left (1, i, left);
		if (mixer_is_stereo (1, i)) {
			if (scf_get_array_int_val (scf, mixer, name, 1, &right) == SCF_TRUE)
				mixer_set_vol_right (1, i, right);
		}
	}
	scf_fini (scf);
	return (TRUE);
}

/*
 * just write the current settings of the mixer device to stdout
 */
void
dump_settings (FILE *fp, const char *mixer_dev, int mixer_id) {
	int nod, i;

	nod = mixer_num_of_devs (mixer_id);
	fprintf (fp, "[%s]\n", mixer_dev);
	for (i = 0; i < nod; i++) {
		if (mixer_is_stereo (1,i)) {
			fprintf (fp, "%s={%d %d}\n",
					mixer_get_name(1,i), mixer_get_vol_left(1,i),
					mixer_get_vol_right(1,i));
		} else {
			fprintf (fp, "%s={%d}\n",
				mixer_get_name(1,i), mixer_get_vol_left(1,i));
		}
	}
}

/*
 */
int
main ( int argc, char **argv) {
	int rc = 0, c;
	int quiet = FALSE, dump = FALSE, ignore_scf = FALSE;
	char *mixer_dev;
	char *dev_name;

	mixer_dev = NULL;

	while ((c = getopt(argc, argv, "?hm:qnVDd:g:xr:-")) != -1) {
		switch (c) {
			case '?':
			case 'h':
				usage (argv[0]);
				break;
			case 'm':
				mixer_dev = optarg;
				break;
			case 'q':
				quiet = TRUE;
				break;
			case 'd':
			case 'g':
			case 'x':
			case 'r':
				/* for the X11 stuff .. */
				break;
			case 'D':
				dump = TRUE;
				break;
			case 'n':
				ignore_scf = TRUE;
				break;
			case 'V':
				printf ("Version %s, "
						"(c) rasca, published unter the GNU GPL\n", VERSION);
				break;
			default:
				break;
		}
	}
	if (quiet || dump) {
		if (argc > optind) {
			usage (argv[0]);
		}
		if (mixer_dev == NULL)
			mixer_dev = DEFAULT_MIXER;
		if (!mixer_init (mixer_dev))
			return (1);
		if (dump) {
			dump_settings (stdout, mixer_dev, 1);
			return (0);
		}
		if (!ignore_scf) {
			parse_and_eval_scf (argv[0], mixer_dev);
		}
	} else {
		/* use gui */
		dev_name = gui_init (&argc, &argv);
		if (argc > optind) {
			usage (argv[0]);
		}
		if (mixer_dev == NULL) {
			if (dev_name == NULL)
				mixer_dev = DEFAULT_MIXER;
			else
				mixer_dev = dev_name;
		}
		if (!mixer_init (mixer_dev))
			return (1);
		if (!ignore_scf)
			parse_and_eval_scf (argv[0], mixer_dev);
		rc = gui_main (mixer_dev, argv[0]);
	}
	return (rc);
}
 
